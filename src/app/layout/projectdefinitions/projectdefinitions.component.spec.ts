import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectdefinitionsComponent } from './projectdefinitions.component';

describe('ProjectdefinitionsComponent', () => {
  let component: ProjectdefinitionsComponent;
  let fixture: ComponentFixture<ProjectdefinitionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectdefinitionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectdefinitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
