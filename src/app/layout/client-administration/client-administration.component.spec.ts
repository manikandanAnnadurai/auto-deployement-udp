import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAdministrationComponent } from './client-administration.component';

describe('ClientAdministrationComponent', () => {
  let component: ClientAdministrationComponent;
  let fixture: ComponentFixture<ClientAdministrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientAdministrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
