import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { formValidators } from "../../helper/formValidators";
import { NgForm } from '@angular/forms';
import { ClientAdminUserBoardService } from '../../services/client-admin-user-board.service';
import { ClientAdministrationService } from '../../services/client-administration.service';
import { SharedvalueService } from 'src/app/services/sharedvalue.service';



declare var $: any
@Component({
  selector: 'app-admin-user-board',
  templateUrl: './client-admin-user-board.component.html',
  providers: [CommonService, MessageService, ToastModule],
  styleUrls: ['./client-admin-user-board.component.css']
})

export class ClientAdminUserBoardComponent implements OnInit {
  showSideBar = true;
  selectedValue: any;
  cols: any[];
  clients: any = [];
  cars: any[];
  adminUserBoard: any;
  adminUserBoardForm: FormGroup;
  showCount: boolean;
  disabled: boolean;
  uploadData: any;
  clientId: number;
  emailId: string;
  phoneNumber: string;
  firstName: string;
  lastName: string;
  password: string;
  userId: number;
  userLoginId: number;
  selectedOrganization: any;
  selectedCar2: any;
  isSubmitted: boolean = false;
  update: boolean = false;
  user_id: number;
  PasswordErrorMessage: string;

  // Organization Name
  organizationNameList = [];
  organizationNameSelectedItems = [];
  organizationNameSettings = {};


  constructor(private http: HttpClient,
    private formBuilder: FormBuilder,
    private ClientAdminUserBoard: ClientAdminUserBoardService,
    private messageService: MessageService,
    private ClientAdministration: ClientAdministrationService,
    private shrService: SharedvalueService
  ) { }

  ngOnInit() {
$('.spinner').show();
    this.shrService.getSideBarDetail().subscribe(resp => { this.showSideBar = resp });
    this.selectedValue = 'true';
    this.GetClientAdminUserBoardList();
    this.adminUserBoardForm = this.formBuilder.group({
      OrganizationName: ["", [Validators.required, Validators.maxLength(50)]],
      FirstName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      LastName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      Email: ["", [Validators.required, formValidators.email, Validators.maxLength(50)]],
      PhoneNumber: ["", [Validators.required, formValidators.noWhitespace, formValidators.numeric, Validators.minLength(7), Validators.maxLength(15)]],
      Password: ["", [Validators.required, Validators.minLength(8), Validators.maxLength(15)]],
      active: "",
      rememberMeFlag: [false]
    });

    this.user_id = parseInt(localStorage.getItem('user_id'));
    this.cols = [
      { field: 'organizationName', header: 'Organization Name' },
      { field: 'adminName', header: 'Admin' },
      { field: 'emailId', header: 'Email' },
      { field: 'phoneNumber', header: 'Phone' }
    ];

    this.organizationNameSettings = {
      singleSelection: true,
      text: "Select Organization",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };


    this.GetAllClients();

  }


  GetClientAdminUserBoardList() {
    this.ClientAdminUserBoard.getRequest('GetAdminUserBoardList').subscribe((AdminUserBoardDetails) => {
      this.adminUserBoard = AdminUserBoardDetails;
      this.showCount = true;
      $('.spinner').hide();
    },err =>{
      $('.spinner').hide();
    });
  }


  GetAllClients() {
    this.ClientAdministration.getRequest('GetClientDetails').subscribe((ClientAdministrationDetails) => {
      for (let i = 0; i <= ClientAdministrationDetails.length - 1; i++) {
        this.organizationNameList.push({
          itemName: ClientAdministrationDetails[i].organizationName, id: ClientAdministrationDetails[i].clientId
        });
      }
    });


  }


  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }


  public get getFields() {
    return this.adminUserBoardForm.controls;
  }


  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }


  onOrganizationNameItemSelect(item: any) {
    this.selectedOrganization = item.id;
  }


  onOrganizationNameItemDeSelect(item: any) {
    this.selectedOrganization = null;
  }

  onOrganizationNameItemDeSelectAll(items: any) {
    if (items.length == 0) {
      this.organizationNameSelectedItems = [];
    }
  }


  ValidatePassword(password: string) {
    var input = password;
    if (input == null || input == '') {
      this.PasswordErrorMessage = "Password should not be empty";
    }
    var hasNumber = new RegExp("[0-9]+");
    var hasUpperChar = new RegExp("[A-Z]+");
    var hasMiniMaxChars = new RegExp("^.{8,15}$");
    var hasLowerChar = new RegExp("[a-z]+");
    //  var hasSymbols = new RegExp("[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
    var hasSymbols = new RegExp("[!@#$%^&*(),.?/:{}|<>]");

    if (!hasUpperChar.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one upper case letter.";
      return this.PasswordErrorMessage;
    }
    else if (!hasLowerChar.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one lower case letter.";
      return this.PasswordErrorMessage;
    }
    else if (!hasMiniMaxChars.test(input)) {
      this.PasswordErrorMessage = "Password should be min 8 to 15 characters.";
      return this.PasswordErrorMessage;
    }
    else if (!hasNumber.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one numeric value.";
      return this.PasswordErrorMessage;
    } else if (!hasSymbols.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one special case character.";
      return this.PasswordErrorMessage;
    }
    else {
      return this.PasswordErrorMessage = 'true';
    }
  }

  onOrganizationNameFilterSelectAll(items: any) {
    this.onOrganizationNameItemSelect(items[items.length - 1]);
  }

  onOrganizationNameFilterDeSelectAll(items: any) {
    this.onOrganizationNameItemDeSelectAll(items = []);
  }


  Open() {
    this.selectedValue = 'true';
    this.isSubmitted = false;
    this.update = false;
  }

  Close() {
    this.isSubmitted = false;
    this.update = false;
    this.adminUserBoardForm.reset();

  }

  successmessage(message) {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: message });
  }

  Errormessage(errorsmessage) {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
  }

  submitForm(params) {
  
    console.log(params);
    let formData = params.value;
    this.validateAllFormFields(this.adminUserBoardForm);
    this.isSubmitted = true;
    this.ValidatePassword(formData["Password"]);
    var data = {};
    this.ValidatePassword(formData["Password"]);
    if (this.adminUserBoardForm.valid && this.update == false && this.PasswordErrorMessage == 'true') {
      $('.spinner').show();
      this.disabled = true;
      data = {
        ClientId: formData["OrganizationName"][0]['id'] * 1,
        FirstName: formData["FirstName"],
        LastName: formData["LastName"],
        PhoneNumber: formData["PhoneNumber"],
        EmailId: formData["Email"],
        Password: formData["Password"],
        Active: (formData["active"] == "true"),
        RecordStatus: 1,
        InsertedBy: this.user_id,
      }

      this.ClientAdminUserBoard.postRequest('InsertAdminUserBoard', data).subscribe((response) => {
        if (response.status == 1) {
          $('.close').trigger('click');
          this.Close();
          this.GetClientAdminUserBoardList();
          this.successmessage("Admin user created successfully");
          this.disabled = false;
          this.isSubmitted = false;
        }
        else {
          $('.spinner').hide();
          this.Errormessage(response["message"]);
          this.disabled = false;
        }
      },err =>{
        this.Errormessage(err.message);
        $('.spinner').hide();
      });
    } else {

      if (this.adminUserBoardForm.valid && this.PasswordErrorMessage == 'true') {
        $('.spinner').show();
        this.disabled = true;
        data = {
          AdminId: this.uploadData["adminId"],
          UserId: this.uploadData["userId"],
          UserLoginId: this.uploadData["userLoginId"],
          ClientId: this.selectedOrganization,
          FirstName: formData["FirstName"],
          LastName: formData["LastName"],
          EmailId: formData["Email"],
          PhoneNumber: formData["PhoneNumber"],
          Password: formData["Password"],
          UpdatedBy: this.user_id,
          RecordStatus: 1,
          InsertedDateTime: this.uploadData["insertedDateTime"],
          Active: (formData["active"] == "true")
        }

        this.ClientAdminUserBoard.postRequest('UpdateAdminUserBoard', data).subscribe((response) => {
          if (response.status == 1) {
            $('.close').trigger('click');
            this.Close();
            this.GetClientAdminUserBoardList();
            this.successmessage("Admin user updated Successfully");
            this.disabled = false;
          }
          else {
            $('.spinner').hide();
            this.Errormessage(response["message"]);
            this.disabled = false;
          }
        },err =>{
          this.Errormessage(err.message);
          $('.spinner').hide();
        });
      }

    }
  }


  editAdminUserBoard(adminUserBoard: any) {
    this.update = true;
    this.selectedOrganization = adminUserBoard.clientId;
    this.organizationNameSelectedItems = [{
      id: adminUserBoard.clientId, itemName: adminUserBoard.organizationName
    }]

    this.adminUserBoardForm.controls['OrganizationName'].setValue(this.organizationNameSelectedItems);
    this.adminUserBoardForm.controls['FirstName'].setValue(adminUserBoard.firstName);
    this.adminUserBoardForm.controls['LastName'].setValue(adminUserBoard.lastName);
    this.adminUserBoardForm.controls['PhoneNumber'].setValue(adminUserBoard.phoneNumber);
    this.adminUserBoardForm.controls['Email'].setValue(adminUserBoard.emailId);
    this.adminUserBoardForm.controls['Password'].setValue(adminUserBoard.password);
    this.selectedValue = String(adminUserBoard.active);
    this.adminUserBoardForm.controls['active'].setValue(this.selectedValue);

    this.uploadData = adminUserBoard;
  }


  deleteAdminUserBoard(adminUserBoard: any) {
$('.spinner').show();
    var data = {
      AdminId: adminUserBoard['adminId'],
      UserId: adminUserBoard['userId'],
      UserLoginId: adminUserBoard['userLoginId']
    }

    this.ClientAdminUserBoard.postRequest('DeleteAdminUserBoard', data).subscribe((response) => {
      if (response.status == 1) {
        this.GetClientAdminUserBoardList();
        this.successmessage("Admin user deleted Successfully");
      }
      else {
        $('.spinner').hide();
        this.Errormessage(response["message"]);
      }
    },err =>{
      this.Errormessage(err.message);
      $('.spinner').hide();
    });
  }

}
