import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAdminUserBoardComponent } from './client-admin-user-board.component';

describe('ClientAdminUserBoardComponent', () => {
  let component: ClientAdminUserBoardComponent;
  let fixture: ComponentFixture<ClientAdminUserBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientAdminUserBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAdminUserBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
