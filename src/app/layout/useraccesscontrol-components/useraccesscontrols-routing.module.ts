import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { UseraccesscontrolComponentsComponent } from './useraccesscontrol-components.component';
// import { UsergroupmanagementComponent } from './usergroupmanagement/usergroupmanagement.component';
import { RolemanagementComponent } from '../rolemanagement/rolemanagement.component';
import { ModulesmanagementComponent } from './modulesmanagement/modulesmanagement.component';
import { PrivilegesmanagementComponent } from './privilegesmanagement/privilegesmanagement.component';
import { UseractivitylogComponent } from './useractivitylog/useractivitylog.component';
import { SharedModule } from '../../shared.module';
import { UsergroupmanagementComponent } from '../usergroupmanagement/usergroupmanagement.component';
// import { ClientAdministrationComponent } from './client-administration/client-administration.component';
// import { AdminUserBoardComponent } from './admin-user-board/admin-user-board.component';
// import { UserManagementComponent } from './user-management/user-management.component';



const routes: Routes = [
        
        // { path: 'Usermanagement', component:  UserManagementComponent},
         { path: 'Usergroupmanagement', component:  UsergroupmanagementComponent},
        { path: 'Rolemanagement', component:  RolemanagementComponent},
        { path: 'Modulesmanagement', component:  ModulesmanagementComponent},
        { path: 'Privilegesmanagement', component:  PrivilegesmanagementComponent},
        { path: 'Useractivitylog', component:  UseractivitylogComponent},
        {path: '', redirectTo: 'Usermanagement'},
        // { path: 'Clientmanagement', component:ClientAdministrationComponent},
		// { path: 'Adminuserboard', component:AdminUserBoardComponent}
    // {
        
    //     // path: '', component: UseraccesscontrolComponentsComponent,
    //     // children: [
        // {path: '', redirectTo: 'Usermanagement'},
        // { path: 'Usermanagement', component:  UsermanagementComponent},
        // { path: 'Usergroupmanagement', component:  UsergroupmanagementComponent},
        // { path: 'Rolemanagement', component:  RolemanagementComponent},
        // { path: 'Modulesmanagement', component:  ModulesmanagementComponent},
        // { path: 'Privilegesmanagement', component:  PrivilegesmanagementComponent},
        // { path: 'Useractivitylog', component:  UseractivitylogComponent},
        // { path: 'Clientmanagement', component:ClientadministrationComponent}
    //     // ]
    // }
];


@NgModule({
    imports: [RouterModule.forChild(routes),SharedModule],
    exports: [RouterModule]
})

export class UserAccessControlsRoutingModule {

}