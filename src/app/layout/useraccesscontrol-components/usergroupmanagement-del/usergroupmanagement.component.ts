import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usergroupmanagement',
  templateUrl: './usergroupmanagement.component.html',
  styleUrls: ['./usergroupmanagement.component.css']
})
export class UsergroupmanagementComponent implements OnInit {
  public groups = [];
  public selectedGroup;
  public selectedUser;
  public projects = [];
  public selectedProject;
  public userList: string;
  public groupMembers = 'Raj Kumar, Ankitha';
  public selectedValue: number;

  constructor() {
    this.selectedValue = 1;
    this.userList = 'Prem, Arockya, Poorni, Lavanya, Sekar';
    this.groups = [
      //  {label: 'LG Group', value: 1, disabled: true},

      { label: 'Honda', value: 1 },
      { label: 'Jaguar', value: 2 },
      { label: 'LG Group', value: 3 },
      { label: 'Mercedes', value: 4 },
      { label: 'Renault', value: 5 },
      { label: 'VW', value: 5 },
      { label: 'Volvo', value: 6 },

    ];
    this.projects = [
      //  {label: 'LG Group', value: 1, disabled: true},

      { label: 'Extraction', value: 1 },
      { label: 'Visualization', value: 2 },
      { label: 'Translation', value: 3 },
      { label: 'Written', value: 4 },
      { label: 'Examine', value: 5 },
      { label: 'Dealer', value: 5 },
      { label: 'Volvo', value: 6 },

    ];

    this.selectedGroup = { label: 'LG Group', value: 3 };
  }

  ngOnInit() {
  }
  OnChange(ev) {
    console.log('OnChange', ev);
    console.log('OnChange', this.selectedGroup);
  }

  moveUserToGroupMember() {

    this.groupMembers += ', ' + this.userList;
  }
}
