import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UseractivitylogComponent } from './useractivitylog.component';

describe('UseractivitylogComponent', () => {
  let component: UseractivitylogComponent;
  let fixture: ComponentFixture<UseractivitylogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UseractivitylogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UseractivitylogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
