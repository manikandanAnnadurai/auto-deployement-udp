import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { jsPlumb } from 'jsplumb';
declare var $: any;
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { WorkflowService } from 'src/app/services/workflow.service';
import { SharedvalueService } from 'src/app/services/sharedvalue.service';
import { InputService } from 'src/app/services/input.service';
import { WorkflowServiceService } from '../../services/workflow-service.service';
import { Pipe, PipeTransform } from '@angular/core';
import { SrchAtrPipe } from './srch-atr.pipe';
import { SearchOutputAtrPipe } from './search-output-atr.pipe';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, RequiredValidator } from '@angular/forms';
import { formValidators } from 'src/app/helper/formValidators';
import { ExcelService } from '../../services/excel.service';
import { empty } from 'rxjs';
import { Item } from 'angular2-multiselect-dropdown';
import { ɵNullViewportScroller } from '@angular/common';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { Table } from "primeng/table";

// import Canvg from 'canvg';
@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  providers: [MessageService, ToastModule, WorkflowServiceService, ExcelService],
  styleUrls: ['./workflow.component.css']
})
export class WorkflowComponent implements OnInit {
  nodeId = 0;
  setActionId = 0;
  editField: string;
  outputdisabled: boolean;
  conSaveDetails = [];
  currentConnection: any = '';
  pathConnections: any = [];
  botList: any = [];
  ShowMessage: boolean;
  status: any[];
  WorkFlowId: any;
  public botListCloner: any = [];
  public botListCloner2: any = [];
  public botTypesClone3: any = [];
  public selectedBotList = [];
  exselectedBotList = [];
  public workflow = [];
  currentActionid: any;
  selectedbotlength: any;
  selectedBotListcloner: any;
  cols: any;
  column: any[];
  public top = 0;
  jsPlumbInstance: any;
  public nodes = [];
  public connections = [];
  public hidecontextMenu = true;
  public previousCount = 0;
  public botd: any = {};
  public spbotd: any = {};
  existingWorkflow: any;
  files: FileList;
  getCanvas: any;
  @ViewChild("atMap", { static: false }) public atMap: Table;

  @ViewChild('myInput', { static: true })
  myInputVariable: any;
  public botTypes: any = [{
    id: 1,
    bot: 'botType 1'
  }, {
    id: 2,
    bot: 'botType 2'
  }, {
    id: 3,
    bot: 'botType 3'
  }];

  public workflowList: any;


  options: any = [
    { label: 'option 1', value: '1' },
    { label: 'option 2', value: '2' },
    { label: 'option 3', value: '3' },
    { label: 'option 4', value: '4' },
  ];

  selected: any = '';

  targetAttributesClone: any;
  targetAttributesClone1: any;
  userAttributes: any;
  sourceAttributes: any;
  sourceAttributesclone: any;
  sourceAttributesclone2: any;
  targetAttributes: any;
  targetBotAttributesList: any;
  disablesourceAttributesDropdown: boolean;
  disableuserAttributeDropdown: boolean;
  aMappingformValuserAttribute: any = '';
  aMappingformValtargetAttribute: any = '';
  aMappingformValsourceAttribute: any = '';
  showtableerror: boolean;
  showruletableerror: boolean;
  showoutputtableerror: boolean;
  validWorkflowName: boolean;
  checksticky: boolean;

  public attributes = [{
    id: 1,
    attr: 'Address Splitter '
  }, {
    id: 2,
    attr: 'checkbox Mob Id'
  }, {
    id: 3,
    attr: 'Dropdown Mob Id'
  },
  {
    id: 4,
    attr: 'SMLE Clx'
  },

  ];
  public attributeTypes: any = [{
    id: 1,
    attr: 'Normal'
  }, {
    id: 2,
    attr: 'Static'
  }, {
    id: 3,
    attr: 'Derived'
  }
  ];
  public curntAttrType = 'NORMAL';
  public actionTypes = [];
  public actionNames = [{ id: 1, actionName: 'Action Name 1' }, { id: 1, actionName: 'Action Name 2' }]


  todo = [
    'Get to work',
    'Pick up groceries',
    'Go home',
    'Fall asleep',
    'Get up',
    'Brush teeth',
    'Take a shower',
    'Check e-mail',
    'Walk dog'
  ];

  done = [];

  outputmappingData = [];
  report_properties = [];
  reportactionmappingid: any = [];
  exreportproperties = [];


  public attributeElemTypes = [];
  public selectedworkflow: any;
  public selectedBotType: any;
  public cm = {
    top: '0px',
    left: '0Px',
    removeIndex: null,
    wfbot: null,

  };
  public wfBotList: any = [];
  public wfBotListCloner: any = [];
  public workflowName = 'Un Known';
  public cwflow: any;
  public currentBotTypeId = 0;
  public commonSearchValue = '';
  public commonSearchValueOne = '';
  public formValues: any = {
    workflowName: '',
    description: ''
  };
  public editwf = false;
  public editIndex = null;
  public issubmitted = false;
  public aMappingformVal = {
    sourceBot: '',
    targetBot: '',
    actionName: '',
    attributeType: this.attributeTypes[0],
    actionType: this.actionTypes[0],
    preAction: this.actionNames[0],
    attributeValue: '',
    wfsequence: '',
    prefix: '',
    suffix: '',
    deliminator: '',
    sequence: '',
    isViewOutput: 1,
    isPrimary: 1,
    userAttribute: '',
    sourceAttribute: '',
    targetAttribute: ''
  };

  public ruleformVal: any = {
    sourceBot: '',
    targetBot: '',
    actionName: '',
    attributeType: this.attributeTypes[0],
    userAttribute: '',
    sourceAttribute: '',
    targetAttribute: '',
    operationType: '',
    selectedRule: ''
  };
  opMapformVal = {
    targetBot: '',
    actionName: '',
    outputFname: ''
  };

  public aMappingTable: any = {
    static: [],
    normal: [],
    derived: []
  };

  public exaMappingTable: any = {
    static: [],
    normal: [],
    derived: []
  };

  public aMappingdata: any = {
    static: [],
    normal: [],
    derived: []
  };
  public ruleTable: any = [];
  public opMapTable: any = {
    userAttribute: [],
    botAttribute: []
  };
  public currentConTab = 'AMAP';
  public jsonUploadData: any = [];

  public userAttrOP = [];
  public botAttrOP = [];
  public submitted = false;
  showSideBar = true;
  public fileName = 'Choose File';
  inputfile: string;
  fileToUpload: File = null;

  columns = [
    {
      header: 'Workflow Name', width: { width: '12%' }, field: 'workflowname', sort: '',
      filtering: { filterString: '', placeholder: 'Workflow Name' }, order: 1, textAlign: ''
    },
    {
      header: 'Workflow Description', width: { width: '12%' }, field: 'description', sort: '',
      filtering: { filterString: '', placeholder: 'Workflow Description' }, order: 2, textAlign: ''
    },
    {
      header: 'Total Steps', width: { width: '16%' }, field: 'totalsteps', sort: '',
      filtering: { filterString: '', placeholder: 'Total Steps' }, order: 3, textAlign: 'text-center'
    },
    {
      header: 'Created By', width: { width: '12%' }, field: 'createdby', sort: '',
      filtering: { filterString: '', placeholder: 'Created By' }, order: 4, textAlign: ''
    },
    {
      header: 'Created Date', width: { width: '13%' }, field: 'createddate', sort: '',
      filtering: { filterString: '', placeholder: 'Created Date' }, order: 5, textAlign: 'text-center'
    },
    {
      header: 'Updated By', width: { width: '10%' }, field: 'updatedby', sort: '',
      filtering: { filterString: '', placeholder: 'Updated By' }, order: 6, textAlign: ''
    },
    {
      header: 'Updated Date', width: { width: '10%' }, field: 'updateddate', sort: '',
      filtering: { filterString: '', placeholder: 'Updated Date' }, order: 7, textAlign: 'text-center'
    },

  ];
  public botTypesCloner: any;
  updateDataDetails: any;
  workflowDetailsForm: FormGroup;
  public draggedItemL: any = [];
  public draggedItemR: any = [];
  public operationTypes: any = [];
  public operationTypesTable: any = [];
  public currentOptype: any = [];
  public currentRuleJson: any = [];
  public ruleJson = '';
  public currentWfDetails: any = {};
  public previouswfDetails: any;
  public currentconnectionName: any = '';
  public srchInAtrValue = '';
  public srchOutAtrValue = '';
  public previousIndex: number;
  public prehighest: any;
  public isbindclickevent = 0;
  public hidecontextMenuRule = true;
  public showTxtboxRuleIdentity = false;
  public currentEvent: any;
  public isEditRule = false;
  public sequenceGeneration: any = [];
  public isremoveShow = true;
  public selectedAttributeToMove = [];
  public ctrlPress: any;
  public sortIndex: any = [];
  public previousOutputMapping = {
    input: [],
    output: []
  };
  public suggestRules = [];

  public rulename = '';
  public isNewRule = false;
  public newrulename = '';
  constructor(
    private messageService: MessageService,
    private wfservice: WorkflowService,
    private formBuilder: FormBuilder,
    public shrService: SharedvalueService,
    private workflowServices: WorkflowServiceService,
    private excelService: ExcelService,
    private inpS: InputService,


  ) {

    this.workflowDetailsForm = formBuilder.group({
      WorkFlowName: ['', [Validators.required, formValidators.alphabeticalComma, Validators.maxLength(50)]],
      WorkFlowDescription: ['', [Validators.maxLength(500)]],
      InputFile: ['', [Validators.required]]
    });


  }

  ngOnInit() {


    $("#WorkFlowName").focus(() => {

      $('.workflownameerror').hide();
    });

    this.operationTypes = [];

    this.operationTypesTable = [];



    this.ShowMessage = true;
    this.outputdisabled = true;
    this.shrService.getSideBarDetail().subscribe(resp => {
      this.showSideBar = resp;
    });

    this.wfservice.getBotList('assets/json/workflowlist.json').subscribe(resp => {
      this.botList = resp.data;
      this.botListCloner = [...this.botList];
      this.botListCloner2 = resp.data;
    });
    this.inpS.getAttribute().subscribe(resp => {
      this.attributeElemTypes = resp.data;
    }, error => {

    });
    this.jsPlumbInstance = jsPlumb.getInstance();

    this.userAttrOP = [{
      id: 1,
      attrName: 'MOB_ID',
      dpName: 'MOB_ID DN'
    },
    {
      id: 2,
      attrName: 'S3_PATH',
      dpName: 'S3PATH DN'
    },
    {
      id: 3,
      attrName: 'CLIENT ID',
      dpName: 'CLIENT DN'
    }];

    this.botAttrOP = [{
      id: 1,
      attrName: 'MOB_ID',
      dpName: 'MOB_ID DN'
    },
    {
      id: 2,
      attrName: 'S3_PATH',
      dpName: 'S3PATH DN'
    },
    {
      id: 3,
      attrName: 'CLIENT ID',
      dpName: 'CLIENT DN'
    }];

    this.GetWorkflowList();
    this.cols = this.columns;
  }

  public get getFields() {
    return this.workflowDetailsForm.controls;
  }

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  handleFileInput(files: FileList) {
    console.log(files);
    this.ShowMessage = true;
    if (files.length > 0) {
      this.fileToUpload = files.item(0);
      this.fileName = files.item(0).name;
      const filetype = files.item(0).name.split('.');
      if (filetype[1] !== 'xlsx') {
        this.ShowMessage = false;
        this.fileName = null;
        this.Errormessage('Please upload xlsx format file');
      } else {
        this.workflowDetailsForm.patchValue({ InputFile: { file: files.item(0) } });
      }
    } else {

    }

  }





  Close() {
    $('.workflownameerror').html('').show();
    this.ShowMessage = true;
    this.validWorkflowName = false;
    this.workflowDetailsForm.reset();
    this.myInputVariable.nativeElement.value = '';
    this.fileName = 'Choose File';
  }

  onFileChange(event) {
    console.log(event);
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      this.inputfile = event.target.files[0];
      this.fileName = file.name;
      const filetype = file.name.split('.');
      const uploadfile = file;
      reader.readAsDataURL(file);
      reader.onload = () => {
        if (filetype[1] !== 'xlsx') {

        } else {
          this.workflowDetailsForm.patchValue({ InputFile: { file: reader.result.slice } });
        }
      };
    }
  }
  // workflow name duplicate checker
  workflowNameValidation(name: any, val) {
    if (name !== undefined && name.trim() !== '') {
      if (name.match(/^[0-9_-]*$/)) {
        if (name.charAt(0) === '-' || name.charAt(0) === '_' || name.charAt(0) === ' ') {
          this.validWorkflowName = false;
          $('.workflownameerror').html('Workflow name should be start with alphanumeric only.').show();
        } else {
          this.validWorkflowName = false;
          $('.workflownameerror').html('Workflow name should be alphanumeric only.').show();
        }
      } else {
        // const formDatas = new FormData();
        // formDatas.append('Workflowname', name);
        // this.workflowServices.postRequest('workflowname_validation', formDatas).subscribe((validate) => {
        //   console.log(validate);
        //   if (validate.status === 'Success') {
        //     this.validWorkflowName = true;
        //   } else {
        //     this.validWorkflowName = false;
        //     $('.workflownameerror').html('Workflow name already exists.').show();
        //   }
        // });
      }
    } else {
      this.validWorkflowName = false;
      $('.workflownameerror').html('Workflow name should be start with alphanumeric only.').show();
      return false;
    }
  }
  nameduplication(name) {
    const formDatas = new FormData();
    formDatas.append('Workflowname', name);
    this.workflowServices.postRequest('workflowname_validation', formDatas).subscribe((validate) => {

      if (validate.status === 'Success') {
        this.validWorkflowName = true;
        return true;
      } else {
        this.validWorkflowName = false;
        $('.workflownameerror').html('Workflow name already exists.').show();
        return false;
      }
    }, error => {
      return false;
    });
  }
  clearFile(){
    $('#InputFile').val('');
  }
  async InsertForm(formdata) {
    $('.spinner').show();
    if (this.editwf !== true) {
      const name = $('#WorkFlowName').val();
     
      const valid = await this.validateAllFormFields(this.workflowDetailsForm);
      if (name !== undefined && name.trim() !== '') {
        if (name.match(/^[0-9_-]*$/) || name.charAt(0) === '-' || name.charAt(0) === '_'
          || name.charAt(0) === ' ' || name.charAt(0) === '.' || name.match(/^[!@#$%^&*(),.?":{}|<>£]*$/) || name.charAt(0) === '['
          || name.charAt(name.length - 1) === ']' || name.includes('.')) {
          this.validWorkflowName = false;
          $('.spinner').hide();
          $('.workflownameerror').html('Workflow name should be start with alphanumeric only.').show();
        } else {
          const formDatas = new FormData();
          formDatas.append('Workflowname', name);
          this.workflowServices.postRequest('workflowname_validation', formDatas).subscribe((validate) => {

            if (validate.status === 'Success') {
              this.validWorkflowName = true;
              if (this.workflowDetailsForm.valid) {
                const datas = {
                  workflowname: formdata.WorkFlowName,
                  createdby: localStorage.getItem('user_name'),
                  updatedby: localStorage.getItem('user_name'),
                  instructions: '',
                  description: formdata.WorkFlowDescription,
                  useraccountid:"1"
                };
                const formDatas = new FormData();
                formDatas.append('details', JSON.stringify(datas));
                formDatas.append('input', formdata.InputFile.file);
                this.workflowServices.postRequest('workflow', formDatas).subscribe((response) => {
                  if (response.status === 'Success') {

                    $('#testone,#nav-home-tab').trigger('click');
                    this.Close();
                    this.GetWorkflowList();
                    this.ShowMessage = true;
                    this.successmessage(response.message);
                  } else {
                    this.ShowMessage = false;
                    this.checksticky = true;
                    $('.spinner').hide();
                    this.Errormessage(response.message);
                  }
                },err =>{
                  $('.spinner').hide();
                  this.Errormessage('Please check the file you upload or try it again.');
                });

              } else {
                $('.spinner').hide();
                return false;
              }
            } else {
              $('.spinner').hide();
              this.validWorkflowName = false;
              $('.workflownameerror').html('Workflow name already exists.').show();
            }
          },err =>{
            console.log(err);
            $('.spinner').hide();
            this.Errormessage('Please check the file you upload or try it again.');
          });
        }
      } else {
        $('.spinner').hide();
        this.validWorkflowName = false;
        // $('.workflownameerror').html('Workflow name should be start with alphanumeric only.').show();
        return false;
      }


    } else {
      if (formdata.InputFile === '') {
        this.workflowDetailsForm.controls.InputFile.setErrors(null);
      }
      if (formdata.WorkFlowName !== '' && formdata.WorkFlowName !== null && formdata.WorkFlowDescription.length < 500) {
        this.validateAllFormFields(this.workflowDetailsForm);
        if (this.workflowDetailsForm.valid) {
          if (this.validWorkflowName === true) {
            this.updateDataDetails.workflowname = formdata.WorkFlowName;
            this.updateDataDetails.description = formdata.WorkFlowDescription;

            const formDatas = new FormData();
            formDatas.append('details', JSON.stringify(this.updateDataDetails));
            //formDatas.append('input', formdata.InputFile.file = undefined ? null : formdata.InputFile.file);
            this.workflowServices.postRequest('Update_newworkflow', formDatas).subscribe((updateresponses) => {
              if (updateresponses.status === 'Success') {
                $('#testone,#nav-home-tab').trigger('click');
                this.Close();
                this.GetWorkflowList();
                this.ShowMessage = true;
                this.successmessage('Workflow Updated successfully');

              } else {
                this.ShowMessage = false;
                this.checksticky = true;
                $('.spinner').hide();
                this.Errormessage(updateresponses.message);
              }
            });
          } else {
            $('.spinner').hide();
            this.ShowMessage = false;
            return false;
          }
        } else {
          $('.spinner').hide();
          return false;
        }
      } else {
        $('.spinner').hide();
        return;
      }
    }
  }

  downloadXlsx() {
    this.status = ['approved', 'rejected', 'pending'];
    const data = [];
    this.workflowServices.getRequest('workflowtemplatedetails').subscribe((Response) => {
      this.excelService.exportAsExcelFile(Response.data, 'sample');
    });

  }



  GetWorkflowList() {
    $('.spinner').show();
    this.workflowServices.getRequest('workflows').subscribe((Response) => {
      this.workflowList = Response.data;
      $('.spinner').hide();
    }, err =>{
      $('.spinner').hide();
      this.Errormessage(err.message);
      
    });
  }

  GetBotList(wf: any) {
    $('.spinner').show();
    this.commonSearchValueOne = '';
    this.clear(0);
    this.selectedBotList = [];
    this.selectedworkflow = wf;
    this.cwflow = wf;
    this.workflowName = wf.workflowname;
    this.workflowServices.getRequest('bots').subscribe(resp => {
      this.botTypes = resp.data[0];
      this.botTypesCloner = JSON.parse(JSON.stringify(this.botTypes));
      this.botTypesClone3 = JSON.parse(JSON.stringify(this.botTypes));
      this.selectedBotType = this.botTypes[0];
      this.botList = this.selectedBotType.categories;
      this.botListCloner = [...this.botList];
      Object.assign(this.botListCloner2, [...this.botList]);
      this.currentBotTypeId = this.selectedBotType.categories;
      $('#nav-profile-tab').trigger('click');
      this.GetWorkflowDetailsById();
    },err =>{
      this.Errormessage(err.message);
      $('.spinner').hide();
    });
  }

  GetWorkflowDetailsById() {
    // debugger;
    this.workflowServices.getRequest('workflows/' + this.selectedworkflow.workflowid).subscribe((Response) => {
      this.selectedBotList = Response && Response.data && Response.data.workflowbots ? Response.data.workflowbots : [];
      this.selectedBotListcloner = this.selectedBotList;
      this.existingWorkflow = Response.data.workflowdiagram;
      this.selectedbotlength = this.selectedBotList.length;

      if (this.existingWorkflow === '') {
        this.existingWorkflow = {};
      }


      if (this.selectedBotList.length) {
        this.selectedBotList.forEach(elem => {
          this.botTypes.forEach(el => {
            el.categories.forEach(src => {
              src.bots.forEach((ele, i) => {
                if (ele.bot_id === elem.bot_id) {
                  src.bots.splice(i, 1);
                }
              });
            });
          });
        });
      }
      $('.spinner').hide();
    }, err =>{
      this.Errormessage(err.message);
      $('.spinner').hide();
    });
  }

  addAMappingTb(atype, table) {
    //debugger;

    this.submitted = false;
    this.showtableerror = false;
    const am = this.aMappingformVal;


    if (atype !== 'STATIC') {
      if ((this.aMappingformValuserAttribute == null || this.aMappingformValuserAttribute === '') &&
        (this.aMappingformValsourceAttribute == null || this.aMappingformValsourceAttribute === '')) {
        this.submitted = true;
        this.aMappingformValuserAttribute = null;
        this.aMappingformValsourceAttribute = null;
        if (this.aMappingformValtargetAttribute === '' ||
          this.aMappingformValtargetAttribute === null || this.aMappingformValtargetAttribute === undefined) {
          this.aMappingformValtargetAttribute = null;
        }
        return;
      }
    }



    if (atype === 'NORMAL') {
      if (am.actionName === '' || am.attributeType === '' || this.aMappingformValtargetAttribute === '' ||
        this.aMappingformValtargetAttribute === null || this.aMappingformValtargetAttribute === undefined) {
        this.submitted = true;
        this.aMappingformValtargetAttribute = null;
        return;
      } else {
        this.targetAttributesClone = this.targetAttributesClone.filter(item =>
          item.attribute_id !== this.aMappingformValtargetAttribute.attribute_id);
        this.targetAttributesClone.push({
          attribute_id: this.aMappingformValtargetAttribute.attribute_id,
          attribute_name: this.aMappingformValtargetAttribute.attribute_name,
          label: this.aMappingformValtargetAttribute.attribute_name, value: this.aMappingformValtargetAttribute, disabled: true
        });
        const obj = {
          // id: Math.floor(Math.random() * 1000) + 100,
          userAttribute: this.aMappingformValuserAttribute == null ? '' : this.aMappingformValuserAttribute,
          sourceAttribute: this.aMappingformValsourceAttribute == null ? '' : this.aMappingformValsourceAttribute,
          targetAttribute: this.aMappingformValtargetAttribute,
          actionid: this.currentActionid,
          inputpropertyid: this.aMappingformValtargetAttribute.attribute_id,
          outputpropertyid: this.aMappingformValsourceAttribute === '' ?
            this.aMappingformValsourceAttribute : this.aMappingformValsourceAttribute.attribute_id,
          workflowinputtemplateid: this.aMappingformValuserAttribute === '' ?
            this.aMappingformValuserAttribute : this.aMappingformValuserAttribute.attribute_id,
          prefix: '',
          suffix: '',
          createdby: localStorage.getItem('user_name'),
          updatedby: localStorage.getItem('user_name')
        };
        this.aMappingTable.normal.unshift(obj);
        this.aMappingformValuserAttribute = '';
        this.aMappingformValsourceAttribute = '';
        this.aMappingformValtargetAttribute = '';
        this.disableuserAttributeDropdown = false;
        this.disablesourceAttributesDropdown = false;

        const amap = this.aMappingTable.normal;
        this.aMappingTable.normal = [];
        this.aMappingTable.normal = amap;
      }


    } else if (atype === 'STATIC') {
      if (am.actionName === '' || am.attributeType === '' || this.aMappingformValtargetAttribute === '' ||
        this.aMappingformValtargetAttribute === null || this.aMappingformValtargetAttribute === undefined) {
        this.submitted = true;
        this.aMappingformValtargetAttribute = null;
        return;
      } else {
        if (this.aMappingformVal.attributeValue === '') {
          this.submitted = true;
          return;
        }
        this.targetAttributesClone = this.targetAttributesClone.filter(item =>
          item.attribute_id !== this.aMappingformValtargetAttribute.attribute_id);
        this.targetAttributesClone.push({
          attribute_id: this.aMappingformValtargetAttribute.attribute_id,
          attribute_name: this.aMappingformValtargetAttribute.attribute_name,
          label: this.aMappingformValtargetAttribute.attribute_name, value: this.aMappingformValtargetAttribute, disabled: true
        });
        const obj = {
          // id: Math.floor(Math.random() * 1000) + 100,
          targetAttribute: this.aMappingformValtargetAttribute,
          attributeValue: this.aMappingformVal.attributeValue,
          botattributeid: this.aMappingformValtargetAttribute.attribute_id,
          actionid: this.currentActionid,
          staticattributevalue: this.aMappingformVal.attributeValue
        };
        this.aMappingTable.static.unshift(obj);
        this.aMappingformValtargetAttribute = '';
        this.aMappingformVal.attributeValue = '';
        const amapstat = this.aMappingTable.static;
        this.aMappingTable.static = [];
        this.aMappingTable.static = amapstat;
      }


    } else if (atype === 'DERIVED') {
      if (am.actionName === '' || am.attributeType === '' || this.aMappingformValtargetAttribute === '' ||
        this.aMappingformValtargetAttribute === null || this.aMappingformValtargetAttribute === undefined) {
        this.submitted = true;
        this.aMappingformValtargetAttribute = null;
        return;
      } else {
        if (this.aMappingformVal.sequence === '' || this.aMappingformVal.prefix.length > 100 ||
          this.aMappingformVal.deliminator.length > 10 || this.aMappingformVal.suffix.length > 100) {
          this.submitted = true;
          return;
        }
        const obj = {
          // id: Math.floor(Math.random() * 1000) + 100,
          userAttribute: this.aMappingformValuserAttribute,
          sourceAttribute: this.aMappingformValsourceAttribute,
          targetAttribute: this.aMappingformValtargetAttribute,
          sequence: this.aMappingformVal.sequence,
          prefix: this.aMappingformVal.prefix,
          suffix: this.aMappingformVal.suffix,
          deliminator: this.aMappingformVal.deliminator,
          workflowinputtemplateid_or_botoutputid: this.aMappingformValsourceAttribute === '' ?
            this.aMappingformValuserAttribute.attribute_id : this.aMappingformValsourceAttribute.attribute_id,
          isfromworkflowinputtemplate: this.aMappingformValsourceAttribute === '' ?
            true : false,
          actionid: this.currentActionid
        };
        this.aMappingTable.derived.unshift(obj);
        this.aMappingformValuserAttribute = '';
        this.aMappingformValsourceAttribute = '';
        this.aMappingformValtargetAttribute = '';
        this.aMappingformVal.sequence = '';
        this.aMappingformVal.prefix = '';
        this.aMappingformVal.suffix = '';
        this.aMappingformVal.deliminator = '';
        this.disableuserAttributeDropdown = false;
        this.disablesourceAttributesDropdown = false;
        const amapderived = this.aMappingTable.derived;
        this.aMappingTable.derived = [];
        this.aMappingTable.derived = amapderived;
      }
      // if (am.actionName === '' || am.attributeType === ''  || am.targetAttribute === '') {
      //   return;
      // } else {
      //   const obj = {
      //     id: Math.floor(Math.random() * 1000) + 100,
      //     userAttribute: this.aMappingformVal.userAttribute,
      //     sourceAttribute: this.aMappingformVal.sourceAttribute,
      //     targetAttribute: this.aMappingformVal.targetAttribute,
      //     sequence: this.aMappingformVal.sequence,
      //     prefix: this.aMappingformVal.prefix,
      //     suffix: this.aMappingformVal.suffix,
      //     deliminator: this.aMappingformVal.deliminator
      //   };
      //   this.aMappingTable.derived.unshift(obj);
      // }


    } else {
      const obj = {
        id: Math.floor(Math.random() * 1000) + 100,
        userAttribute: 'InputMobId',
        sourceAttribute: '',
        targetAttribute: 'Validate Input',
        sequence: 1,
        prefix: 'START',
        suffix: 'END',
        deliminator: '-'
      };
      this.aMappingTable.derived.unshift(obj);
      const amapderived = this.aMappingTable.derived;
      this.aMappingTable.derived = [];
      this.aMappingTable.derived = amapderived;


    }

    // this.curntAttrType = 'false';
    // setTimeout(() => {
    this.curntAttrType = atype;
    this.atMap.reset();

    // });


  }


  GetattributemappingDetails(type) {
    $('.spinner').show();
    this.aMappingTable.normal = [];
    this.aMappingTable.static = [];
    this.aMappingTable.derived = [];

    this.workflowServices.getRequest('attributemapping/' + this.currentActionid).subscribe((Response) => {

      // debugger;
      if (Response.data) {
        if (Response.data.normalattributes.length !== 0) {
          for (let index = 0; index <= Response.data.normalattributes.length - 1; index++) {

            const sourceAttributes = [];
            sourceAttributes.push({
              attribute_id: Response.data.normalattributes[index].outputpropertyid,
              attribute_name: Response.data.normalattributes[index].outputpropertyname
            });

            const targetAttributes = [];
            targetAttributes.push({
              attribute_id: Response.data.normalattributes[index].inputpropertyid,
              attribute_name: Response.data.normalattributes[index].inputpropertyname
            });

            const userAttributes = [];
            userAttributes.push({
              attribute_id: Response.data.normalattributes[index].workflowinputtemplateid,
              attribute_name: Response.data.normalattributes[index].workflowinputtemplatename
            });

            this.targetAttributesClone = this.targetAttributesClone.filter(item =>
              item.attribute_id !== Response.data.normalattributes[index].inputpropertyid);

            this.targetAttributesClone.push({
              attribute_id: Response.data.normalattributes[index].inputpropertyid,
              attribute_name: Response.data.normalattributes[index].inputpropertyname,
              label: Response.data.normalattributes[index].inputpropertyname, value: targetAttributes, disabled: true
            });

            this.aMappingTable.normal.push({
              attributemappingid: Response.data.normalattributes[index].attributemappingid, userAttribute: userAttributes[0],
              sourceAttribute: sourceAttributes[0], targetAttribute: targetAttributes[0],
              inputpropertyid: Response.data.normalattributes[index].inputpropertyid,
              outputpropertyid: Response.data.normalattributes[index].outputpropertyid,
              workflowinputtemplateid: Response.data.normalattributes[index].workflowinputtemplateid,
              actionid: Response.data.normalattributes[index].actionid,
              prefix: Response.data.normalattributes[index].prefix, suffix: Response.data.normalattributes[index].suffix,
              createdby: localStorage.getItem('user_name'), updatedby: localStorage.getItem('user_name')
            });


          }

        }
        if (Response.data.staticattributes.length !== 0) {
          for (let staticindex = 0; staticindex <= Response.data.staticattributes.length - 1; staticindex++) {

            const targetAttributes = [];
            targetAttributes.push({
              attribute_id: Response.data.staticattributes[staticindex].inputpropertyid,
              attribute_name: Response.data.staticattributes[staticindex].inputpropertyname
            });

            this.aMappingTable.static.push({
              attributeValue: Response.data.staticattributes[staticindex].staticattributevalue,
              botattributeid: Response.data.staticattributes[staticindex].inputpropertyid,
              staticattributevalue: Response.data.staticattributes[staticindex].staticattributevalue,
              targetAttribute: targetAttributes[0],
              botstaticattributesid: Response.data.staticattributes[staticindex].botstaticattributesid,
              actionid: Response.data.staticattributes[staticindex].actionid
            });


            // this.targetAttributesClone = this.targetAttributesClone.filter(item =>
            //   item.attribute_id !== Response.data.staticattributes[staticindex].inputpropertyid);

            // this.targetAttributesClone.push({
            //   attribute_id: Response.data.staticattributes[staticindex].inputpropertyid,
            //   attribute_name: Response.data.staticattributes[staticindex].inputpropertyname,
            //   label: Response.data.staticattributes[staticindex].inputpropertyname, value: targetAttributes, disabled: true
            // });

          }
        }

        if (Response.data.derivedattributes.length !== 0) {
          for (let derivedindex = 0; derivedindex <= Response.data.derivedattributes.length - 1; derivedindex++) {


            const sourceAttributes = [];
            sourceAttributes.push({
              attribute_id: Response.data.derivedattributes[derivedindex].workflowinputtemplateid_or_botoutputid,
              attribute_name: Response.data.derivedattributes[derivedindex].outputpropertyname
            });

            const targetAttributes = [];
            targetAttributes.push({
              attribute_id: Response.data.derivedattributes[derivedindex].inputpropertyid,
              attribute_name: Response.data.derivedattributes[derivedindex].inputpropertyname
            });

            const userAttributes = [];
            userAttributes.push({
              attribute_id: Response.data.derivedattributes[derivedindex].workflowinputtemplateid_or_botoutputid,
              attribute_name: Response.data.derivedattributes[derivedindex].workflowinputtemplatename
            });


            this.aMappingTable.derived.push({
              userAttribute: userAttributes[0].workflowinputtemplatename = null ? '' : userAttributes[0],
              sourceAttribute: sourceAttributes[0], targetAttribute: targetAttributes[0],
              sequence: Response.data.derivedattributes[derivedindex].concatenatingsequence,
              prefix: Response.data.derivedattributes[derivedindex].prefix, suffix: Response.data.derivedattributes[derivedindex].suffix,
              deliminator: Response.data.derivedattributes[derivedindex].separator,
              workflowinputtemplateid_or_botoutputid: Response.data.derivedattributes[derivedindex].workflowinputtemplateid_or_botoutputid,
              isfromworkflowinputtemplate: Response.data.derivedattributes[derivedindex].isfromworkflowinputtemplate,
              actionid: Response.data.derivedattributes[derivedindex].actionid,
              botderivedinputpropertiesid: Response.data.derivedattributes[derivedindex].botderivedinputpropertiesid
            });

          }

        }
        this.curntAttrType = type;
        this.atMap.reset();

      }else{
        // this.Errormessage(Response.message);
      }
      this.curntAttrType = type;
      if (type === 'NORMAL') {
        this.aMappingformVal.attributeType = this.attributeTypes[0]
      } else if (type === 'STATIC') {
        this.aMappingformVal.attributeType = this.attributeTypes[1]
      } else if (type === 'DERIVED') {
        this.aMappingformVal.attributeType = this.attributeTypes[2]
      }
$('.spinner').hide();
    });
  }

  workflowNameValidator(event) {
    $('.workflownameerror').html('').show();
    const kcode = (event.which) ? event.which : event.keyCode;

    if (kcode === 8 || kcode === 9 ||
      kcode === 46 || kcode === 95 ||
      kcode === 45 || kcode === 32 ||
      (kcode > 47 && kcode < 58) ||
      (kcode > 64 && kcode < 91) ||
      (kcode > 96 && kcode < 123)) {
      return true;
    }
    else {
      return false;
    }
  }
  refInputValidator(event) {

    $('#refSetBtn').trigger('click');

  }
  numberOnly(event): boolean {

    if ((event.target.value.length) === 10) {
      return false;
    }

    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 31 && (charCode < 48 || charCode > 57)) || charCode === 69) {
      return false;
    }
    return true;

  }
  isJSONEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }

  IsValidJSONString(str) {

    const fi = str.slice(0, 1);
    const li = str.slice(-1);
    let strs = str;
    if (fi === '"' || li === '"') {
      strs = str.substring(1, str.length - 1);

    }
    try {
      let jsp = $.parseJSON(strs);
      if (this.isJSONEmpty(jsp)) {
        return false
      } else {
        return true;

      }
    } catch (e) {
      return false;
    }

  } addruleTb() {
    // debugger;
    $('.notjson').hide();
    this.submitted = true;
    this.showruletableerror = false;
    const rl = this.ruleformVal;
    if (rl.operationType === '' || this.ruleJson === '' || this.ruleJson === null) {
      this.ruleJson = null;
      return;
    } else if (rl.selectedRule === '') {
      return;
    } else if (this.isNewRule && this.newrulename === '') {
      return;
    }
    else {
      if (!this.IsValidJSONString(this.ruleJson)) {
        $('.notjson').html('Entered JSON is not valid.').show();
        return;
      } else {

        const c = this.operationTypesTable.findIndex(x => x.operationTypeId === rl.operationType.operationtype_id);
        let rulename = '';
        if (this.isNewRule) {
          rulename = this.newrulename;
          this.suggestRules.unshift({
            rulejson: this.ruleJson,
            rulename: rulename
          });
          const sugg = [...this.suggestRules];
          this.suggestRules = [];
          this.suggestRules = sugg;
        } else {
          rulename = this.rulename
        }
        if (c === -1) {

          this.operationTypesTable.push({
            id: Math.floor(Math.random() * 1000),
            operationTypeId: rl.operationType.operationtype_id,
            operationType: rl.operationType.operationtype_name,
            isactive: true,
            rulejson: [{
              id: Math.floor(Math.random() * 1000),
              rulename: rulename,
              criteria_json: JSON.parse(this.ruleJson),
              isactive: true
            }]

          });
        } else {
          this.operationTypesTable[c].rulejson.push({
            id: Math.floor(Math.random() * 1000),
            rulename: rulename,
            criteria_json: JSON.parse(this.ruleJson),
            isactive: true
          });
          this.operationTypesTable[c].isactive = true;
        }
      }

    }

    this.ruleJson = '';
    this.ruleformVal.selectedRule = '';
    this.isNewRule = false;
    this.submitted = false;
    $('.notjson').hide();
    $('#textAreaRule').attr({ 'disabled': false })
    


  }

  clearRulemapping() {
    this.ruleformVal.operationType = '';
    this.ruleJson = '';
    this.showruletableerror = false;
    this.ShowMessage = true;
  }
  getoutputmappingDetails() {
    $('.spinner').show();
    this.previousOutputMapping.output = [];
    this.previousOutputMapping.input = [];
    this.aMappingformValuserAttribute = '';
    this.aMappingformValsourceAttribute = '';
    this.aMappingformValtargetAttribute = '';
    this.workflowServices.getRequest('outputmapping/' + this.currentActionid).subscribe((Responsedata) => {
      //debuggezr;
     
      this.reportactionmappingid = '';
      if (Responsedata && Responsedata.data && Responsedata.data.reportproperties && Responsedata.data.reportproperties.length > 0) {
        this.exreportproperties = [];
        this.report_properties = [];
        const reportpropertiesdata = Responsedata.data;
        this.opMapformVal.outputFname = reportpropertiesdata.reportsheetname;
        this.reportactionmappingid = reportpropertiesdata.reportactionmappingid;
        for (let reportIndex = 0; reportIndex <= reportpropertiesdata.reportproperties.length - 1; reportIndex++) {
          let propertydisplayname = '';
          if (reportpropertiesdata && reportpropertiesdata.reportproperties && reportpropertiesdata.reportproperties[reportIndex] && reportpropertiesdata.reportproperties[reportIndex].outputpropertydisplayname) {
            propertydisplayname = reportpropertiesdata.reportproperties[reportIndex].outputpropertydisplayname;
          } else {
            propertydisplayname = reportpropertiesdata.reportproperties[reportIndex].outputpropertyname;
          }
          this.report_properties.push({
            propertydisplayname: propertydisplayname,
            actionid: reportpropertiesdata.reportproperties[reportIndex].actionid,
            botid: reportpropertiesdata.reportproperties[reportIndex].botid,
            createdby: reportpropertiesdata.reportproperties[reportIndex].createdby,
            createddate: reportpropertiesdata.reportproperties[reportIndex].createddate,
            displayorder: reportpropertiesdata.reportproperties[reportIndex].displayorder,
            isactive: reportpropertiesdata.reportproperties[reportIndex].isactive,
            outputpropertydisplayname: reportpropertiesdata.reportproperties[reportIndex].outputpropertydisplayname,
            outputpropertyid: reportpropertiesdata.reportproperties[reportIndex].outputpropertyid,
            outputpropertyname: reportpropertiesdata.reportproperties[reportIndex].outputpropertyname,
            reportpropertyid: reportpropertiesdata.reportproperties[reportIndex].reportpropertyid,
            targetbotid: reportpropertiesdata.reportproperties[reportIndex].targetbotid,
            updatedby: reportpropertiesdata.reportproperties[reportIndex].updatedby,
            updateddate: reportpropertiesdata.reportproperties[reportIndex].updateddate
          });


          const p = this.sourceAttributesclone.findIndex(x => x.outputpropertyname === reportpropertiesdata.reportproperties[reportIndex].outputpropertyname)
          if (p !== -1) {
            this.sourceAttributesclone.splice(p, 1);
          }
          // const valId: string = reportpropertiesdata.reportproperties[reportIndex].outputpropertyid.toString();
          if (reportIndex === (reportpropertiesdata.reportproperties.length - 1)) {
            // this.sourceAttributesclone = this.sourceAttributesclone.filter(item => item.botattributeid !== null);
            this.previousOutputMapping.input = [...this.sourceAttributesclone];
            this.sourceAttributesclone2 = JSON.parse(JSON.stringify(this.sourceAttributesclone));
            if (this.report_properties.length > 0) {
              this.previousOutputMapping.output = [];
              this.previousOutputMapping.output = JSON.parse(JSON.stringify(this.report_properties));
            }
          }

        }

        $('.spinner').hide();

      } else {
        // this.Errormessage('No data found in selected attribute.')
        this.report_properties = [];
       $('.spinner').hide();
      }

    },err =>{
      this.Errormessage(err.message);
      this.report_properties = [];
      $('.spinner').hide();
    });
  }

  getrulemappingDetails() {
    $('.spinner').show();
    // debugger;
    this.workflowServices.getRequest('rule/' + this.currentActionid).subscribe((Response) => {
      // debugger;
      if (Response.data !== null) {
        if (Response.data.actionoperation.length !== 0) {
          this.operationTypesTable = [];
          for (let i = 0; i <= Response.data.actionoperation.length - 1; i++) {
            this.operationTypesTable.push({
              id: Math.floor(Math.random() * 1000),
              actionid: Response.data.actionoperation[i].actionid,
              actionoperationid: Response.data.actionoperation[i].actionoperationid,
              isactive: Response.data.actionoperation[i].isactive,
              operationTypeId: Response.data.actionoperation[i].operationtypeid,
              operationType: Response.data.actionoperation[i].operationtype,
              rulejson: Response.data.actionoperation[i].rule
            });
          }
          $('.spinner').hide();
        }
      }else{
        $('.spinner').hide();
        // this.Errormessage(Response.message);
      }
    },err=>{
      $('.spinner').hide();
      this.Errormessage(err.message);
    });
  }

  rulemapping() {
    $('.spinner').show();
    $('#rulemappingSaveBtn').addClass('event-none');

    if (this.operationTypesTable.length === 1 && this.operationTypesTable[0].isactive === false) {
      this.ShowMessage = false;
      this.showruletableerror = true;
      return false;
    }

    if (this.operationTypesTable.length !== 0) {
      const actionoperations = [];
      this.ShowMessage = false;
      for (let i = 0; i <= this.operationTypesTable.length - 1; i++) {

        // const findId = this.operationTypesTable.filter(Items => Items.actionoperationid === this.operationTypesTable[i].actionoperationid);

        // if (findId === undefined) {
        // }

        const rules = [];

        for (let j = 0; j <= this.operationTypesTable[i].rulejson.length - 1; j++) {
          rules.push({
            ruleid: this.operationTypesTable[i].rulejson[j].ruleid === undefined ? '' : this.operationTypesTable[i].rulejson[j].ruleid,
            sequence: j + 1, isactive: this.operationTypesTable[i].rulejson[j].isactive,
            rulename: this.operationTypesTable[i].rulejson[j].rulename,
            criteria_json: this.operationTypesTable[i].rulejson[j].criteria_json
          });
        }

        actionoperations.push({
          actionoperationid: this.operationTypesTable[i].actionoperationid === undefined ? '' :
            this.operationTypesTable[i].actionoperationid,
          actionid: this.operationTypesTable[i].actionid === undefined ? this.currentActionid : this.operationTypesTable[i].actionid,
          operationtype: this.operationTypesTable[i].operationType,
          operationtypeid: this.operationTypesTable[i].operationTypeId, sequence: i + 1, createdby: localStorage.getItem('user_name'),
          updatedby: localStorage.getItem('user_name'), isactive: this.operationTypesTable[i].isactive, rule: rules
        });


      }

      const ruledatas = {
        actionoperation: actionoperations
      };

      const formDatas = new FormData();
      formDatas.append('details', JSON.stringify(ruledatas));

      this.workflowServices.postRequest('rule', formDatas).subscribe((response) => {
        // debugger;
        if (response.status === 'Success') {

          this.getrulemappingDetails();
          this.successmessage(response.message);
          setTimeout(() => {
            $('#rulemappingSaveBtn').removeClass('event-none');
          }, 1000);
        } else {
          // this.Errormessage(response.message);
          $('.spinner').hide();
          $('#rulemappingSaveBtn').removeClass('event-none');
        }
      });
    } else {
      this.ShowMessage = false;
      this.showruletableerror = true;
      $('.spinner').hide();
      $('#rulemappingSaveBtn').removeClass('event-none');
      //this.Errormessage('Please map the Rule');
    }
  }

  deloperation(data, index) {
    // debugger;
    if (data[index].actionoperationid !== undefined) {
      this.operationTypesTable[index].isactive = false;
      // const rulemappingData = [];
      const rules = [];

      for (let i = 0; i <= this.operationTypesTable[index].rulejson.length - 1; i++) {
        rules.push({
          actionoperationid: this.operationTypesTable[index].rulejson[i].actionoperationid,
          createdDate: this.operationTypesTable[index].rulejson[i].createdDate,
          createdby: this.operationTypesTable[index].rulejson[i].createdby,
          criteria_json: this.operationTypesTable[index].rulejson[i].criteria_json,
          isactive: false,
          ruleid: this.operationTypesTable[index].rulejson[i].ruleid,
          sequence: this.operationTypesTable[index].rulejson[i].sequence,
          updatedDate: this.operationTypesTable[index].rulejson[i].updatedDate,
          updatedby: this.operationTypesTable[index].rulejson[i].updatedby
        });
      }

      this.operationTypesTable[index].rulejson = rules;

      // rulemappingData.push({
      //   actionid: data[index].actionid, actionoperationid: data[index].actionoperationid,
      //   isactive: false, operationtype: data[index].operationType,
      //   operationtypeid: data[index].operationTypeId, rule: rules, createdby: localStorage.getItem('user_name'),
      //   updatedby: localStorage.getItem('user_name'), sequence: index + 1
      // });

      // const ruledatas = {
      //   actionoperation: rulemappingData
      // };

      // const formDatas = new FormData();
      // formDatas.append('details', JSON.stringify(ruledatas));

      // this.workflowServices.postRequest('updaterule', formDatas).subscribe((response) => {
      //   // debugger;
      //   if (response.status === 'Success') {
      //     this.getrulemappingDetails();
      //     this.successmessage(response.message);
      //   } else {
      //     this.Errormessage(response.message);
      //   }
      // });
    } else {
      data.splice(index, 1);
    }
  }

  delrule(rules, data, index) {
    // debugger;

    if (data[index].ruleid !== undefined) {

      const opreationindex = this.operationTypesTable.findIndex(Items => Items.actionoperationid === data[index].actionoperationid);
      this.operationTypesTable[opreationindex].rulejson[index].isactive = false;

      const b = data.findIndex(x => x.isactive === true);
      if (b === -1) {
        this.operationTypesTable[opreationindex].isactive = false;
      }
      // const optindex = this.operationTypesTable.findIndex(item => item.actionoperationid === data[index].actionoperationid);
      // const rulemappingData = [];

      // const rules = [];

      // rules.push({
      //   criteria_json: data[index].criteria_json, isactive: false, ruleid: data[index].ruleid, sequence: data[index].sequence
      // });

      // rulemappingData.push({
      //   actionid: this.operationTypesTable[optindex].actionid, actionoperationid: this.operationTypesTable[optindex].actionoperationid,
      //   isactive: this.operationTypesTable[optindex].isactive, operationtype: this.operationTypesTable[optindex].operationType,
      //   operationtypeid: this.operationTypesTable[optindex].operationTypeId, rule: rules, createdby: localStorage.getItem('user_name'),
      //   updatedby: localStorage.getItem('user_name'), sequence: optindex + 1
      // });

      // const ruledatas = {
      //   actionoperation: rulemappingData
      // };

      // const formDatas = new FormData();
      // formDatas.append('details', JSON.stringify(ruledatas));

      // this.workflowServices.postRequest('updaterule', formDatas).subscribe((response) => {
      //   // debugger;
      //   if (response.status === 'Success') {
      //     this.getrulemappingDetails();
      //     this.successmessage(response.message);
      //   } else {
      //     this.Errormessage(response.message);
      //   }
      // });
    } else {
      data.splice(index, 1);
    }
  }

  saveConnectorModal(val) {
    // ;
    let data;
    if (this.currentConTab === 'AMAP') {
      data = {
        formValues: this.aMappingformVal,
        aMappingTable: this.aMappingTable
      };
    }

    if (this.currentConTab === 'RULE') {
      data = {
        formValues: this.ruleformVal,
        ruleTable: this.ruleTable
      };
    }

    if (this.currentConTab === 'OMAP') {
      this.submitted = true;
      if (this.opMapformVal.outputFname === '') {
        return;
      }

      data = {
        formValues: this.opMapformVal,
        opMapTable: this.opMapTable
      };
    }
  }

  deleteAM(data, remove) {
    // debugger;
    this.targetAttributesClone = this.targetAttributesClone.filter(item =>
      item.attribute_id !== remove.targetAttribute.attribute_id);
    this.targetAttributesClone.push({
      attribute_id: remove.targetAttribute.attribute_id, attribute_name: remove.targetAttribute.attribute_name,
      label: remove.targetAttribute.attribute_name, value: remove.targetAttribute, disabled: false
    });
    if (this.curntAttrType === 'NORMAL') {
      this.exaMappingTable.normal.push(remove);
      this.aMappingTable.normal = this.aMappingTable.normal.filter(item => item !== remove);
    }
    if (this.curntAttrType === 'STATIC') {
      this.exaMappingTable.static.push(remove);
      this.aMappingTable.static = this.aMappingTable.static.filter(item => item !== remove);
    }
    if (this.curntAttrType === 'DERIVED') {
      this.exaMappingTable.derived.push(remove);
      this.aMappingTable.derived = this.aMappingTable.derived.filter(item => item !== remove);
    }
  }

  deleteRule(data, i) {
    data.splice(i, 1);
  }



  botDetailsModalOpen(data) {
    this.workflowServices.getRequest('bots/' + data.bot_id).subscribe(resp => {
      this.botd = resp.data;
      this.spbotd = { spbotname: data.spbotname, spbottype: data.spbottype, spbottypeid: data.spbottypeid, isesurlsp: data.isesurlsp };
    });
  }

  openModaladdwf() {
    this.messageService.clear();
    this.formValues = {
      worflowName: '',
      description: ''
    };
    this.ShowMessage = true;
    this.issubmitted = false;
    this.editwf = false;
  }

  addNewWorkflow(val) {
    this.issubmitted = true;
    if (this.formValues.description.trim() === '') {
      return;
    }

    if (this.formValues.workflowName.trim() === '') {
      return;
    }
    // file
    const uploadfile = this.formValues.formData;
    // file
    $('#adduser').modal('hide');
    if (val) {
      const wf = this.workflowList[this.editIndex];
      const obj = {
        id: wf.id,
        description: this.formValues.description,
        steps: wf.steps,
        createdDate: wf.createdDate,
        updatedDate: '02/02/2020',
        formData: uploadfile,
        inputAttributes: this.formValues.inputValues,
        workflow: this.formValues.workflowName,
        nodes: wf.nodes,
        connections: wf.connections
      };
      this.workflowList[this.editIndex] = obj;
      this.successmessage('Workflow updated successfully');
    } else {
      const cloner = [...this.workflowList];
      cloner.push({
        id: Math.floor(Math.random() * 1000),
        description: this.formValues.description,
        steps: 0,
        createdDate: '29/01/2020',
        updatedDate: '02/02/2020',
        inputAttributes: this.formValues.inputValues,
        workflow: this.formValues.workflowName,
        nodes: [],
        connections: []
      });
      this.workflowList = [];
      this.workflowList = cloner;
      this.successmessage('Workflow created successfully');
    }


  }
  editWorkflow(wf) {
    $('.spinner').show();
    $('#adduser').modal('show');
    this.updateDataDetails = wf;
    this.workflowDetailsForm.setValue({
      WorkFlowName: wf.workflowname,
      WorkFlowDescription: wf.description,
      InputFile: ''
    });
    this.issubmitted = true;
    this.editwf = true;
    $('.spinner').hide();
  }

  DeleteWF(i) {
    this.workflowList.splice(i, 1);
    this.successmessage('Workflow deleted successfully');
  }

  saveoutputdata() {
    $('.spinner').show();
    $('#outputSave').addClass('event-none');
    this.ShowMessage = false;
    if (this.opMapformVal.outputFname === '') {
      this.submitted = true;
      $('.spinner').hide();
      $('#outputSave').removeClass('event-none');
      return;
    } else {
      if (this.opMapformVal.outputFname.length > 200) {
        $('.spinner').hide();
        $('#outputSave').removeClass('event-none');
        return;
      } else {
        // start functionality
        if (this.report_properties.length > 0) {

          if (this.previousOutputMapping.output.length > 0 ) {
            // update


            const reportpropertiesdatas = [];
            for (let index = 0; index <= this.report_properties.length - 1; index++) {
              const pkreportpropId = this.report_properties && this.report_properties[index].reportpropertyid ? this.report_properties[index].reportpropertyid : undefined;
              if (pkreportpropId === undefined) {
                reportpropertiesdatas.push({
                  outputpropertyname: this.report_properties[index].outputpropertyname, isactive: true,
                  propertydisplayname: this.report_properties[index].propertydisplayname, displayorder: index + 1
                });
              } else {
                reportpropertiesdatas.push({
                  outputpropertyname: this.report_properties[index].outputpropertyname, isactive: true,
                  reportpropertyid: this.report_properties[index].reportpropertyid,
                  propertydisplayname: this.report_properties[index].propertydisplayname, displayorder: index + 1
                });
              }

            }

            this.previousOutputMapping.output.forEach(preObj => {
              const b = this.report_properties.findIndex(x => x.reportpropertyid === preObj.reportpropertyid);
              if (b === -1) {
                reportpropertiesdatas.push({
                  outputpropertyname: preObj.outputpropertyname, isactive: false,
                  reportpropertyid: preObj.reportpropertyid,
                  propertydisplayname: preObj.propertydisplayname, displayorder: preObj.displayorder
                })
              }
            })
            const exoutputData = [];
            
            exoutputData.push({
              reportactionmappingid: this.reportactionmappingid,
              workflowid: this.selectedworkflow.workflowid,
              actionid: this.currentActionid,
              isactive: true,
              createdby: localStorage.getItem('user_name'),
              updatedby: localStorage.getItem('user_name'),
              isprimaryoutput: true,
              reportsheetname: this.opMapformVal.outputFname,
              report_properties: reportpropertiesdatas
            });

            const exformDatas = new FormData();
            exformDatas.append('details', JSON.stringify(exoutputData[0]));

            this.workflowServices.postRequest('update_reportactionmapping', exformDatas).subscribe((responsesed) => {
              if (responsesed.status === 'Success') {
                $('#outputSave').removeClass('event-none');
                this.getoutputmappingDetails();
                this.successmessage(responsesed.message);
              } else {
                $('.spinner').hide();
                this.Errormessage(responsesed.message);
              }
            }, err => {
              $('.spinner').hide();
              $('#outputSave').removeClass('event-none');
            });

            // update and insert
          } else {
            // insert
            const reportpropertiesdatas = [];
            for (let index = 0; index <= this.report_properties.length - 1; index++) {
              const pkreportpropId = this.report_properties && this.report_properties[index].reportpropertyid ? this.report_properties[index].reportpropertyid : undefined;
              if (pkreportpropId === undefined) {
                reportpropertiesdatas.push({
                  outputpropertyname: this.report_properties[index].outputpropertyname, isactive: true,
                  propertydisplayname: this.report_properties[index].propertydisplayname, displayorder: index + 1
                });
              } else {
                reportpropertiesdatas.push({
                  outputpropertyname: this.report_properties[index].outputpropertyname, isactive: true,
                  reportpropertyid: this.report_properties[index].reportpropertyid,
                  propertydisplayname: this.report_properties[index].propertydisplayname, displayorder: index + 1
                });
              }

            }
            const outputData = [];
            outputData.push({
              workflowid: this.selectedworkflow.workflowid, actionid: this.currentActionid, isactive: true,
              createdby: localStorage.getItem('user_name'),
              updatedby: localStorage.getItem('user_name'), isprimaryoutput: true, reportsheetname: this.opMapformVal.outputFname,
              report_properties: reportpropertiesdatas
            });
            const formDatas = new FormData();
            formDatas.append('details', JSON.stringify(outputData[0]));
            this.workflowServices.postRequest('reportactionmapping', formDatas).subscribe((response) => {
              if (response.status === 'Success') {
                $('#outputSave').removeClass('event-none');
                this.getoutputmappingDetails();
                this.successmessage(response.message);
              } else {
                $('.spinner').hide();
                this.Errormessage(response.message);
              }
            }, err => {
              $('.spinner').hide();
              $('#outputSave').removeClass('event-none');
            });
          }
        } else {
          $('.spinner').hide();
          $('#outputSave').removeClass('event-none');
          this.Errormessage('Please add atleast one attribute.');
        }

      } // functionality else end
    }
  }

  editoutput(outputdata) {
    $(this).prop('disabled', false);
  }
  dragStarted(event, item, data, isleft) {

    const a = this.selectedAttributeToMove.findIndex(x => x.id === item.id);
    if (a === -1) {
      item.selected = true;
      let idx = data.indexOf(item);
      item.selected = true;
      this.selectedAttributeToMove.push(item)
    } else {

    }

    this.selectedAttributeToMove.forEach((elem, i) => {

      const uIx = data.findIndex(x => x.id === elem.id);
      data.splice(uIx, 1);

      if (this.selectedAttributeToMove.length === (i + 1)) {
        this.selectedAttributeToMove.forEach((ele, i) => {
          data.push(ele);
        });
      }


      if (isleft) {
        this.draggedItemL.push(elem);
      } else {
        this.draggedItemR.push(elem)
      }
    });
    $('.selected').addClass('dragging');
    $('.selected').css({ 'opacity': 0.3, 'height': '0' });
    $('cdk-drag-placeholder').css({ 'opacity': 0 });







  }
  onKeyDown(e, item, data) {


    this.ctrlPress = e.ctrlKey;

    if (e.ctrlKey) {
      // item.index = idx;

      let idx = data.indexOf(item);
      this.sortIndex.push(idx);
      this.sortIndex.sort();

      this.selectedAttributeToMove = [];
      this.sortIndex.forEach(i => {

        data[i].selected = true;
        this.selectedAttributeToMove.push(data[i]);
      });
    }
  }

  droped(event: CdkDragDrop<string[]>, isleft) {

    $('.selected').removeClass('dragging');
    $('.selected').css({ 'opacity': 1 });
    $('cdk-drag-placeholder').css({ 'opacity': 1 });
    //debugger;
    // chekc if length === 1
    if (this.selectedAttributeToMove.length === 1) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      } else {
        this.showoutputtableerror = false;
        if (!isleft) {
          this.exreportproperties.push(event.previousContainer.data[event.previousIndex]);
        } else {


        }
        // 
        transferArrayItem(event.previousContainer.data,
          event.container.data,
          this.previousIndex,
          event.currentIndex);

      }
    } else {
      if (event.previousContainer === event.container) {

        this.selectedAttributeToMove.forEach((el, ix) => {
          const n = event.container.data.indexOf(el);
          if (n === -1) { } else {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex + ix);

          }
        });

      } else {
        this.showoutputtableerror = false;
        if (!isleft) {
          this.selectedAttributeToMove.forEach((elm, inx) => {
            const b = event.previousContainer.data.indexOf(elm);
            if (b === -1) { } else {
              this.exreportproperties.push(event.previousContainer.data[b + inx]);
            }
            // moveItemInArray(event.container.data, event.previousIndex + ix , event.currentIndex);
          });

        } else {
          this.selectedAttributeToMove.forEach((elm, inx) => {
            const c = event.previousContainer.data.indexOf(elm);
            if (c === -1) { } else {
              delete event.previousContainer.data[c]['reportpropertyid'];
            }
            // moveItemInArray(event.container.data, event.previousIndex + ix , event.currentIndex);
          });

        }
        //
        this.selectedAttributeToMove.forEach((ele, idx) => {
          const c = event.previousContainer.data.indexOf(ele);
          transferArrayItem(event.previousContainer.data,
            event.container.data,
            c,
            event.currentIndex + idx);
        })

      }
    }

    // chekc if length === 1
    //  check if length more than > 1


    try {
      const currentItem = JSON.parse(JSON.stringify(this.draggedItemL));

      const b = this.sourceAttributesclone2.findIndex(x => x.outputpropertyname === currentItem[0].outputpropertyname);
      if (b === -1) { } else { this.sourceAttributesclone2.splice(b, 1) }
      this.draggedItemL = [];
      this.draggedItemR = [];
      this.selectedAttributeToMove = [];

    } catch (e) {
      this.selectedAttributeToMove = [];
      this.draggedItemL = [];
      this.draggedItemR = [];
    }
    this.sortIndex = [];
    event.container.data.forEach(el => {
      el['selected'] = false;
    });
    event.previousContainer.data.forEach(elm => {
      elm['selected'] = false;
    });
  }

  dragItemL(item) {
    // this.draggedItemL.push(item);
    const b = this.selectedAttributeToMove.indexOf(item);
    if (b === -1) {
      this.selectedAttributeToMove.push(item);
    }
    this.draggedItemL = this.selectedAttributeToMove;
    $('.selected').addClass('dragging');
    $('.selected').css({ 'opacity': 0.3 });
    $('cdk-drag-placeholder').css({ 'opacity': 0 });
  }
  dragItemR(item) {
    // this.draggedItemR.push(item);
    const b = this.selectedAttributeToMove.indexOf(item);
    if (b === -1) {
      this.selectedAttributeToMove.push(item);
    }
    this.draggedItemR = this.selectedAttributeToMove;
    $('.selected').addClass('dragging');
    $('.selected').css({ 'opacity': 0.3 });
    $('cdk-drag-placeholder').css({ 'opacity': 0 });
  }
  dragItemOptype(item) {
    this.currentOptype.push(item);
  }
  dragItemrj(item) {
    this.currentRuleJson.push(item);
  }

  drop(event: CdkDragDrop<string[]>) {
    this.exselectedBotList.push(event.previousContainer.data[event.previousIndex]);
    const botid = event.previousContainer.data[event.previousIndex]['bot_id'];
    if (event.previousContainer === event.container) {
      return;
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
    this.botListCloner2.forEach(elm => {
      const k = elm.bots.findIndex(x => x.bot_id === botid);
      if (k === -1) { } else {
        elm.bots.splice(k, 1);
      }
    });
  }
  dropsort(event: CdkDragDrop<string[]>, data) {
    moveItemInArray(data, event.previousIndex, event.currentIndex);
    this.currentOptype = [];
    this.currentRuleJson = [];
  }


  removeSelectedBot(selbot, a) {

    let b;
    if (this.existingWorkflow) {
      b = this.existingWorkflow.nodes.findIndex(x => x.botId === selbot.bot_id);

    } else {
      b = -1;
    }


    if (b === -1) {

      this.botTypesClone3.forEach((elm, inx) => {
        elm.categories.forEach((elem, ix) => {
          elem.bots.forEach(el => {
            if (el.bot_id === selbot.bot_id) {
              this.botTypes[inx].categories[ix].bots.push(selbot);
            }
          });
        });
      });

      this.botCommonSearch($('#cmnSearch').val());
      this.selectedBotList.splice(a, 1);

    } else {
      this.Errormessage('Sorry you cannot remove the existing workflow bots.');
    }

    this.exselectedBotList = this.exselectedBotList.filter(items => items.bot_id !== selbot.bot_id);

  }
  dragTouchEndEvent(itemArray, item) {
    this.previousIndex = itemArray.findIndex(e => e === item);

  }
  inputAttributeSearch(val) {
    this.srchInAtrValue = val;
    // this.sourceAttributesclone = this.sourceAttributesclone2.filter((obj) => {
    //   return obj.outputpropertyname.toLowerCase().includes(val.toLowerCase())
    // });
  }
  outputAttributeSearch(val) {
    this.srchOutAtrValue = val;
    // this.sourceAttributesclone = this.sourceAttributesclone2.filter((obj) => {
    //   return obj.outputpropertyname.toLowerCase().includes(val.toLowerCase())
    // });
  }
  initialbot() {

    this.nodeId = 0;
    const initalbots = [{
      id: 'BOT0BOT0',
      nodeId: 0,
      cName: 'Input',
      botId: 0,
      left: '100px',
      bot_type: 'Initial',
      top: '30px',
      y: 30,
      x: 100,
      active: false,
      wfgroupId: -1,
    },
    {
      id: 'BOT0.1BOT0.1',
      nodeId: 1,
      cName: 'Output',
      botId: 0.1,
      left: '300px',
      bot_type: 'END',
      top: '30px',
      y: 30,
      x: 300,
      active: false,
      wfgroupId: -1,
    }];

    initalbots.forEach(obj => {
      this.workflow.push(obj);
      this.loadJsplump(obj, 0);
    })

  }
  addtoWorkflow(bot, node, data, index) {
    let bot_type_id;
    let bot_type;
    let highest;
    if (bot.bot_id !== 0.1) {
      if (this.workflow.length > 0) {
        highest = Math.max.apply(Math, this.workflow.map((x) => { return x.y; }));
        this.nodeId = Math.max.apply(Math, this.workflow.map((x) => { return x.nodeId }));
      } else {
        highest = 0;
        this.nodeId = 0;
      }
      this.nodeId = this.nodeId + 1;

      this.botTypesClone3.forEach(c => {
        c.categories.forEach(b => {
          b.bots.forEach(bt => {
            if (bt.bot_id === bot.bot_id) {

              bot_type_id = c.bot_type_id;
              bot_type = c.bot_type;

            }
          });
        });
      });
    } else {
      highest = Math.max.apply(Math, this.workflow.map((x) => { return x.y; }));
      this.nodeId = 0.1;
      bot_type_id = 0.1;
      bot_type = 'END';
    }


    let xleft; let x;

    if (data) {
      if (index === 0) {
        this.prehighest = highest
      }
      this.top = this.prehighest + 70 + node.y;
      xleft = node.left;
      x = xleft.split('px')[0];
    } else {
      this.top = highest + 70;
      xleft = '100px';
      x = 100;
    }

    const obj = {
      id: 'BOT' + this.nodeId + 'BOT' + (bot.bot_id ? bot.bot_id : 0),
      nodeId: this.nodeId,
      cName: bot.bot_name,
      botId: bot.bot_id,
      left: xleft,
      bot_type: bot_type,
      bot_type_id: bot_type_id,
      top: this.top + 'px',
      y: this.top,
      x: x,
      active: false,
      wfgroupId: -1,
    };

    if (!obj.bot_type) {
      obj.bot_type = 'Initial';
      obj.bot_type_id = 0;
      obj.cName = 'Input';
    };

    if (data) {
      data.connections.forEach(elm => {
        if (elm.source === node.id) {
          elm.source = obj.id;
          const srctgt = elm.id.split('-');
          elm.id = obj.id + '-' + elm.target;
          elm.uuids[0] = obj.id + '_right';
        }
        if (elm.target === node.id) {
          elm.target = obj.id;
          const srctgt = elm.id.split('-');
          elm.id = elm.source + '-' + obj.id;
          elm.uuids[0] = obj.id + '_left';
        }
      })
    }
    this.workflow.push(obj);
    this.loadJsplump(obj, 1);


  }
  setAction() {
    this.setActionId = this.setActionId + 1;
    const k = this.pathConnections.findIndex(x => x.id === this.currentConnection);
    if (k === -1) { } else {
      this.pathConnections[k].actionId = this.setActionId;
    }

  }

  loadJsplump(obj, reuse) {
    setTimeout(() => {


      const Endpoint1 = {
        endpoint: ['Dot', { radius: 5 }],
        paintStyle: { fill: '#263544' },
        isSource: true,

        connectorStyle: { stroke: '#263544', strokeWidth: 3 },

        connector: ['Flowchart', { stub: [2, 2], gap: 0, cornerRadius: 0, alwaysRespectStubs: true }],
        maxConnections: obj && obj.bot_type && obj.bot_type.toUpperCase() === 'STOREDPROCEDURE' ? 1000 : 1000,
        deleteEndpointsOnDetach: true,

        isTarget: false,
        connectorOverlays: [['Arrow', { location: 1, }]]

        // connectorOverlays: [['Arrow', { location: 1, }], ['Label',
        //   { label: '<a class="px-1 bg-dark rounded" id="'+obj.id+'label'+'"><i class="fa fa-cog text-white"></i></a>', location: 0.5, cssClass: 'setting' }]],



      };
      const Endpoint2 = {
        endpoint: ['Dot', { radius: 5 }],
        paintStyle: { fill: 'red' },
        isSource: false,
        maxConnections: obj && obj.bot_type && obj.bot_type.toUpperCase() === 'STOREDPROCEDURE' || (obj.bot_type === 'END') ? 1000 : 1,
        isTarget: true,
        connectionsDetachable: true,
        deleteEndpointsOnDetach: true,
        detachable: true,
        connectorOverlays: [['Arrow', { location: 1 }]]
        // connectorOverlays: [['Arrow', { location: 1 }], ['Label',
        //   { label: '<a class="px-1 bg-dark rounded"  id="'+obj.id+'label'+'"><i class="fa fa-cog text-white"></i></a>', location: 0.5, cssClass: 'setting' }]],


      };
      if (obj.bot_type !== 'END') {
        this.jsPlumbInstance.addEndpoint(obj.id, { anchor: 'Right', uuid: obj.id + '_right' }, Endpoint1);

      }
      // this.jsPlumbInstance.addEndpoint(obj.id, { anchor: 'Right', uuid: obj.id + '_right' }, Endpoint1);
      if (obj.bot_type === undefined) { obj.bot_type = 'Initial' }
      if (obj && obj.bot_type && obj && obj.bot_type !== 'Initial') {
        this.jsPlumbInstance.addEndpoint(obj.id, { anchor: 'Left', uuid: obj.id + '_left' }, Endpoint2);
      }

      this.jsPlumbInstance.draggable(obj.id, {
        getConstrainingRectangle: () => [99999, 99999],
        containment: true,
        drag: (e) => {
          // Your code comes here
          // this.jsPlumbInstance.repaint($(obj.id)); // Note that it will only repaint the dragged element
        },
        stop: (e) => {
          const k = this.workflow.findIndex(x => x.id === e.el.id);
          this.workflow[k].left = e.finalPos[0] + 'px';
          this.workflow[k].top = e.finalPos[1] + 'px';
          this.workflow[k].y = e.finalPos[1] + 5;
          this.workflow[k].x = e.finalPos[0]
          // Your code for capturing dragged element position.
        }
      });




    }, 200);
  }


  removeDuplicates(data) {
    return data.filter((value, index) => data.indexOf(value) === index)
  }

  saveNodeJson() {
    this.wfBotlistsearch('');
    $('#botSearchInput').val('');

    this.sequenceGeneration = [];
    //debugger;
    const ob = [];
    this.pathConnections = [];
    const allconnections = this.jsPlumbInstance.getAllConnections();
    allconnections.forEach((info, cix) => {
      const sbotId = info.sourceId.split('BOT');
      const tbotId = info.targetId.split('BOT');
      let pwfi = -1;
      let ruleref = '';
      
      if (this.previouswfDetails) {
        pwfi = this.previouswfDetails.connections.findIndex(x => x.id === info.sourceId + '-' + info.targetId);
        if (pwfi !== -1) {
          ruleref = this.previouswfDetails.connections[pwfi].reference;
        
        } else {
          ruleref = '';
        }

      }

      const pc = this.pathConnections.findIndex(x => x.id === info.sourceId + '-' + info.targetId);
      if (pc === -1) {
        this.pathConnections.push({
          id: info.sourceId + '-' + info.targetId, source: info.sourceId, target: info.targetId,
          reference: ruleref ? ruleref : '',
          uuids: [info.sourceId + '_right', info.targetId + '_left'], actionId: null, sourceBotId: sbotId[2], targetBotId: tbotId[2]
        });

      }


      if ((cix + 1) === allconnections.length) {


        this.workflow.forEach((nd, ind) => {
          const obj = {
            nodeId: nd.id,
            target: this.jsPlumbInstance.getConnections({ source: nd.id }),
            source: this.jsPlumbInstance.getConnections({ target: nd.id }),
            scope: this.jsPlumbInstance.getConnections({ scope: nd.id })
          }
          ob.push(obj);
          if ((ind + 1) === this.workflow.length) {
            this.findSequence(ob, 'BOT0BOT0', 1);
            const seqno = 0;
            const con = [];
            this.conSaveDetails = [];
            let initialBot;

            this.pathConnections.forEach((elm, n) => {
              const t = ob.findIndex(x => x.nodeId === elm.target);
              const s = ob.findIndex(x => x.nodeId === elm.source);
              let types = '';
              let typeId;
              let parent = '';
              let parentTgt = '';
              let isPrimary = null;
              let dependent = null;
              let isPrimaryId = null;
              let dependentId = null;
              let parentId = null;
              let mergecon = [];

              if (ob[t] && ob[t].source && ob[t].source.length > 1) {
                types = 'MERGE';
                ob[t].source.forEach((b, is) => {

                  mergecon.push({ conId: b.sourceId + '-' + b.targetId });
                  const k = this.pathConnections.findIndex(x => x.id === b.sourceId + '-' + b.targetId);
                  if (k === -1) { } else {

                    if (is === 0) {
                      this.pathConnections[k]['primary'] = true;
                      this.pathConnections[k]['dependent'] = false;

                    } else {
                      this.pathConnections[k]['dependent'] = true;
                      this.pathConnections[k]['primary'] = false;
                      this.pathConnections[k]['dependentId'] = mergecon[0];
                    }
                  }


                });

              } else if (ob[s] && ob[s].target && ob[s].target.length > 1) {
                types = 'SPLIT';
              } else {
                types = 'SIMPLE';
              }


              const m = this.actionTypes.findIndex(x => x.actiontype_name.toUpperCase() === types);
              if (m === -1) { } else {
                typeId = this.actionTypes[m].actiontype_id;
              }


              if (ob[s] && ob[s].source && ob[s].source.length === 0) {
                initialBot = elm.source;
                parent = null;
                parentTgt = null;
              } else {
                parent = ob[s] && ob[s].source && ob[s].source[0].sourceId ? ob[s].source[0].sourceId : null;
                parentTgt = ob[s] && ob[s].source && ob[s].source[0].targetId ? ob[s].source[0].targetId : null;
              }

              const sbotid = elm.source.split('BOT')[2];

              const tbotid = elm.target.split('BOT')[2];

              const src = this.wfBotList.findIndex(x => x.bot_id === parseInt(sbotid));
              const tgt = this.wfBotList.findIndex(x => x.bot_id === parseInt(tbotid));
              let srcname;
              let tgtname;


              if (src !== -1) { srcname = this.wfBotList[src].bot_name; }
              if (tgt !== -1) { tgtname = this.wfBotList[tgt].bot_name; }

              elm['sourceBot'] = this.wfBotList[src];
              elm['targetBot'] = this.wfBotList[tgt];


              if (parent && parentTgt) {
                parentId = parent + '-' + parentTgt
              } else {
                parentId = null
              }
              if (srcname === undefined) {
                srcname = 'Input';
              }
              if (tgtname === undefined) {
                tgtname = 'Output';
              }
              let seq = '';
              const a = this.sequenceGeneration.findIndex(x => x.sourceTarget === elm.id);
              if (a !== -1) {
                seq = this.sequenceGeneration[a].sequence;
              }
              let isviewoutput = false; 
             if(this.previouswfDetails && this.previouswfDetails.withAction.length > 0){
              const v = this.previouswfDetails.withAction.findIndex(x => x.uniqueconnectionid === elm.source+'-'+elm.target);
              if(v !== -1){
                isviewoutput = this.previouswfDetails.withAction[v].isviewoutput;
                }
             }
            
           
              const objt = {
                sequence: seq,
                actionId: elm.actionId,
                connectionId: elm.id,
                parentId: parentId,
                mergecon: mergecon,
                connectionName: srcname + '-' + tgtname,
                sourceBot: this.wfBotList[src] ? this.wfBotList[src] : {},

                targetBot: this.wfBotList[tgt] ? this.wfBotList[tgt] : {},

                connectionType: types,
                connectionTypeId: typeId,
                isViewOutput: isviewoutput,
                isPrimary: isPrimary,
                dependent: dependent,
                isPrimaryId: isPrimaryId,
                dependentId: dependentId,
                dependentcon: null,
                reference: (elm.reference !== '' && elm.reference) ? elm.reference : '',
                parentCon: ''
              };


              this.conSaveDetails.push(objt);

              if (this.pathConnections.length === (n + 1)) {
                this.conSaveDetails.forEach((em, ix) => {
                  const pc = this.pathConnections.findIndex(x => x.id === em.connectionId);
                  em.isPrimary = this.pathConnections[pc].primary ? this.pathConnections[pc].primary : null;
                  em.dependent = this.pathConnections[pc].dependent ? this.pathConnections[pc].dependent : null;
                  em.dependentId = this.pathConnections[pc].dependentId ? this.pathConnections[pc].dependentId : null;
                  if (em.dependentId && em.dependentId.conId) {

                    let spliter = em.dependentId.conId.split('-');
                    const sbotID = spliter[0].split('BOT')[2];

                    const tbotID = spliter[1].split('BOT')[2];

                    const cfsrc = this.wfBotList.findIndex(x => x.bot_id === parseInt(sbotID));
                    const cftgt = this.wfBotList.findIndex(x => x.bot_id === parseInt(tbotID));
                    let cfsrcname;
                    let cftgtname;


                    if (cfsrc !== -1) { cfsrcname = this.wfBotList[cfsrc].bot_name; }
                    if (cftgt !== -1) { cftgtname = this.wfBotList[cftgt].bot_name; }
                    const oj = {
                      connectionId: em.sourceId + '-' + em.targetId,
                      connectionName: cfsrcname + '-' + cftgtname
                    }


                    em.dependentcon = oj.connectionName;


                  }


                  if (em.parentId) {
                    const pnt = this.conSaveDetails.findIndex(x => x.connectionId === em.parentId);
                    em.parentCon = this.conSaveDetails[pnt].connectionName;
                  }
                  if ((ix + 1) === this.conSaveDetails.length) {
                    this.conSaveDetails.sort(this.GetSortOrder("sequence"));
                  }
                });

              }

            });
          }

        });
        let sLen = 0; let tLen = 0;
        ob.forEach(st => {
          if (st.source.length === 0) {
            sLen = sLen + 1;
          }
          if (st.target.length === 0) {
            tLen = tLen + 1;
          }
        });

        if (sLen >= 2 || tLen >= 2) {
          this.Errormessage('Please check the connections. Some bots are not connected properly.');
          return;
        }
        const clonerwf = [...this.workflow];
        const clonerwflist = [...this.workflowList];
        const datas = {
          id: this.cwflow.workflowid,
          nodeId: this.cwflow.nodeId,
          workflow: this.workflowName,
          description: this.cwflow.description,
          createdDate: this.cwflow.createdDate,
          updatedDate: this.cwflow.updatedDate,
          steps: this.pathConnections.length,
          nodes: clonerwf,
          connections: this.pathConnections,
          connectionPathDetails: this.conSaveDetails,


        };

        if (this.workflow.length !== 0) {
          $('#consaveDetail').modal('show');
        } else {
          this.Errormessage('Please Create the Workflow');
        }


        this.wfBotList.forEach((val, ind) => {


          this.botTypesClone3.forEach(c => {
            c.categories.forEach(b => {
              b.bots.forEach(bt => {
                if (bt.bot_id === val.bot_id) {

                  val.bot_type_id = c.bot_type_id;
                  val.bot_type = c.bot_type;

                }
              })
            })
          })

        });


        this.currentWfDetails = {
          workflowid: this.cwflow.workflowid,
          workflowdiagram: datas,
          workflowbots: this.wfBotList
        };
      }
    });



  }
  GetSortOrder(prop) {
    return function (a, b) {
      if (a[prop] > b[prop]) {
        return 1;
      } else if (a[prop] < b[prop]) {
        return -1;
      }
      return 0;
    }
  }

  findSequence(ob, bot, num) {
    const xi = ob.findIndex(x => x.nodeId === bot);
    if (xi !== -1) {

      this.sequenceGen(ob, ob[xi].target, num, bot);

    }
  }

  sequenceGen(ob, targets, num, bot) {
    const tg = [];
    if (targets.length > 0) {
      targets.forEach((el, i) => {
        const ix = this.sequenceGeneration.findIndex(x => x.sourceTarget === bot + '-' + el.targetId);
        if (ix === -1) {

          const xi = ob.findIndex(x => x.nodeId === el.targetId);

          this.sequenceGeneration.push({ sequence: num, sourceTarget: bot + '-' + el.targetId });
          this.findSequence(ob, el.targetId, num + 1);

        }


      })
    }

  }
  isViewoutputCheck(e, data) {
    data = e.checked;

  }

  toggleviewOp(val) {
    if (val) {
      val = false;
    } else {
      val = true;
    }
  }
  successmessage(message) {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: message });
  }

  Errormessage(errorsmessage) {
    if (this.checksticky === true) {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage, sticky: true });
      this.checksticky = false;
    } else {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
    }
  }
  selectworkflow(wf, reuse) {
    // debugger;
    $('.spinner').show();
    try {
      if (!reuse) {
        this.workflowServices.getRequest('actiontypes').subscribe((Response) => {
          this.actionTypes = Response.data;
         
        });
      }
      this.workflowServices.getRequest('workflows/' + wf.workflowid).subscribe((Response) => {
        if (Response.status === 'Success') {
          if (!reuse) {
            this.exselectedBotList = [];
            this.selectedBotList = Response.data.workflowbots;

            this.loadPrevious(Response.data.workflowdiagram, reuse);
            this.previouswfDetails = Response.data.workflowdiagram;
            
          } else {
            const workflowbots = Response.data.workflowbots;
            Response.data.workflowdiagram.nodes.forEach((el, ui) => {
              const botIx = workflowbots.findIndex(x => x.bot_id === el.botId);
              let bot: any;
              if (botIx === -1) {
                // this.wfBotList.push(workflowbots[botIx])
                bot = {};
              } else {
                bot = workflowbots[botIx];

              }
              const wix = this.wfBotList.findIndex(x => x.bot_id === el.botId);
              if (wix === -1) { if (el.botId !== 0) { this.wfBotList.push(bot) } }
              this.addtoWorkflow(bot, el, Response.data.workflowdiagram, ui);
              if (Response.data.workflowdiagram.nodes.length === ui + 1) {

                this.createConnections(Response.data.workflowdiagram, 'REUSE');
              }
              
            })

          }


        }
      });
    } catch (e) {
      console.log(e);
      
      $('.spinner').hide();
    }

  }
  loadPrevious(data, reuse) {
$('.spinner').show();

    if (this.isbindclickevent === 0) {
      this.isbindclickevent = 1;

      this.bindclickandConnectionEvents();
    }

    try {
      if (data.nodes.length === 0) {
        if (this.isbindclickevent === 0) {
          this.isbindclickevent = 1;

          this.bindclickandConnectionEvents();
        }
        this.initialbot();

      } else {
        this.previousCount++;
        data.nodes.forEach((elem, i) => {
          let highest;
          if (this.workflow.length > 0) {
            highest = Math.max.apply(Math, this.workflow.map((x) => {
              return x.y;
            }));
          } else {
            highest = 0;
          }

          if (reuse) {
            this.top = highest + elem.y;
          } else {
            this.top = elem.y;
          }

          const obj = {
            id: elem.id,
            cName: elem.cName,
            nodeId: elem.nodeId,
            botId: elem.botId,
            bot_type: elem.bot_type,
            bot_type_id: elem.bot_type_id,
            left: elem.left,
            top: this.top + 'px',
            y: this.top,
            x: elem.x,
            active: false,
            wfgroupId: this.previousCount,
          };
          if (reuse && (elem.bot_type_id === 0 || !elem.bot_type_id)) {
            obj.id = 'P' + elem.id;
          }
          this.workflow.push(obj);

          this.loadJsplump(obj, reuse);

          if ((i + 1) === data.nodes.length) {

            this.createConnections(data, this.previousCount);
          }
          setTimeout(() => {
            this.dragselect(obj);
          }, 300);


        });
      }
      $('.spinner').hide();
    }
    catch (e) {
      if (this.isbindclickevent === 0) {
        this.isbindclickevent = 1;

        this.bindclickandConnectionEvents();
      }
      this.initialbot();
      $('.spinner').hide();
    }


  }


  bindclickandConnectionEvents() {
    this.jsPlumbInstance.bind('click', (con) => {
      $('.spinner').show();
      // debugger;

      // empty the rule table. 
      this.messageService.clear();
      this.operationTypesTable = [];
      // hide error Json validator
      $('.notjson').hide();

      this.currentConnection = con.sourceId + '-' + con.targetId;

      if (this.existingWorkflow.withAction === undefined) {
        this.existingWorkflow = this.existingWorkflow.workflowdiagram;
      }

      const a = this.existingWorkflow.withAction.findIndex(x => x.uniqueconnectionid === this.currentConnection);

      this.currentActionid = this.existingWorkflow.withAction[a].actionid;

      if (a === -1) { } else {
        // open the pop up modal
        if (this.currentActionid !== null) {

          $('#conDetails').modal('show');
          this.currentconnectionName = this.existingWorkflow.withAction[a].actionname;
          const k = this.pathConnections.findIndex(x => x.id === con.sourceId + '-' + con.targetId);
          const s = this.workflow.findIndex(x => x.id === con.sourceId);
          const t = this.workflow.findIndex(x => x.id === con.targetId);
          // connector click
          this.aMappingformVal.sourceBot = this.workflow[s].cName;
          this.aMappingformVal.targetBot = this.workflow[t].cName;
          this.aMappingformVal.actionName = this.workflow[s].cName + this.workflow[t].cName;
          this.ruleformVal.sourceBot = this.workflow[s].cName;
          this.ruleformVal.targetBot = this.workflow[t].cName;
          this.ruleformVal.actionName = this.workflow[s].cName + this.workflow[t].cName;
          this.opMapformVal.targetBot = this.workflow[t].cName;
          this.opMapformVal.actionName = this.workflow[s].cName + this.workflow[t].cName;

           // empty the search box value
           this.srchInAtrValue = '';
           this.srchOutAtrValue = '';
           $('#inputsearch,#outputsearch').val('');
           $('.uAttribute,.sAttribute').text('(' + this.workflow[s].cName + ')');
           $('.tAttribute').text('(' + this.workflow[t].cName + ')');
           // $('#attrMapping-tab').trigger('click');
           this.opMapformVal.outputFname = '';
           this.submitted = false;
           const formDatas = new FormData();
           formDatas.append('workflowid', this.cwflow.workflowid);
           formDatas.append('sourcebotid', this.workflow[s].botId);
           formDatas.append('targetbotid', this.workflow[t].botId);

          this.workflowServices.getRequest('bots/' + this.workflow[t].botId).subscribe(resp => {
            // debugger;
            this.targetAttributesClone1 = [];
            this.sourceAttributesclone = [];
            for (let i = 0; i <= resp.data.output_attributes.length - 1; i++) {
              this.sourceAttributesclone.push({
                outputpropertyname: resp.data.output_attributes[i].attributename,
                propertydisplayname: resp.data.output_attributes[i].attributename,
                botattributeid: resp.data.output_attributes[i].botattributeid,
                isderived: resp.data.output_attributes[i].isderived,
                isstatic: resp.data.output_attributes[i].isstatic,
                displayorder: i
              });
            }
            this.sourceAttributesclone2 = JSON.parse(JSON.stringify(this.sourceAttributesclone));
            this.workflowServices.postRequest('botattributes', formDatas).subscribe((response) => {
              // ;
              if (response.status === 'Success') {
                this.targetAttributesClone = [];
                const datas = response.data[0];
                this.userAttributes = datas[0].user_attributes;
                this.sourceAttributes = datas[0].source_bot.output_attributes;
  
                this.targetBotAttributesList = datas[0].target_bot.Input_attributes;
                this.targetAttributes = this.targetBotAttributesList.normal_attributes;
  
                for (let i = 0; i <= this.targetAttributes.length - 1; i++) {
                  this.targetAttributesClone.push({
                    attribute_id: this.targetAttributes[i].attribute_id, attribute_name: this.targetAttributes[i].attribute_name,
                    label: this.targetAttributes[i].attribute_name, value: this.targetAttributes[i]
                  });
                }
                this.targetAttributesClone1 = this.targetAttributesClone;
                let split = con.targetId.split('BOT');
          
                const sp = this.selectedBotList.findIndex(x => x.bot_id === parseInt(split[2]))
      
                if (this.selectedBotList && this.selectedBotList[sp] && this.selectedBotList[sp].spbottype) {
                  if (this.selectedBotList[sp].spbottype === 'BatchConfig') {
                    
                    this.report_properties = [];
                    this.exreportproperties = [];
                    this.previousOutputMapping.input = [];
                    this.previousOutputMapping.output = [];
                    $('#output-tab').trigger('click');
                    this.getoutputmappingDetails();
                    $('#attrMapping-tab').hide();
                  } else {
                    this.report_properties = [];
                    this.exreportproperties = [];
                    this.previousOutputMapping.input = [];
                    this.previousOutputMapping.output = [];
                    $('#attrMapping-tab').show();

                    $('#attrMapping-tab').trigger('click');
                    this.GetattributemappingDetails('NORMAL');
                  }
                } else {
                  this.report_properties = [];
                  this.exreportproperties = [];
                  this.previousOutputMapping.input = [];
                  this.previousOutputMapping.output = [];
                  $('#attrMapping-tab').show();
                  $('#attrMapping-tab').trigger('click');
                  this.GetattributemappingDetails('NORMAL');
                }
               
  
              } else {
                this.Errormessage(response.message);
              }
            });
          },err =>{
            this.Errormessage(err.message);
          });

        
 
 
 
         
 
           
           
         

         

        }
      }
    });
    // connector click end
    // connection bind start
    this.jsPlumbInstance.bind('connection', (info) => {

      const sbotId = info.sourceId.split('BOT');
      const tbotId = info.targetId.split('BOT');
      // this.currentConnection = info.sourceId + '-' + info.targetId;
      const pci = this.pathConnections.findIndex(x => x.id === info.sourceId + '-' + info.targetId);

      var con = info.connection;
      var arr = this.jsPlumbInstance.select({ source: con.sourceId, target: con.targetId });
      if (arr.length > 1) {
        this.jsPlumbInstance.deleteConnection(con);
        return;
      }

      let pwfi = -1;

      if (this.previouswfDetails !== null) {
        pwfi = this.previouswfDetails.connections.findIndex(x => x.id === info.sourceId + '-' + info.targetId);

      }
      let ruleref = '';
      if (pwfi !== -1) {
        if (pci === -1) {
          if (this.previouswfDetails !== null) {
            ruleref = this.previouswfDetails.connections[pwfi].reference;

          }
        } else {
          ruleref = '';

        }




      }

      if (pci === -1) {


        this.pathConnections.push({
          id: info.sourceId + '-' + info.targetId, source: info.sourceId, target: info.targetId,
          reference: ruleref ? ruleref : '',
          uuids: [info.sourceId + '_right', info.targetId + '_left'], actionId: null, sourceBotId: sbotId[2], targetBotId: tbotId[2]
        });
      } else {
        // this.jsPlumbInstance.deleteConnection(info.connection);
      }



      try {
        $('#RDM-' + info.sourceId + '-' + info.targetId).contextmenu((event) => {

          let botids = event.currentTarget.id.split('-');
          const stid = botids[1] + '-' + botids[2];
          const pcix = this.pathConnections.findIndex(x => x.id === stid);

          if (pcix === -1) {
            this.isEditRule = false;
          } else {
            let ruleref = this.pathConnections[pcix].reference;
            if (ruleref !== '' && ruleref) { this.isEditRule = true; } else { this.isEditRule = false; }
          }
          this.currentEvent = event;
          this.cm.top = event.pageY + 'px';
          this.cm.left = event.pageX + 'px';
          this.hidecontextMenu = true;
          this.hidecontextMenuRule = false;
          this.showTxtboxRuleIdentity = false;
          return false;
        });
      } catch (e) {
        console.log(e);
      }




    });


    // connector detach event catcher
    // this.jsPlumbInstance.bind("connectionDetached", (info, originalEvent) => {
    //   console.log(info, 'detached');

    //   const sbotId = info.sourceId.split('BOT');
    //   const tbotId = info.targetId.split('BOT');
    //   const pwfi = this.pathConnections.findIndex(x => x.id === info.sourceId + '-' + info.targetId);


    //   // if (pwfi !== -1) {
    //   //   this.pathConnections.splice(pwfi, 1);

    //   // }

    // });

  }

  getRuleDetails() {
    this.aMappingformValuserAttribute = '';
    this.aMappingformValsourceAttribute = '';
    this.aMappingformValtargetAttribute = '';
    this.workflowServices.getRequest('operationtypes/' + this.currentActionid).subscribe((Response) => {

      this.operationTypes = Response.data;

      this.getrulemappingDetails();



    });
  }
  selOperationType(obj) {
    this.suggestRules = obj.rule_suggestions;
    const b = this.suggestRules.findIndex(x => x.rulename === 'Create new');
    if (b === -1) {
      this.suggestRules.push({ rulename: 'Create new', rulejson: '' })
    }
    this.ruleJson = '';
    $('#textAreaRule').attr({ 'disabled': false })

  }

  selectSuggestRule(rule) {

    if (rule.rulename === 'Create new') {
      this.isNewRule = true;
      this.newrulename = '';
      this.ruleJson = '';
      $('#textAreaRule').attr({ 'disabled': false })

    } else {
      this.isNewRule = false;
      this.ruleJson = rule.rulejson;
      this.rulename = rule.rulename;
      $('#textAreaRule').attr({ 'disabled': true })
    }

  }

  fullscreen() {
    const el = document.getElementById('fullscreen');
    try {
      // el.webkitRequestFullscreen();
      // el.mozRequestFullScreen();
      // el.msRequestFullscreen();
      // finally the standard version
      el.requestFullscreen();
    } catch (e) {
      // finally the standard version
      el.requestFullscreen();
    }
    // use necessary prefixed versions



  }

  createConnections(data, reuse) {

    setTimeout(() => {
      data.connections.forEach(elem => {
        let dom;
        if (reuse === 'REUSE' || !elem.actionId) {
          dom = '';
        } else {
          let classname = '';
          let ref = '';
          if (elem.reference !== '' && elem.reference) {
            classname = '';
            ref = elem.reference;

          } else {
            classname = 'd-none';
          }
          let id = '#RULERDM-' + elem.source + `-` + elem.target;
          dom = `<div class="` + classname + `"  style="
          position: absolute;
          max-width: 80px;
          padding: 2px 7px;
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
          transform: translate(-50%, -50%);
          text-align: center;
          box-shadow: 2px 2px 2px #ccc;
          background: rgb(0 0 0 / 0.62);
          color: #ffffff;
          border-radius: 3px;
          bottom: 16px;
          left: 7px;
          font-size: 10px;
          id="`+ id + `" >` + elem.reference + `</div>
          <span class="px-1 bg-dark rounded context-menu-click" style="padding:2px;" id="RDM-` + elem.source + `-` + elem.target + `">
          <i class="fa fa-cog text-white ` + elem.source + elem.target + `"></i></span>`
        }

        this.jsPlumbInstance.connect({
          uuids: [elem.source + '_right', elem.target + '_left'], overlays: [
            ['Label', {
              label: dom,
              location: 0.5, cssClass: 'setting'
            }]
          ],
        });


      });
    }, 300);


  }

  selectRow(isViewOutput) {
    if (isViewOutput) {
      isViewOutput = false;
    } else {
      isViewOutput = true;
    }
  }

  save() {
    // debugger;
    $('.spinner').show();
    $('#confirmBtn').addClass('event-none');
    const conData = [];

    let newconnector = [];
    this.conSaveDetails.forEach(elm => {

      if (this.existingWorkflow && this.existingWorkflow.withAction) {
        const acid = this.existingWorkflow.withAction.findIndex(x => x.uniqueconnectionid === elm.connectionId);
        const pacid = this.existingWorkflow.withAction.findIndex(x => x.parentuniqueconnectionid === elm.connectionId);
        if (acid === -1) {
          elm.actionId = null;
          if (!elm.connectionId.includes('BOT0.1BOT0.1')) {
            newconnector.push(elm)
          }
        } else {
          elm.actionId = this.existingWorkflow.withAction[acid].actionid;
        }
      } else {
        elm.actionId = null;
        if (!elm.connectionId.includes('BOT0.1BOT0.1')) {
          newconnector.push(elm)
        }

      }
      if (!elm.connectionId.includes('BOT0.1BOT0.1')) {
        conData.push({
          uniqueconnectionid: elm.connectionId,
          actionid: elm.actionId,
          actionname: elm.connectionName,
          workflowid: this.currentWfDetails.workflowid,
          sequence: elm.sequence,
          parentactionname: elm.parentCon,
          parentactionnameid: null,
          mergecon: elm.mergecon,
          parentuniqueconnectionid: elm.parentId ? elm.parentId : null,
          sourcebotid: elm.sourceBot.bot_id ? elm.sourceBot.bot_id : 0,
          // sourcebottypename: elm.sourceBot.bot_type,
          sourcebottypeid: elm.sourceBot.bot_type_id ? elm.sourceBot.bot_type_id : 0,
          targetbotid: elm.targetBot.bot_id,
          targetbottypeid: elm.targetBot.bot_type_id,
          targetbottype: elm.targetBot.bot_type,
          actiontype: elm.connectionType,
          actiontypeid: elm.connectionTypeId,
          isviewoutput: elm.isViewOutput,
          isactive: true,
          isprimary: elm.isPrimary,
          dependantuniqueconnectionid: elm.dependentId && elm.dependentId.conId ? elm.dependentId.conId : '',
          dependantactionid: null,
          dependantactionname: elm.dependentcon ? elm.dependentcon : '',
          commonpropertyid: null,
          isinputtrackingrequired: false
        });
      }

    });



    const details = {
      action: conData,
      removedconnector: [],
      newConnector: newconnector
    };
    let wfapi = '';
    if (this.previouswfDetails && this.previouswfDetails.withAction) {

      this.previouswfDetails.withAction.forEach(ac => {
        const id = conData.findIndex(x => x.actionid === ac.actionid);
        if (id === -1) {
          details.removedconnector.push(ac);
        }
      });

      if (this.previouswfDetails.withAction.length) {
        wfapi = 'update_action';
        // delete details.newConnector;
      } else {
        wfapi = 'action';
      }
    } else {
      wfapi = 'action';
    }


    const formData = new FormData();
    formData.append('details', JSON.stringify(details));



    this.workflowServices.postRequest(wfapi, formData).subscribe((response) => {
      if (response.code === 200 || response.code === '200') {
        this.clear(0);
        this.currentWfDetails.workflowdiagram['withAction'] = response.data;
        this.existingWorkflow = this.currentWfDetails.workflowdiagram;

        response.data.forEach(ac => {
          const aid = this.pathConnections.findIndex(x => x.id === ac.uniqueconnectionid);
          const acid = this.currentWfDetails.workflowdiagram.connections.findIndex(x => x.id === ac.uniqueconnectionid);
          if (aid === -1) { } else {
            this.pathConnections[aid].actionId = ac.actionid;
          }
          if (acid === -1) { } else {
            this.currentWfDetails.workflowdiagram.connections[acid].actionId = ac.actionid;
          }
        });

        const formDatas = new FormData();
        formDatas.append('details', JSON.stringify(this.currentWfDetails));
        this.workflowServices.postRequest('Update_workflow', formDatas).subscribe((responses) => {
          if (responses.status === 'Success') {
            // debugger;
            $('#consaveDetail').modal('hide');
           
            
            this.conSaveDetails = [];
            this.pathConnections = [];
            this.selectworkflow(this.cwflow, 0);
            this.successmessage('Workflow Updated successfully');
            $('#confirmBtn').removeClass('event-none');
            // $('.spinner').hide();
          } else {
            this.Errormessage(responses.message);
            $('#confirmBtn').removeClass('event-none');
            $('.spinner').hide();
          }
        }, err => {
          $('#confirmBtn').removeClass('event-none');
          console.log(err);
          this.Errormessage(err.message);
          $('.spinner').hide();
        });

      }else{
        this.Errormessage(response.message);
        $('#confirmBtn').removeClass('event-none');
        $('.spinner').hide();
      }
    });




  }

  clear(val) {
    // ;
    this.workflow.forEach((elem, i) => {
      if (elem.id !== 0) {
        this.jsPlumbInstance.remove(elem.id);
        if (i + 1 === this.workflow.length) {
          this.workflow = [];
          this.pathConnections = [];

        }

      }

    });

    //  this.jsPlumbInstance.reset();
    this.top = 0;
    // let wf = [];
    //  const y = this.workflow.findIndex(x => x.id === 0);
    //   if(y === -1){}else{
    //     wf.push(this.workflow[y])
    //   }
    // this.workflow = [];
    // // this.workflow = wf;

    // this.pathConnections = [];
    if (val) {
      this.initialbot();
    }

    //  this.conSaveDetails = [];



  }
  gotoWfC() {
    $('#nav-home-tab').trigger('click');

  }


  nextToWorkflow(selectedWF: any) {
$('.spinner').show();
    if (this.selectedBotList.length !== 0) {
      this.commonSearchValue = '';
      this.wfBotList = this.selectedBotList;
      this.wfBotListCloner = [...this.wfBotList];
      this.pathConnections = [];
      if (selectedWF.workflowbots === null) {
        const datas = {
          workflowid: selectedWF.workflowid,
          workflowdiagram: this.existingWorkflow === null ? null : this.existingWorkflow.workflowdiagram,
          workflowbots: this.wfBotList
        };
        const formDatas = new FormData();
        formDatas.append('details', JSON.stringify(datas));

        this.workflowServices.postRequest('Update_workflow', formDatas).subscribe((response) => {
          if (response.status === 'Success') {
            $('#nav-contact-tab').trigger('click');
            this.selectworkflow(selectedWF, 0);
            // this.successmessage(response.message);
            $('.spinner').hide();
          } else {
            $('.spinner').hide();
            this.Errormessage(response.message);
          }
        });
      } else {
        if (this.exselectedBotList.length !== 0) {
          const datas = {
            workflowid: selectedWF.workflowid,
            workflowdiagram: this.existingWorkflow,
            workflowbots: this.wfBotList
          };
          const formDatas = new FormData();
          formDatas.append('details', JSON.stringify(datas));
          this.workflowServices.postRequest('Update_workflow', formDatas).subscribe((response) => {
            if (response.status === 'Success') {
              $('#nav-contact-tab').trigger('click');
              this.selectworkflow(selectedWF, 0);
              // this.successmessage(response.message);
              $('.spinner').hide();
            } else {
              $('.spinner').hide();
              this.Errormessage(response.message);
            }
          });
        } else if (this.selectedbotlength !== this.selectedBotList.length) {
          const datas = {
            workflowid: selectedWF.workflowid,
            workflowdiagram: this.existingWorkflow,
            workflowbots: this.wfBotList
          };
          const formDatas = new FormData();
          formDatas.append('details', JSON.stringify(datas));
          this.workflowServices.postRequest('Update_workflow', formDatas).subscribe((response) => {
            if (response.status === 'Success') {
              $('#nav-contact-tab').trigger('click');
              this.selectworkflow(selectedWF, 0);
              $('.spinner').hide();
              // this.successmessage(response.message);
            } else {
              $('.spinner').hide();
              this.Errormessage(response.message);
            }
          },err =>{
            $('.spinner').hide();
            this.Errormessage(err.message);
          });
        } else {
          $('#nav-contact-tab').trigger('click');
          this.selectworkflow(selectedWF, 0);
        }
        // if (this.selectedbotlength !== this.selectedBotList.length) {
        // } else {
        //   $('#nav-contact-tab').trigger('click');
        //   this.selectworkflow(selectedWF);
        // }
      }
    } else {
      $('.spinner').hide();
      this.Errormessage('Please Select the Bots');
    }

  }

  onRightClick($event, wfbot, idx) {
    this.hidecontextMenu = false;
    this.hidecontextMenuRule = true;
    this.cm.left = $event.pageX + 'px';
    this.cm.top = $event.pageY + 'px';
    this.cm.removeIndex = idx;
    this.cm.wfbot = wfbot;
    if (this.cm.wfbot.id === 'BOT0BOT0' || this.cm.wfbot.id === 'BOT0.1BOT0.1') {
      this.isremoveShow = false;
    } else {
      this.isremoveShow = true;
    }




    return false;
  }
  addruleIdentity() {
    this.hidecontextMenu = true;
    this.hidecontextMenuRule = true;
    this.showTxtboxRuleIdentity = true;
    $('#refTxt').val('').focus();
  }
  editruleIdentity() {
    this.showTxtboxRuleIdentity = true;
    this.hidecontextMenu = true;
    this.hidecontextMenuRule = true;


    setTimeout(() => {
      $('#refTxt').val($(this.currentEvent.target.offsetParent.childNodes[0]).html()).focus();
    });


  }
  delruleIdentity() {
    this.showTxtboxRuleIdentity = false;
    this.hidecontextMenu = true;
    this.hidecontextMenuRule = true;

    let botids = this.currentEvent.currentTarget.id.split('-');
    const stid = botids[1] + '-' + botids[2];
    const pcix = this.pathConnections.findIndex(x => x.id === stid);
    if (pcix !== -1) {
      this.pathConnections[pcix].reference = '';
      let id = '#RULERDM-' + botids[1] + '-' + botids[2];
      id = id.toLowerCase()
      $(this.currentEvent.target.offsetParent.childNodes[0]).addClass('d-none');
    }


  }

  setReference() {
    let ref = $('#refTxt').val();
    $('#refTxt').val('');
    this.hidecontextMenu = true;
    this.hidecontextMenuRule = true;
    this.showTxtboxRuleIdentity = false;
    if (ref !== '') {
      $(this.currentEvent.target.offsetParent.childNodes[0]).html(ref).attr('title', ref).removeClass('d-none');
      const botid = this.currentEvent.currentTarget.id.split('-');

      const fi = this.pathConnections.findIndex(x => x.id === botid[1] + '-' + botid[2])
      if (fi !== -1) {
        this.pathConnections[fi].reference = ref;
      }
    }


  }
  removeConnection() {
    const botid = this.currentEvent.currentTarget.id.split('-');
    this.hidecontextMenu = true;
    this.hidecontextMenuRule = true;
    this.showTxtboxRuleIdentity = false;
    const conn = this.jsPlumbInstance.getConnections({
      //only one of source and target is needed, better if both setted
      source: botid[1],
      target: botid[2]
    });

    if (conn[0]) {
      this.jsPlumbInstance.deleteConnection(conn[0]);
    }
    // this.jsPlumbInstance.deleteConnection(this.currentEvent.currentTarget.id);
  }


  dragselect(wf) {
    this.hidecontextMenu = true;
    if (wf.active) {
      this.jsPlumbInstance.removeFromDragSelection(wf.id);
      wf.active = false;
    } else {
      this.jsPlumbInstance.addToDragSelection(wf.id);
      wf.active = true;
    }
  }
  deselectAll(e) {
    const f = this.workflow.find(x => x.id === e.srcElement.id);
    if (f === undefined) {
      this.workflow.forEach(ele => {
        this.jsPlumbInstance.removeFromDragSelection(ele.id);
        ele.active = false;
      });
    } else {

    }
    this.showTxtboxRuleIdentity = false;
    this.hidecontextMenu = true;
    this.hidecontextMenuRule = true;

  }


  removeSingle(idx) {
    let removeCon = [];
    this.pathConnections.forEach((em, n) => {
      if ((em.source === this.workflow[idx].id) || (em.target === this.workflow[idx].id)) {
        removeCon.push(em.id);
      }
      if (this.pathConnections.length === (n + 1)) {
        removeCon.forEach(el => {
          const ci = this.pathConnections.findIndex(x => x.id === el);
          if (ci === -1) { } else {
            this.pathConnections.splice(ci, 1);
          }
        });
      }
    });

    this.jsPlumbInstance.remove(this.workflow[idx].id);

    this.workflow.splice(idx, 1);

    this.hidecontextMenu = true;
  }
  removeAll(gid) {
    this.hidecontextMenu = true;
    const clonerwf = [...this.workflow];
    const wfArray = [];
    clonerwf.forEach((el, i) => {
      if (el.wfgroupId === gid) {
        wfArray.push(el.id);
      }
      if (clonerwf.length === i + 1) {
        setTimeout(() => {
          wfArray.forEach((id, ix) => {
            const k = this.workflow.findIndex(x => x.id === id);
            if (k === -1) { } else {
              this.jsPlumbInstance.remove(this.workflow[k].id);
              this.workflow.splice(k, 1);
            }
            if (ix === (wfArray.length - 1)) {
              if (this.workflow.length === 0) {
                this.initialbot();
              }
            }
          });
        }, 0);

      }
    });




  }
  selectedbotType(selbotType) {
    this.commonSearchValue = '';

    this.selectedBotType = selbotType;

    this.botList = this.selectedBotType.categories;
    this.currentBotTypeId = this.selectedBotType.bot_type_id;
    this.botListCloner = [...this.botList];
    Object.assign(this.botListCloner2, [...this.botList]);

  }

  botCommonSearch(val) {
    const botListclone = JSON.parse(JSON.stringify(this.botListCloner2));
    botListclone.forEach((obj, j) => {

      obj.bots = obj.bots.filter(a => {
        return a.bot_name.toLowerCase().includes(val.toLowerCase());
      });
      if ((j + 1) === botListclone.length) {
        this.botList = botListclone.filter(elm => {
          if (elm.bots.length) {
            for (let i = 0; i < elm.bots.length; i++) {
              if (elm.bots[i].bot_name.toLowerCase().includes(val.toLowerCase())) {
                return true;
              } else {
                return false;
              }
            }
          } else {
            return true;
          }

        });
      }
    });

  }
  wfBotlistsearch(val) {

    this.wfBotList = this.wfBotListCloner.filter((obj) => {
      return obj.bot_name.toLowerCase().includes(val.toLowerCase());
    });
  }

  botTypeFilter(typeId) {

    if (typeId === null) {
      this.botList = this.botListCloner;
    } else {
      this.botList = this.botListCloner.reduce((acc, a) => {
        const ch = a.botList && a.botList.filter(b => b.botTypeId === typeId);
        if (ch && ch.length) acc.push({ ...a, botList: ch });
        // else if(a.categroyName.includes('val')) acc.push({ categroyName: a.categroyName });
        return acc;
      }, []);

    }

  }


  attributeJsonFile(event) {

    let dataJ;
    try {
      if (event.target.files.length === 1) {
        const reader = new FileReader();
        reader.readAsText(event.target.files[0], 'UTF-8');
        reader.onload = (evt) => {
          const target: any = evt.target;
          const content: string = target.result;
          dataJ = JSON.parse(content);
          this.jsonUploadData = dataJ.data;
          this.appendtoRuleTable();
        };


      }
    } catch (e) {
      console.log(e);
    }

  }

  appendtoRuleTable() {
    this.jsonUploadData.forEach((element, i) => {
      this.ruleTable.push({
        id: Math.floor(Math.random() * 1000),
        userAttribute: element.userAttribute,
        sourceAttribute: element.sourceAttribute,
        targetAttribute: element.targetAttribute
      });
    });
  }

  uploadfile(e) {
    try {
      this.fileName = e.target.files[0].name;
      const formData = new FormData();
      formData.append('file', e.target.files[0]);
      formData.append('fileName', e.target.files[0].name);
      this.formValues.formData = formData;
      this.workflowDetailsForm.patchValue({ InputFile: { input: formData } });
    } catch (e) {
      console.log('not uploaded');
    }

  }

  uploadBulk(e, identity) {
    // this.pleaseWait = true;
    try {

      this.fileName = e.target.files[0].name;
      let output;
      import('xlsx').then(xlsx => {
        let workBook = null;
        let jsonData = null;
        const reader = new FileReader();
        // const file = ev.target.files[0];
        reader.onload = (event) => {
          const data = reader.result;
          workBook = xlsx.read(data, { type: 'binary' });
          jsonData = workBook.SheetNames.reduce((initial, name) => {
            const sheet = workBook.Sheets[name];
            initial[name] = xlsx.utils.sheet_to_json(sheet);
            return initial;
          }, {});
          output = jsonData[Object.keys(jsonData)[0]];
          if (identity) {
            this.addBulkAttributeList(output);
          } else {
            this.addBulkAttributeValues(output);
          }

        };
        reader.readAsBinaryString(e.target.files[0]);
      });

    } catch (e) {
      // this.pleaseWait = false;
    }

  }

  addBulkAttributeValues(data) {

    let inputString = '';
    data.forEach((elem, i) => {
      inputString += (i === 0 ? '' : ', ') + elem.value;
    });
    this.formValues.inputValues = inputString;
    // this.pleaseWait = false;
  }

  addBulkAttributeList(data) {

    this.formValues.inputValues = [];
    data.forEach(elem => {

      let inputvalues = elem['COMMA SEPERATED'] ? elem['COMMA SEPERATED'] : '';
      inputvalues = inputvalues.replace(/^[,\s]+|[,\s]+$/g, '');
      inputvalues = inputvalues.replace(/\s*,\s*/g, ',');
      let curAttributeType;

      const a = this.attributeElemTypes.findIndex(x => x.attributeTypeName.toUpperCase() === elem.TYPE);
      if (a === -1) { } else {
        curAttributeType = this.attributeTypes[a];

      }

      const obj = {
        id: Math.floor(Math.random() * 1000),
        attributeName: elem['ATTRIBUTE NAME'],
        displayName: elem['DISPLAY NAME'],
        itemName: elem['DISPLAY NAME'],
        selAttrTypes: curAttributeType,

        inputValues: inputvalues ? inputvalues.split(',') : '',
        min: elem.MIN ? elem.MIN : '',
        max: elem.MAX ? elem.MAX : '',
        dateFrom: elem['DATE FROM'] ? elem['DATE FROM'] : '',
        dateTo: elem['DATE TO'] ? elem['DATE TO'] : '',
        dateFormat: elem['DATE FORMAT'] ? elem['DATE FORMAT'] : '',
        selected: false
      };
      this.formValues.inputValues.push(obj);

    });
    // this.pleaseWait = false;
  }

  clearattributemapping() {
    // debugger;
    this.isNewRule = false;
    this.ruleformVal.selectedRule = '';
    this.report_properties = [];
    this.aMappingformValuserAttribute = '';
    this.aMappingformValsourceAttribute = '';
    this.aMappingformValtargetAttribute = '';
    this.opMapformVal.outputFname = '';
    this.aMappingformVal.sequence = '';
    this.aMappingformVal.prefix = '';
    this.aMappingformVal.deliminator = '';
    this.aMappingformVal.suffix = '';
    this.aMappingformVal.attributeValue = '';
    this.disableuserAttributeDropdown = false;
    this.disablesourceAttributesDropdown = false;
    this.ShowMessage = true;
    this.ruleformVal.operationType = '';
    this.aMappingTable.normal = [];
    this.aMappingTable.static = [];
    this.aMappingTable.derived = [];
    this.exaMappingTable.normal = [];
    this.exreportproperties = [];
    // this.attributeSelect(this.attributeTypes[0]);
    // this.aMappingformVal.attributeType = this.attributeTypes[0];
    this.showoutputtableerror = false;
    this.ruleformVal.operationType = '';
    this.ruleJson = '';
    this.showruletableerror = false;
    this.messageService.clear();
    //this.ShowMessage = true;
  }

  attributemapping() {
    // debugger;
    $('.spinner').show();
    $('#attMappingSaveBtn').addClass('event-none');

    this.ShowMessage = true;
    if (this.curntAttrType === 'NORMAL') {
      // this.curntAttrType = 'false';

      if (this.targetAttributesClone.length === this.aMappingTable.normal.length) {
        const nAttributeList = [];
        const exnAttributeList = [];

        for (let i = 0; i <= this.aMappingTable.normal.length - 1; i++) {
          const normalPkidValue = this.aMappingTable.normal[i].attributemappingid;
          if (normalPkidValue === undefined) {
            nAttributeList.push({
              attributemappingid: '',
              actionid: this.aMappingTable.normal[i].actionid,
              inputpropertyid: this.aMappingTable.normal[i].inputpropertyid,
              outputpropertyid: this.aMappingTable.normal[i].outputpropertyid,
              workflowinputtemplateid: this.aMappingTable.normal[i].workflowinputtemplateid,
              prefix: '',
              suffix: '',
              isactive: true,
              createdby: localStorage.getItem('user_name'),
              updatedby: localStorage.getItem('user_name')
            });
          } else {
          }
        }


        for (let i = 0; i <= this.exaMappingTable.normal.length - 1; i++) {
          exnAttributeList.push({
            attributemappingid: this.exaMappingTable.normal[i].attributemappingid,
            actionid: this.exaMappingTable.normal[i].actionid,
            inputpropertyid: this.exaMappingTable.normal[i].inputpropertyid,
            outputpropertyid: this.exaMappingTable.normal[i].outputpropertyid,
            workflowinputtemplateid: this.exaMappingTable.normal[i].workflowinputtemplateid,
            prefix: '',
            suffix: '',
            isactive: false,
            createdby: this.exaMappingTable.normal[i].createdby,
            updatedby: this.exaMappingTable.normal[i].updatedby
          });
        }



        const ndatas = {
          normalattributes: nAttributeList
        };

        const exndatas = {
          normalattributes: exnAttributeList
        };

        const formDatas = new FormData();
        formDatas.append('details', JSON.stringify(ndatas));

        this.workflowServices.postRequest('normalattributes', formDatas).subscribe((response) => {
          // ;
          if (response.status === 'Success') {
            this.ShowMessage = false;
            // this.clearattributemapping();
            // this.GetattributemappingDetails();
            // this.successmessage(response.message);
            const exformDatas = new FormData();
            exformDatas.append('details', JSON.stringify(exndatas));
            this.workflowServices.postRequest('update_normalattributes', exformDatas).subscribe((responses) => {
              // ;
              if (responses.status === 'Success') {
                this.ShowMessage = false;
                this.clearattributemapping();


                this.successmessage(responses.message);
                this.GetattributemappingDetails(this.curntAttrType);
                setTimeout(() => {
                 
                  $('#attMappingSaveBtn').removeClass('event-none');
                }, 1000);


              } else {
                $('.spinner').hide();
                $('#attMappingSaveBtn').removeClass('event-none');
                this.Errormessage(responses.message);
              }
            });
          } else {
            $('.spinner').hide();
            $('#attMappingSaveBtn').removeClass('event-none');
            this.Errormessage(response.message);
          }
        }, err => {
          $('.spinner').hide();
          $('#attMappingSaveBtn').removeClass('event-none');
          this.Errormessage(err.message);
        });

      } else {
        this.ShowMessage = false;
        // this.showtableerror = true;
        $('.spinner').hide();
        $('#attMappingSaveBtn').removeClass('event-none');
        this.Errormessage('Please map all the attributes');
      }
    } else if (this.curntAttrType === 'STATIC') {

      if (this.aMappingTable.static.length !== 0) {
        const sAttributeList = [];
        const exsAttributeList = [];

        for (let i = 0; i <= this.aMappingTable.static.length - 1; i++) {

          const idS = this.exaMappingTable.static.filter(items => items.botattributeid === this.aMappingTable.static[i].botattributeid);
          if (idS.length === 0) {
            const staticPkidValue = this.aMappingTable.static[i].botstaticattributesid;
            if (staticPkidValue === undefined) {
              sAttributeList.push({
                botattributeid: this.aMappingTable.static[i].botattributeid,
                actionid: this.aMappingTable.static[i].actionid,
                staticattributevalue: this.aMappingTable.static[i].staticattributevalue
              });
            }
          } else {
            exsAttributeList.push({
              botstaticattributeid: idS[0].botstaticattributesid,
              botattributeid: idS[0].botattributeid,
              actionid: idS[0].actionid,
              staticattributevalue: this.aMappingTable.static[i].staticattributevalue
            });
          }


        }

        const sdatas = {
          staticattributes: sAttributeList
        };

        const exsdatas = {
          staticattributes: exsAttributeList
        };

        const formDatas = new FormData();
        formDatas.append('details', JSON.stringify(sdatas));

        const exformDatas = new FormData();
        exformDatas.append('details', JSON.stringify(exsdatas));

        this.workflowServices.postRequest('staticattributes', formDatas).subscribe((response) => {
          if (response.status === 'Success') {
            this.ShowMessage = false;
            // this.clearattributemapping();
            // this.GetattributemappingDetails();
            // this.successmessage(response.message);
            this.workflowServices.postRequest('update_staticattributes', exformDatas).subscribe((responses) => {
              if (responses.status === 'Success') {
                this.ShowMessage = false;
                this.clearattributemapping();
                this.GetattributemappingDetails(this.curntAttrType);
                this.successmessage(responses.message);
                setTimeout(() => {
                  $('#attMappingSaveBtn').removeClass('event-none');
                }, 1000);
              } else {
                $('.spinner').hide();
                $('#attMappingSaveBtn').removeClass('event-none');
                this.Errormessage(responses.message);
              }
            });
          } else {
            this.ShowMessage = false;
            $('.spinner').hide();
            $('#attMappingSaveBtn').removeClass('event-none');
            this.Errormessage(response.message);
          }
        }, err => {
          $('.spinner').hide();
          $('#attMappingSaveBtn').removeClass('event-none');
          this.Errormessage(err.message);
        });
      } else {
        this.ShowMessage = false;
        $('.spinner').hide();
        $('#attMappingSaveBtn').removeClass('event-none');
        this.Errormessage('Please map the attributes');
      }
    } else if (this.curntAttrType === 'DERIVED') {

      if (this.aMappingTable.derived.length !== 0) {
        const dAttributeList = [];
        const exdAttributeList = [];

        for (let i = 0; i <= this.aMappingTable.derived.length - 1; i++) {
          const derivedPkidValue = this.aMappingTable.derived[i].botderivedinputpropertiesid;
          if (derivedPkidValue === undefined) {
            dAttributeList.push({
              botderivedinputpropertiesid: '',
              botattributeid: this.aMappingTable.derived[i].targetAttribute.attribute_id,
              actionid: this.aMappingTable.derived[i].actionid,
              isactive: true,
              isfromworkflowinputtemplate: this.aMappingTable.derived[i].isfromworkflowinputtemplate,
              workflowinputtemplateid_or_botoutputid: this.aMappingTable.derived[i].workflowinputtemplateid_or_botoutputid,
              concatenatingsequence: this.aMappingTable.derived[i].sequence,
              separator: this.aMappingTable.derived[i].deliminator,
              prefix: this.aMappingTable.derived[i].prefix,
              suffix: this.aMappingTable.derived[i].suffix,
              createdby: localStorage.getItem('user_name'),
              updatedby: localStorage.getItem('user_name')
            });
          }
        }

        for (let i = 0; i <= this.exaMappingTable.derived.length - 1; i++) {
          exdAttributeList.push({
            botderivedinputpropertiesid: this.exaMappingTable.derived[i].botderivedinputpropertiesid,
            botattributeid: this.exaMappingTable.derived[i].targetAttribute.attribute_id,
            actionid: this.exaMappingTable.derived[i].actionid,
            isactive: false,
            isfromworkflowinputtemplate: this.exaMappingTable.derived[i].isfromworkflowinputtemplate,
            workflowinputtemplateid_or_botoutputid: this.exaMappingTable.derived[i].workflowinputtemplateid_or_botoutputid,
            concatenatingsequence: this.exaMappingTable.derived[i].sequence,
            separator: this.exaMappingTable.derived[i].deliminator,
            prefix: this.exaMappingTable.derived[i].prefix,
            suffix: this.exaMappingTable.derived[i].suffix,
            createdby: localStorage.getItem('user_name'),
            updatedby: localStorage.getItem('user_name')
          });
        }

        const ddatas = {
          derivedattributes: dAttributeList
        };

        const exddatas = {
          derivedattributes: exdAttributeList
        };

        const formDatas = new FormData();
        formDatas.append('details', JSON.stringify(ddatas));

        const exformDatas = new FormData();
        exformDatas.append('details', JSON.stringify(exddatas));

        this.workflowServices.postRequest('derivedattributes', formDatas).subscribe((response) => {
          if (response.status === 'Success') {
            this.ShowMessage = false;
            // this.clearattributemapping();
            // this.GetattributemappingDetails();
            // this.successmessage(response.message);
            this.workflowServices.postRequest('update_derivedattributes', exformDatas).subscribe((responses) => {
              if (responses.status === 'Success') {
                this.ShowMessage = false;
                this.clearattributemapping();
                this.GetattributemappingDetails(this.curntAttrType);
                this.successmessage(responses.message);
                setTimeout(() => {
                  $('#attMappingSaveBtn').removeClass('event-none');
                }, 1000);
              } else {
                this.ShowMessage = false;
                $('.spinner').hide();
                $('#attMappingSaveBtn').removeClass('event-none');
                this.Errormessage(responses.message);
              }
            }, err => {
              $('.spinner').hide();
              $('#attMappingSaveBtn').removeClass('event-none');
              this.Errormessage(err.message);
            });
          } else {
            $('.spinner').hide();
            this.ShowMessage = false;
            $('#attMappingSaveBtn').removeClass('event-none');
            this.Errormessage(response.message);
          }
        });
      } else {
        $('.spinner').hide();
        this.ShowMessage = false;
        $('#attMappingSaveBtn').removeClass('event-none');
        this.Errormessage('Please map the attributes');
      }

    }

  }

  sourceAttributesonChange(data) {
    // ;
    this.disableuserAttributeDropdown = true;
    this.aMappingformValuserAttribute = '';
  }

  userAttributesonChange(data) {
    // ;
    this.disablesourceAttributesDropdown = true;
    this.aMappingformValsourceAttribute = '';
  }

  targetAttributesonChange(selectedvalue) {
    // ;
  }

  // new changes
  attributeSelect(selAtrType) {
    // ;
    this.submitted = false;
    this.showtableerror = false;
    this.aMappingformValuserAttribute = '';
    this.aMappingformValsourceAttribute = '';
    this.aMappingformValtargetAttribute = '';
    this.targetAttributesClone = [];
    //this.targetAttributesClone = this.targetAttributesClone1;
    if (selAtrType.attr === 'Static') {
      this.targetAttributes = this.targetBotAttributesList.static_attributes;
      for (let i = 0; i <= this.targetAttributes.length - 1; i++) {
        const checkid = this.aMappingTable.static.filter(item =>
          item.targetAttribute.attribute_id === this.targetAttributes[i].attribute_id);
        if (checkid.length === 0) {
          this.targetAttributesClone.push({
            attribute_id: this.targetAttributes[i].attribute_id, attribute_name: this.targetAttributes[i].attribute_name,
            label: this.targetAttributes[i].attribute_name, value: this.targetAttributes[i]
          });
        } else {
          this.targetAttributesClone.push({
            attribute_id: this.targetAttributes[i].attribute_id, attribute_name: this.targetAttributes[i].attribute_name,
            label: this.targetAttributes[i].attribute_name, value: this.targetAttributes[i], disabled: true
          });
        }
      }
    } else if (selAtrType.attr === 'Derived') {
      this.targetAttributes = this.targetBotAttributesList.derived_attributes;
      for (let i = 0; i <= this.targetAttributes.length - 1; i++) {
        this.targetAttributesClone.push({
          attribute_id: this.targetAttributes[i].attribute_id, attribute_name: this.targetAttributes[i].attribute_name,
          label: this.targetAttributes[i].attribute_name, value: this.targetAttributes[i]
        });
      }
    } else {
      this.targetAttributes = this.targetBotAttributesList.normal_attributes;
      for (let i = 0; i <= this.targetAttributes.length - 1; i++) {
        const checkid = this.aMappingTable.normal.filter(item =>
          item.targetAttribute.attribute_id === this.targetAttributes[i].attribute_id);
        if (checkid.length === 0) {
          this.targetAttributesClone.push({
            attribute_id: this.targetAttributes[i].attribute_id, attribute_name: this.targetAttributes[i].attribute_name,
            label: this.targetAttributes[i].attribute_name, value: this.targetAttributes[i]
          });
        } else {
          this.targetAttributesClone.push({
            attribute_id: this.targetAttributes[i].attribute_id, attribute_name: this.targetAttributes[i].attribute_name,
            label: this.targetAttributes[i].attribute_name, value: this.targetAttributes[i], disabled: true
          });
        }

      }
    }
    this.curntAttrType = selAtrType.attr.toUpperCase();

  }

  generatePDF() {
    // $('#drawing').addClass('html2canvasreset');
    const div = document.getElementById('drawing');

    const options = {
      background: 'white',
      useCORS: true,
      allowTaint: true,
      scrollX: -window.scrollX,
      scrollY: -window.scrollY
    };
    let highestx;
    let highesty;
    if (this.workflow.length > 0) {
      highestx = Math.max.apply(Math, this.workflow.map((x) => {
        return x.x;
      }));
    } else {
      highestx = 0;
    }
    if (this.workflow.length > 0) {
      highesty = Math.max.apply(Math, this.workflow.map((x) => {
        return x.y;
      }));
    } else {
      highesty = 0;
    }



    const $clone = $('#drawing').clone();

    $('#previewImage').append($clone);
    $('#previewImage>.workflow-space').attr('id', '');
    $('#previewImage>.workflow-space').css({ 'width': (highestx + 170) + 'px', 'height': (highesty + 170) + 'px' });
    $('#previewImage>.workflow-space').addClass('border-0 html2canvasreset');
    $clone.find('.jtk-connector').each(function () {
      // for every SVG element created by JsPlumb for connections ...
      var left = parseInt(this.style.left, 10) + 'px';
      var top = parseInt(this.style.top, 10) + 'px';
      this.removeAttribute('style');
      this.removeAttribute('position');
      this.setAttribute('width', parseInt(this.getAttribute('width'), 10) + 'px');
      this.setAttribute('height', parseInt(this.getAttribute('height'), 10) + 'px');
      this.setAttribute('preserveAspectRatio', 'xMidYMid meet');
      // www . w3 . org / 2000 / svg ' );
      // this.children [0] is the path for connection line
      // this.children [1] is the path for connection arrow shape

      this.setAttribute('viewbox', '0 0' + parseInt(this.getAttribute('width'), 10) + '' + parseInt(this.getAttribute('height'), 10));
      this.children[0].setAttribute('stroke-width', '2px');
      this.children[0].setAttribute('stroke', '#000');
      this.children[1].setAttribute('fill', '#000');
      this.children[1].setAttribute('stroke', '#000');
      $clone.find(this).wrap('<span style = "position: absolute; left:' + left + '; top:' + top + ';"> </span>');
    });






    setTimeout(() => {


      html2canvas($('#previewImage>.workflow-space')[0], options).then((canvas) => {
        // var img = canvas.toDataURL("image/PNG");
        let a = document.createElement('a');
        // toDataURL defaults to png, so we need to request a jpeg, then convert for file download.
        a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
        a.download = this.workflowName + '.jpg';
        a.click();
        setTimeout(() => {

          $('.workflow-space').css({ 'width': 'unset' });
          $('.workflow-space').removeClass('border-0');
          $('#previewImage').html('');
        })


      });
    });//set timeout end


  }
  // workflow download working ==========================================================





  // workflow name duplicate checker
}
