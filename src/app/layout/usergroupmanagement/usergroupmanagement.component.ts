import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserGroupService } from '../../services/user-group.service';
import { HttpClient } from '@angular/common/http';
import { formValidators } from '../../helper/formValidators';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { NgForm } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { ThrowStmt } from '@angular/compiler';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';
import { ProjectDefinitionService } from '../../services/project-definition.service';
import { SharedvalueService } from 'src/app/services/sharedvalue.service';

declare var $: any;
@Component({
  selector: 'app-usergroupmanagement',
  templateUrl: './usergroupmanagement.component.html',
  providers: [CommonService, MessageService, ToastModule, DatePipe],
  styleUrls: ['./usergroupmanagement.component.css'],

})
export class UsergroupmanagementComponent implements OnInit {
  //  remove variable
  public groupId = 70;
  public editgroupId: any;
  public deletegroupId: any;
  // remove variable
  public groups = [];
  public selectedGroup;
  userGroup: any;
  userGroupDetails = [];
  userGroupForm: FormGroup;
  isSubmitted = false;
  selectedValue: string;
  cols: any[];
  show: boolean;
  groupName: string;
  uploadData: any;
  disabled: boolean;
  showmenu: boolean;
  editgroup = false;
  // user Group Updation variables


  public selectedUser;

  // Project 
  projectList = [];
  projectSelectedItems = [];
  projectSettings = {};

  public userList: string;
  public isGroupActive: number;
  users: any;
  selectedUserList: any;
  groupUserList: SelectItem[];
  selectedUserwithRole = [];
  selectedGroupUsersList: any;
  isActive: any;
  selectedTableUser: any;
  exportPdfcolumn: any;
  groupmail: string;
  user_id: number;
  emailPattern = "^[A-za-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  datePipervalue: any;
  // user Group Updation variables end
  // setting dropdown
  userdpSettings: any;
  showSideBar = true;

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private UserGroup: UserGroupService,
    private messageService: MessageService,
    private datePipe: DatePipe,
    private ProjectDefinition: ProjectDefinitionService,
    public shrService: SharedvalueService
  ) {
    this.userdpSettings = {
      singleSelection: false,
      text: "Select Users",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      badgeShowLimit: 2,
    }

    this.isActive = 0;



  }

  ngOnInit() {
    $('.spinner').show();
    $('.modal').appendTo('#fullscreen');
    this.shrService.getSideBarDetail().subscribe(resp => { this.showSideBar = resp });

    this.selectedValue = '1';

    this.GetUserGroupList();
    this.user_id = parseInt(localStorage.getItem('user_id'));
    this.userGroupForm = this.formBuilder.group({
      groupName: ['', [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      project: ['', Validators.required],
      groupmail: ['', [Validators.required, formValidators.email]],
      selectedUserList: ['', Validators.required],
      isActive: ['0', Validators.required]

    });


    this.cols = [
      { field: 'groupName', header: 'Group Name' },
      { field: 'noOfUser', header: 'No.of User' },
      { field: 'project', header: 'Project' },
      { field: 'groupMailId', header: 'Group Mail ID' },
      { field: 'createdBy', header: 'Created by' },
    ];

    this.projectSettings = {
      singleSelection: true,
      text: "Select Project",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };


  }


  GetProjectList() {
    // $('.spinner').show();
    this.ProjectDefinition.getRequest('GetProjectList').subscribe((ProjectDetails) => {
      // $('.spinner').hide();
      let projectsArr = ProjectDetails;
      this.projectList = [];
      projectsArr.forEach(resource => {
        this.projectList.push({ id: resource['projectId'], itemName: resource['projectName'] });
      });

    });
  }

  onProjectSelect(item: any) {
    this.selectedUserwithRole = [];
    this.selectedUserList = [];
    this.users = [];
    this.UserGroup.getRequest('GetProjectUserRoleList/' + item.id + '').subscribe((UserRoleListDetails) => {
      let userRoleList = UserRoleListDetails;
      let userRoleArr = {};
      userRoleList.forEach(resource => {
        if (!userRoleArr.hasOwnProperty(resource['userId'])) {
          userRoleArr[resource['userId']] = {};
        }
        userRoleArr[resource['userId']]['userId'] = resource['userId'];
        userRoleArr[resource['userId']]['userName'] = resource['userName'];
        if (!userRoleArr[resource['userId']].hasOwnProperty('roles')) {
          userRoleArr[resource['userId']]['roles'] = [];
        }
        if (!userRoleArr[resource['userId']].hasOwnProperty('roleIds')) {
          userRoleArr[resource['userId']]['roleIds'] = [];
        }
        userRoleArr[resource['userId']]['roles'].push(resource['roleName']);
        userRoleArr[resource['userId']]['roleIds'].push(resource['roleId']);

      });

      let usersObjArr = Object.keys(userRoleArr).map(i => userRoleArr[i]);
      usersObjArr.forEach((element, index) => {
        this.users.push({ itemName: element['userName'], id: element['userId'], roleNames: element['roles'].join(), roleIds: element['roleIds'].join() })
      });
    });

  }

  OnProjectDeSelect(item: any) {
    this.selectedUserList = [];
    this.selectedUserwithRole = [];
    this.users = [];
  }

  onProjectDeSelectAll(items: any) {
    this.selectedUserList = [];
    this.selectedUserwithRole = [];
    this.projectSelectedItems = [];
  }


  onProjectFilterSelectAll(items: any) {
    this.onProjectSelect(items[items.length - 1]);
  }


  onProjectFilterDeSelectAll(items: any) {
    items.forEach(resource => {
      this.OnProjectDeSelect(resource);
    });
  }


  delUserFromGroup(item: any, user) {

    const index = this.userGroupForm.value.selectedUserList.findIndex(x => x.roleIds === item.roleId && x.itemName === user.userName);
    this.userGroupForm.value.selectedUserList.splice(index, 1);
    const i = this.selectedUserwithRole.findIndex(x => x.roleId === item.roleId);
    if (this.selectedUserwithRole[i].userList === []) { } else {
      const j = this.selectedUserwithRole[i].userList.findIndex(x => x.userName === user.userName);
      this.selectedUserwithRole[i].userList.splice(j, 1);

    }

  }


  setMultiselectLabel() {
    this.selectedUserList = this.userGroupForm.value.selectedUserList;

    if (this.selectedUserList.length === 0) { $('.ui-multiselect-label.ui-corner-all').text('Choose'); return; }
    if (this.selectedUserList.length > 3) {
      // tslint:disable-next-line: max-line-length
      $('.ui-multiselect-label.ui-corner-all').text(this.selectedUserList.length === 1 ? '1 User selected' : this.selectedUserList.length + ' Users selected'); return;
    }
    let users = '';
    this.selectedUserList.forEach((elem, i) => {
      if (i === 0) { users += elem.label; } else { users += ', ' + elem.label; }
    });
    $('.ui-multiselect-label.ui-corner-all').text(users);
  }

  GetUserGroupList() {
    console.log('usergroup')
    $('.spinner').show();
    this.UserGroup.getRequest('GetUserGroupList').subscribe((UserGroupDetails) => {

      let userGroupArr = UserGroupDetails;
      if (userGroupArr != null) {
        let userGroupObj = {};
        userGroupArr.forEach((elem, ix) => {
          if (!userGroupObj.hasOwnProperty(elem.userGroupId)) {
            userGroupObj[elem.userGroupId] = {};
          }
          userGroupObj[elem.userGroupId]['userGroupId'] = elem.userGroupId;
          userGroupObj[elem.userGroupId]['groupName'] = elem.userGroupName;
          userGroupObj[elem.userGroupId]['noOfUser'] = elem.noOfUsers;
          userGroupObj[elem.userGroupId]['groupMailId'] = elem.userGroupMail;
          userGroupObj[elem.userGroupId]['project'] = elem.project;
          userGroupObj[elem.userGroupId]['insertedDateTime'] = elem.project;
          userGroupObj[elem.userGroupId]['createdBy'] = elem.createdBy;
          userGroupObj[elem.userGroupId]['active'] = elem.active;
          userGroupObj[elem.userGroupId]['insertedDateTime'] = elem.createdOn;
          if (!userGroupObj[elem.userGroupId].hasOwnProperty('users')) {
            userGroupObj[elem.userGroupId]['users'] = {};
          }
          if (!userGroupObj[elem.userGroupId]['users'].hasOwnProperty(elem.userId)) {
            userGroupObj[elem.userGroupId]['users'][elem.userId] = {};
          }

          userGroupObj[elem.userGroupId]['users'][elem.userId]['userName'] = elem.userName;

          if (!userGroupObj[elem.userGroupId]['users'][elem.userId].hasOwnProperty('roles')) {
            userGroupObj[elem.userGroupId]['users'][elem.userId]['roles'] = [];
          }

          userGroupObj[elem.userGroupId]['users'][elem.userId]['roles'].push(elem.roleName);
          if (ix === (userGroupArr.length - 1)) {
            // $('.spinner').hide();

          }
        });
        this.userGroup = Object.keys(userGroupObj).map(i => userGroupObj[i])
        let userLists = Object.keys(userGroupObj).map(i => userGroupObj[i]);
        userLists.forEach((elem, index) => {

          Object.keys(elem.users).forEach((userId, index2) => {

            if (!userGroupObj[elem.userGroupId].hasOwnProperty('tooltip')) {
              userGroupObj[elem.userGroupId]['tooltip'] = [];
            }

            let roles = elem.users[userId].roles.filter(function (item, index, inputArray) {
              return inputArray.indexOf(item) == index;
            });

            userGroupObj[elem.userGroupId]['tooltip'].push(elem.users[userId].userName + '(' + roles.join() + ')');
            userGroupObj[elem.userGroupId]['userList'] = userGroupObj[elem.userGroupId]['tooltip'].join('\n');
            if (index === (userLists.length - 1)) {
              // $('.spinner').hide();

            }
          });


        })


        this.userGroup.sort(this.dynamicsort("userGroupId", 'desc'));
        this.showmenu = true;
        $('.spinner').hide();
      } else {
        this.Errormessage('User group data is empty');
        this.showmenu = false;
        $('.spinner').hide();
      }


      this.exportPdfcolumn = this.cols.map(col => ({ title: col.header, dataKey: col.field }));
    }, err => {
      $('.spinner').hide();
    });

    this.GetProjectList();
  }

  dynamicsort(property, order) {
    var sort_order = 1;
    if (order === "desc") {
      sort_order = -1;
    }
    return function (a, b) {
      // a should come before b in the sorted order
      if (a[property] < b[property]) {
        return -1 * sort_order;
        // a should come after b in the sorted order
      } else if (a[property] > b[property]) {
        return 1 * sort_order;
        // a and b are the same
      } else {
        return 0 * sort_order;
      }
    }
  }

  public get getFields() {
    return this.userGroupForm.controls;
  }


  submitForm() {
$('.spinner').show();
    this.isSubmitted = true;
    let formData = this.userGroupForm.value;
    this.selectedUserList = this.userGroupForm.value.selectedUserList;
    if (this.userGroupForm.invalid) {
      $('.spinner').hide();
      return false;
    }

    if (!this.editgroup) {
      let data = [];

      formData['selectedUserList'].forEach((elem, index) => {
        let roles = elem['roleNames'].split(',');
        let roleIds = elem['roleIds'].split(',');
        data.push({
          UserId: elem['id'] * 1,
          ProjectId: formData['project'][0]['id'] * 1,
          UserGroupName: formData['groupName'],
          Active: parseInt(this.userGroupForm.value.isActive) ? true : false,
          GroupMailId: formData['groupmail'],
          ProjectName: formData['project'][0]['itemName'],
          NoOfUser: this.selectedUserList.length * 1,
          InsertedBy: this.user_id,
          RecordStatus: 1
        });
      });


      if (this.selectedUserList.length > 0) {

        this.UserGroup.postRequest('InsertUserGroupList', data).subscribe((response) => {
          if (response.status == 1) {
            this.isSubmitted = false;
            $('.close').trigger('click');
            this.Close();
            this.GetUserGroupList();
            this.successmessage(response["message"]);
            this.disabled = false;
          }
          else {
            $('.spinner').hide();
            this.Errormessage(response["message"]);
            this.disabled = false;
          }
        },  err =>{
          this.Errormessage(err.message);
          $('.spinner').hide();
        });
      }else{
        $('.spinner').hide();
      }
    } else {
      this.updateForm();
    }


  }



  deleteGroup(userGroup: any) {
    this.deletegroupId = userGroup.userGroupId;
    this.groupName = userGroup.groupName;
  }



  deleteUserGroup(userGropId) {

    var data = {
      'userGroupId': userGropId,
    };

    const isEmpty = !Object.values(data).some(x => (x !== null && x !== '' && x === undefined));

    if (isEmpty) {
      this.UserGroup.postRequest('DeleteUserGroup', data).subscribe((response) => {
        if (response.status == 1) {
          $('.modal-close').trigger('click');
          this.GetUserGroupList();
          this.successmessage("User deleted successfully");
        }
        else {
          if (response["status"] == 2) {
            this.Errormessage(response['message']);
          }
        }
      });
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }


  onItemSelect(item: any) {
    let obj;
    let index;
    if (this.selectedUserwithRole.length) {
      index = this.selectedUserwithRole.findIndex(x => x.roleId === item.roleIds);
    } else {
      index = -1;
    }

    if (index === -1) {
      obj = {
        roleId: item.roleIds,
        roleName: item.roleNames,
        userList: [{ userName: item.itemName, userId: item.id }]
      };
      this.selectedUserwithRole.push(obj);
    } else {
      obj = { userName: item.itemName, userId: item.id };
      this.selectedUserwithRole[index].userList.push(obj);
    }


  }




  OnItemDeSelect(item: any) {
    const index = this.selectedUserwithRole.findIndex(x => x.roleId === item.roleIds);
    const i = this.selectedUserwithRole[index].userList.findIndex(x => x.userName === item.itemName);
    this.selectedUserwithRole[index].userList.splice(i, 1);

  }



  onItemSelectAll(items: any) {
    this.selectedUserwithRole = [];
    items.forEach((elem, index) => {
      this.onItemSelect(elem);
    });

  }


  onFilterSelectAll(items: any) {
    items.forEach((elem, index) => {
      this.onItemSelect(elem);
    });

  }

  onItemDeSelectAll(items: any) {
    this.selectedUserwithRole = [];
    this.selectedUserList = [];
  }

  onFilterDeSelectAll(items: any) {
    items.forEach(resource => {
      this.OnItemDeSelect(resource);
    })
  }


  editUserGroup(userGroup: any) {
    this.isSubmitted = false;
    this.editgroup = true;
    this.projectSelectedItems = [];
    this.UserGroup.getRequest('GetEditUserList/' + userGroup.userGroupId + '').subscribe(EditUserGroupDetails => {
      let userGroupDetails = EditUserGroupDetails;
      let userIds = [];
      userGroupDetails.forEach((elem, index) => {
        userIds.push(elem.userId);
        this.groupName = elem.userGroupName;
        this.projectSelectedItems = [{ id: elem.projectId, itemName: elem.projectName }];
        this.groupmail = elem.groupMailId;
        this.isActive = elem.active ? '1' : '0';
        this.editgroupId = elem.userGroupId
      });

      userIds = userIds.filter(function (item, index, inputArray) {
        return inputArray.indexOf(item) == index;
      });



      this.EditSelectedProjectUserRole(this.projectSelectedItems[0], userIds);

    });

  }

  EditSelectedProjectUserRole(item, userIds) {
    $('.spinner').show();
    this.selectedUserwithRole = [];
    this.selectedUserList = [];
    this.users = [];
    this.UserGroup.getRequest('GetProjectUserRoleList/' + item.id + '').subscribe((UserRoleListDetails) => {
      let userRoleList = UserRoleListDetails;
      let userRoleArr = {};
      userRoleList.forEach(resource => {
        if (!userRoleArr.hasOwnProperty(resource['userId'])) {
          userRoleArr[resource['userId']] = {};
        }
        userRoleArr[resource['userId']]['userId'] = resource['userId'];
        userRoleArr[resource['userId']]['userName'] = resource['userName'];
        if (!userRoleArr[resource['userId']].hasOwnProperty('roles')) {
          userRoleArr[resource['userId']]['roles'] = [];
        }
        if (!userRoleArr[resource['userId']].hasOwnProperty('roleIds')) {
          userRoleArr[resource['userId']]['roleIds'] = [];
        }
        userRoleArr[resource['userId']]['roles'].push(resource['roleName']);
        userRoleArr[resource['userId']]['roleIds'].push(resource['roleId']);

      });

      let usersObjArr = Object.keys(userRoleArr).map(i => userRoleArr[i]);

      usersObjArr.forEach((element, index) => {
        this.users.push({ itemName: element['userName'], id: element['userId'], roleNames: element['roles'].join(), roleIds: element['roleIds'].join() })
      });

      this.users.forEach((elem, index) => {
        if (userIds.includes(elem.id)) {
          this.selectedUserList.push(elem);
          this.onItemSelect(elem);
        }
      });

      this.userGroupForm.setValue({
        groupName: this.groupName,
        project: this.projectSelectedItems,
        groupmail: this.groupmail,
        selectedUserList: this.selectedUserList,
        isActive: this.isActive
      });
      $('.spinner').hide();
    }, err =>{
      $('.spinner').hide();
    });

  }


  Open() {

    this.isSubmitted = false;
    this.editgroup = false;
    this.userGroupForm.reset();
    this.selectedUserwithRole = [];
  }

  Close() {
    this.userGroupForm.reset();
  }

  successmessage(message) {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: message });
  }

  Errormessage(errorsmessage) {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
  }

  updateForm() {
    $('.spinner').show();
    this.isSubmitted = true;
    let formData = this.userGroupForm.value;
    this.selectedUserList = this.userGroupForm.value.selectedUserList;
    if (this.userGroupForm.invalid) {
      $('.spinner').hide();
      return false;
    }

    let data = [];
    formData['selectedUserList'].forEach((elem, index) => {
      let roles = elem['roleNames'].split(',');
      let roleIds = elem['roleIds'].split(',');
      data.push({
        UserGroupId: this.editgroupId,
        UserId: elem['id'] * 1,
        ProjectId: formData['project'][0]['id'] * 1,
        UserGroupName: formData['groupName'],
        Active: parseInt(this.userGroupForm.value.isActive) ? true : false,
        GroupMailId: formData['groupmail'],
        ProjectName: formData['project'][0]['itemName'],
        NoOfUser: this.selectedUserList.length * 1,
        InsertedBy: this.user_id,
        RecordStatus: 1
      });
    });


    if (this.selectedUserList.length > 0) {
      this.UserGroup.postRequest('UpdateUserGroupList', data).subscribe((response) => {
        if (response.status == 1) {
          $('.close').trigger('click');
          this.Close();
          this.GetUserGroupList();
          this.successmessage(response["message"]);
          this.disabled = false;
        }
        else {
          this.Errormessage(response["message"]);
          $('.spinner').hide();
          this.disabled = false;
        }
      }, err =>{
        this.Errormessage(err.message);
        $('.spinner').hide();
      });
      $('.spinner').hide();
    }else{
      $('.spinner').hide();
    }
  }







  exportPdf() {

    const doc = new jsPDF('l');
    doc.autoTable(this.exportPdfcolumn, this.userGroup);
    doc.save('UDP_User_Groups' + new Date().getTime() + '.pdf');

  }


  exportExcel() {
    import('xlsx').then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.userGroup);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, 'UDP_User_Groups');
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {


      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXTENSION_TYPE = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + new Date().getTime() + EXTENSION_TYPE);
    });
  }


  exportCSV() {
    import('xlsx').then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.userGroup);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'csv', type: 'array' });

      this.saveAsCSVFile(excelBuffer, 'UDP_User_Groups');
    });
  }

  saveAsCSVFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {


      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXTENSION_TYPE = '.csv';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + new Date().getTime() + EXTENSION_TYPE);
    });
  }


}
