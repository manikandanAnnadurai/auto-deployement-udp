import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { InputService } from 'src/app/services/input.service';
import * as moment from 'moment';
import { MultiDragDropService } from './multi-drag-drop.service';
import { Pipe, PipeTransform } from '@angular/core';
import { SearchPipe } from './search.pipe';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { formValidators } from "../../helper/formValidators";
import { MessageService } from 'primeng/api';
import { ExportService } from 'src/app/services/export.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { SharedvalueService } from 'src/app/services/sharedvalue.service';
import { TreeDragDropService } from 'primeng/api';

declare var $: any;
@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css'],
  providers: [TreeDragDropService, MessageService],
})


export class InputComponent implements OnInit {

  public ftpForm: FormGroup;
  public groupNameSuggestion: any = [];
  public suggestionList: any = [];
  public grpieName: any = '';
  public dbForm: FormGroup;
  public cloudForm: FormGroup;
  public apiForm: FormGroup;
  public attributes: any;
  public isCheckGrpName: any;
  public isCheckSubGrpName: any;
  public attributeTypeSettings: any;
  public attributeTypes: any;
  public selAttrTypes: any;
  public inputFrom: any;
  public msFormatSetting: any;
  public editSavedType: boolean;
  public dateFormats: any;
  public selDateFormat: any;
  public isBulkUpload = 1;
  public selectedValue: any;
  public user_id: number;
  public selectedRadioBtn: any;
  public currentProjectGroups: any = [];
  public pleaseWait = false;
  public isFullScreen = true;
  public allDefaultAttr = [];
  public resultData: any = [];
  public errorExclReports = [];
  public errorExistsComma = [];
  public valuesInvalid = {
    values: false,
    min: false,
    max: false,
    dateFrom: false,
    dateTo: false,
    dateFormat: false,
  };
  public delAttributeModal: any = {};
  public fileName = 'Choose File';
  public cdateFormat: any = 'DD MMM, YYYY';
  public formValues: any = {
    isEdit: false,
    attributeName: '',
    displayName: '',
    isSameAsDisplayName: true,
    opdisplayName: '',
    selAttrTypes: '',
    inputValues: '',
    min: '',
    max: '',
    dateFrom: '',
    dateTo: '',
    dateFormat: '',
    isBulkUpload: 1
  };

  public editFormValues: any = {
    isEdit: true,
    attributeName: '',
    displayName: '',
    isSameAsDisplayName: true,
    opdisplayName: '',
    selAttrTypes: '',
    inputValues: '',
    min: '',
    max: '',
    dateFrom: '',
    dateTo: '',
    dateFormat: '',
    isBulkUpload: 1
  };
  public editformIndex: number;
  public isEditAttribute = false;
  public editData: any;
  public editAttributeArray: any;
  public isreload = false;
  public submitForm: boolean;
  public isUpdateBtn: boolean;
  public saveRecords: boolean;
  public dynamicName: string;
  public checkFile: boolean;
  public errorFile: any;
  public errorExistsAttr: any = [];
  public PasswordErrorMessage: string;
  // completed variable

  // drag and drop variable
  public AttributeGroups: any = [];
  public attrList: any = [{
    id: 0,
    groupName: 'Attribute List',
    searchKey: '',
    maingroup: [],
    subgroups: [],

  }];

  public groups: any = [];
  // drag and drop variable
  sortIndex = [];
  // drag and drop variable new
  count = 0;
  stored: any = [];
  ctrlPress = false;
  isMultidragging = true;

  issubmitted = false;
  showSideBar = true;
  public nonNumericAttr: string;
  public nonNumericDisplay: string;
  public nonNumericOdpDisplay: string;
  nodeSelected: any = [];
  dragableGroups: any = [];
  public cmgroup: any = {};
  public subGroupUpdate = false;
  public dragData: any = [];
  // new end
  public defaultAttribute = [];
  public selDefAttribute = [];
  public confDatas = [];
  public onItemSelectCount: number;
  public removalgroups: any = [];
  constructor(
    private inpS: InputService,
    private formBuilder: FormBuilder,
    private dgdp: MultiDragDropService,
    private messageService: MessageService,
    private exports: ExportService,
    private shrService: SharedvalueService,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: any
  ) {
    this.renderer.listen('window', 'click', (e: Event) => {
      /**
       * Only run when toggleButton is not clicked
       * If we don't check this, all clicks (even on the toggle button) gets into this
       * section which in the result we might never see the menu open!
       * And the menu itself is checked here, and it's where we check just outside of
       * the menu and button the condition abbove must close the menu
       */
      //this.isCheckGrpName = false;
    });

    this.attributeTypeSettings = {
      singleSelection: true,
      enableSearchFilter: true,
      showCheckbox: false,
      // enableFilterSelectAll: false,
      labelKey: "attributeTypeName",
      primaryKey: "id"
    };
    this.msFormatSetting = {
      singleSelection: true,
      text: 'Select a Date Format',
      enableSearchFilter: false,
      showCheckbox: false,
      // enableFilterSelectAll: false,
      labelKey: 'format',
      primaryKey: 'id'
    };


    this.dateFormats = [{
      id: 1,
      format: 'MM/DD/YYYY',
    },
    {
      id: 2,
      format: 'DD/MM/YYYY'
    },
    {
      id: 3,
      format: 'DD/MMM/YYYY'
    },

    ];

  }



  GetProjectConfigurationList(attributeTypes) {
    $('.spinner').show();
    this.inpS.getRequest('GetProjectConfigurationAttributeList').subscribe((ProjectConfigurationList) => {
      let unmappedAttr = JSON.parse(ProjectConfigurationList[0].defaultAttributes);
      let mappedAttr = JSON.parse(ProjectConfigurationList[0].mappedAttributes);
      $('.spinner').hide();
      this.defaultAttribute = [];
      this.allDefaultAttr = [];
      // let unmappedAttr = [
      //   {
      //     "projectid": 101,
      //     "moduleid": 3,
      //     "attributeid": 125,
      //     "attributename": "Name",
      //     "attributetype": "Label",
      //     "attributevalue": {
      //       "value": "",
      //       "min": 0,
      //       "max": 0,
      //       "format": null,
      //       "range": null
      //     },
      //     "displayname": "Name",
      //     "attributedisplayorder": null,
      //     "active": null,
      //     "recordstatus": null,
      //     "insertedby": null,
      //     "insertedtime": "2020-10-20T13:34:16.836569+00:00"
      //   },
      //   {
      //     "projectid": 101,
      //     "moduleid": 3,
      //     "attributeid": 126,
      //     "attributename": "ModelNumber",
      //     "attributetype": "Label",
      //     "attributevalue": {
      //       "value": "",
      //       "min": 0,
      //       "max": 0,
      //       "format": null,
      //       "range": null
      //     },
      //     "displayname": "Model Number",
      //     "attributedisplayorder": null,
      //     "active": null,
      //     "recordstatus": null,
      //     "insertedby": null,
      //     "insertedtime": "2020-10-22T10:04:44.989727+00:00"
      //   }
      // ];

      // let mappedAttr = [];
      // let mappedAttr = [
      //   {
      //     "projectid": 101,
      //     "moduleid": 3,
      //     "attributeid": 102,
      //     "attributename": "Name",
      //     "attributetype": "Textbox",
      //     "attributevalue": {
      //       "value": "",
      //       "min": 0,
      //       "max": 0,
      //       "format": null,
      //       "range": null
      //     },
      //     "displayname": "Name",
      //     "attributedisplayorder": 1,
      //     "groupid": 111,
      //     "parentgroupid": 107,
      //     "groupname": "GPONE_1",
      //     "displaygroupname": "GPONE_1",
      //     "grouporder": 1,
      //     "displaygrouporder": 1,
      //     "levelid": 2,
      //     "relationid": 107,
      //     "parent_subgroup_name": "GPONE",
      //     "active": null,
      //     "recordstatus": null,
      //     "insertedby": null,
      //     "insertedtime": "2020-10-16T03: 49: 05.717297+00: 00"
      //   },
      //   {
      //     "projectid": 101,
      //     "moduleid": 3,
      //     "attributeid": 100,
      //     "attributename": "LastName",
      //     "attributetype": "Textbox",
      //     "attributevalue": {
      //       "value": "",
      //       "min": 0,
      //       "max": 0,
      //       "format": null,
      //       "range": null
      //     },
      //     "displayname": "LastName",
      //     "attributedisplayorder": 1,
      //     "groupid": 107,
      //     "parentgroupid": null,
      //     "groupname": "GPONE",
      //     "displaygroupname": "GPONE",
      //     "grouporder": 1,
      //     "displaygrouporder": 1,
      //     "levelid": 1,
      //     "relationid": null,
      //     "parent_subgroup_name": "",
      //     "active": null,
      //     "recordstatus": null,
      //     "insertedby": null,
      //     "insertedtime": "2020-10-16T03: 49: 05.717297+00: 00"
      //   },
      //   {
      //     "projectid": 101,
      //     "moduleid": null,
      //     "attributeid": null,
      //     "attributename": null,
      //     "attributetype": null,
      //     "attributevalue": null,
      //     "displayname": null,
      //     "attributedisplayorder": null,
      //     "groupid": 110,
      //     "parentgroupid": 107,
      //     "groupname": "GPONE1_1",
      //     "displaygroupname": "GPONE1_1",
      //     "grouporder": 1,
      //     "displaygrouporder": 1,
      //     "levelid": 3,
      //     "relationid": 111,
      //     "parent_subgroup_name": "GPONE_1",
      //     "active": null,
      //     "recordstatus": null,
      //     "insertedby": null,
      //     "insertedtime": "2020-10-16T03: 49: 05.717297+00: 00"
      //   },
      //   {
      //     "projectid": 101,
      //     "moduleid": 3,
      //     "attributeid": 101,
      //     "attributename": "FirstName",
      //     "attributetype": "Textbox",
      //     "attributevalue": {
      //       "value": "",
      //       "min": 0,
      //       "max": 0,
      //       "format": null,
      //       "range": null
      //     },
      //     "displayname": "FirstName",
      //     "attributedisplayorder": 2,
      //     "groupid": 111,
      //     "parentgroupid": 107,
      //     "groupname": "GPONE_1",
      //     "displaygroupname": "GPONE_1",
      //     "grouporder": 1,
      //     "displaygrouporder": 1,
      //     "levelid": 2,
      //     "relationid": 107,
      //     "parent_subgroup_name": "GPONE",
      //     "active": null,
      //     "recordstatus": null,
      //     "insertedby": null,
      //     "insertedtime": "2020-10-16T03: 49: 05.717297+00: 00"
      //   },
      //   {
      //     "projectid": 101,
      //     "moduleid": null,
      //     "attributeid": null,
      //     "attributename": null,
      //     "attributetype": null,
      //     "attributevalue": null,
      //     "displayname": null,
      //     "attributedisplayorder": null,
      //     "groupid": 112,
      //     "parentgroupid": 107,
      //     "groupname": "GPONE_2",
      //     "displaygroupname": "GPONE_2",
      //     "grouporder": 1,
      //     "displaygrouporder": 2,
      //     "levelid": 2,
      //     "relationid": 107,
      //     "parent_subgroup_name": "GPONE",
      //     "active": null,
      //     "recordstatus": null,
      //     "insertedby": null,
      //     "insertedtime": "2020-10-16T03: 49: 05.717297+00: 00"
      //   },
      //   {
      //     "projectid": 101,
      //     "moduleid": null,
      //     "attributeid": null,
      //     "attributename": null,
      //     "attributetype": null,
      //     "attributevalue": null,
      //     "displayname": null,
      //     "attributedisplayorder": null,
      //     "groupid": 109,
      //     "parentgroupid": null,
      //     "groupname": "GPTWO",
      //     "displaygroupname": "GPTWO",
      //     "grouporder": 2,
      //     "displaygrouporder": 1,
      //     "levelid": 1,
      //     "relationid": null,
      //     "parent_subgroup_name": "",
      //     "active": null,
      //     "recordstatus": null,
      //     "insertedby": null,
      //     "insertedtime": "2020-10-16T03: 49: 05.717297+00: 00"
      //   },
      //   {
      //     "projectid": 101,
      //     "moduleid": null,
      //     "attributeid": null,
      //     "attributename": null,
      //     "attributetype": null,
      //     "attributevalue": null,
      //     "displayname": null,
      //     "attributedisplayorder": null,
      //     "groupid": 108,
      //     "parentgroupid": null,
      //     "groupname": "GPTHREE",
      //     "displaygroupname": "GPTHREE",
      //     "grouporder": 3,
      //     "displaygrouporder": 1,
      //     "levelid": 1,
      //     "relationid": null,
      //     "parent_subgroup_name": "",
      //     "active": null,
      //     "recordstatus": null,
      //     "insertedby": null,
      //     "insertedtime": "2020-10-16T03: 49: 05.717297+00: 00"
      //   }
      // ];






      // this.attrList[0].maingroup = [];

      let projectId = localStorage.getItem('project_id');
      if (projectId == null || projectId == '') {
        this.Errormessage("Please select one project for Project Definition");
        $('.spinner').hide();
        return false;
      }

      if (unmappedAttr != null) {
        unmappedAttr.forEach((elem, index) => {
         // if (Number(elem.projectid) === Number(projectId)) {
            let attrTypeId = this.attributeTypes.filter(element => element.attributeTypeName == elem.attributetype);
            let dateSplit = [];
            if (elem.attributevalue.range != null) {
              dateSplit = elem.attributevalue.range.split(" - ");
            }


            let dateformat = {};
            if (elem.attributevalue.format) {
              dateformat = this.dateFormats.filter(obj => obj.format === elem.attributevalue.format);
            }



            var attrList = {
              id: elem.attributeid,
              attributeName: elem.attributename,
              displayName: elem.displayname,
              isSameAsDisplayName: elem.outputname,
              opdisplayName: elem.outputname,
              selAttrTypes: attrTypeId[0],
              inputValues: elem.attributevalue.value,
              min: elem.attributevalue.min,
              max: elem.attributevalue.max,
              dateFrom: dateSplit[0],
              dateTo: dateSplit[1],
              dateFormat: dateformat[0],
              selected: false,
              projectId: elem.projectid,
              moduleId: elem.moduleid,
              attributedisplayorder: elem.attributedisplayorder,
              groupid: elem.groupid,
              group_subgroup_name: elem.parent_subgroup_name,
              displaygroupname: elem.displaygroupname,
              grouporder: elem.grouporder,
              displaygrouporder: elem.displaygrouporder,
              flag: elem.outputflag,
            };
            this.defaultAttribute.push(attrList);
            this.allDefaultAttr.push(attrList);
            // this.attrList[0].maingroup.push(attrList);
          //}

        });
      }

      let subGroupAttr = [];
      let groupAttr = [];

      if (mappedAttr != null) {
        let attrList = [];
        let attrObjList = {};



        mappedAttr.forEach((elem, index) => {

          if (Number(elem.projectid) != Number(projectId)) {
            this.suggestionList.push({
              name: elem.groupname,
              code: elem.projectid
            });
          } else {
            this.currentProjectGroups.push({
              name: elem.groupname,
              code: elem.projectid
            })
          }


          if (Number(elem.projectid) === Number(projectId)) {

            if (!attrObjList.hasOwnProperty(elem.groupid)) {
              attrObjList[elem.groupid] = [];
              if (elem.parentgroupid == null) {
                groupAttr.push(elem)
              }
            }

            if (elem.parentgroupid != null) {
              subGroupAttr.push(elem);
            }

            if (elem.attributeid != null) {
              let attrTypeId = this.attributeTypes.filter(element => element.attributeTypeName == elem.attributetype);
              let dateSplit = [];
              if (elem.attributevalue.range != null) {
                dateSplit = elem.attributevalue.range.split(" - ");
              }
              let dateformat = {};
              if (elem.attributevalue.format) {
                dateformat = this.dateFormats.filter(obj => obj.format === elem.attributevalue.format);
              }

              const grpAttrObj = {
                id: elem.attributeid,
                attributeName: elem.attributename,
                displayName: elem.displayname,
                isSameAsDisplayName: elem.outputname,
                opdisplayName: elem.outputname,
                flag: elem.outputflag,
                inputValues: elem.attributevalue.value,
                min: elem.attributevalue.min,
                max: elem.attributevalue.max,
                dateFrom: dateSplit[0],
                dateTo: dateSplit[1],
                dateFormat: dateformat[0],
                dbValue: true,
                selAttrTypes: attrTypeId[0],
                selected: false,
                projectId: elem.projectid,
                moduleId: elem.moduleid,
                attributedisplayorder: elem.attributedisplayorder,
                groupid: elem.groupid,
                group_subgroup_name: elem.parent_subgroup_name,
                displaygroupname: elem.displaygroupname,
                grouporder: elem.grouporder,
                displaygrouporder: elem.displaygrouporder,
              }

              attrObjList[elem.groupid].push(grpAttrObj);
            }
          }
        });


        console.log(this.suggestionList);

        groupAttr.forEach((elem, index) => {

          const obj: any = {
            id: elem.groupid,
            label: elem.groupname,
            parentSubGroupName: elem.parent_subgroup_name,
            searchKey: '',
            isSelected: true,
            expandedIcon: 'pi pi-folder-open',
            collapsedIcon: 'pi pi-folder',
            displayOrder: elem.displaygrouporder,
            levelId: elem.levelid,
            attributeList: attrObjList[elem.groupid],
            children: []
          }

          obj.parentId = (obj.parentgroupid == null) ? obj.id : obj.parentgroupid;
          obj.relationId = elem.relationId;
          obj.dbValue = true;
          obj.projectId = elem.projectid,
            obj.moduleId = elem.moduleid,
            obj.displayGroupOrder = elem.displaygrouporder;
          obj.displayGroupName = elem.displaygroupname;
          this.groups.push(obj);
          this.nodeSelected.push(obj);

        });

        console.log(subGroupAttr);

        let subGroupObj = {};
        let groupNameCheck = [];
        subGroupAttr.forEach((elem, subEleIndex) => {
          if (!subGroupObj.hasOwnProperty(elem.groupid)) {
            subGroupObj[elem.groupid] = [];
          }
          subGroupObj[elem.groupid] = [];
          var n = groupNameCheck.includes(elem.displaygroupname);
          if (!n) {
            groupNameCheck.push(elem.displaygroupname);
            if (elem.parentgroupid != null) {
              const chileObj: any = {
                id: elem.groupid,
                label: elem.groupname,
                parentSubGroupName: elem.parent_subgroup_name,
                searchKey: '',
                relationId: elem.relationid,
                isSelected: true,
                dbValue: true,
                expandedIcon: 'pi pi-folder-open',
                collapsedIcon: 'pi pi-folder',
                displayOrder: elem.displaygrouporder,
                levelId: elem.levelid,
                attributeList: attrObjList[elem.groupid],
                children: []
              };

              chileObj.parentId = elem.parentgroupid;
              chileObj.dbValue = true;
              chileObj.displayGroupOrder = elem.displaygrouporder;
              chileObj.displayGroupName = elem.displaygroupname;
              chileObj.projectId = elem.projectid;
              chileObj.moduleId = elem.moduleid;
              const findobj = this.finder(this.groups, elem.relationid);
              if (findobj != null) {
                findobj.children.push(chileObj);
                this.nodeSelected.push(chileObj);
              }
            }
          }

        });


        console.log(groupAttr)
        console.log(subGroupObj);
        this.restructureGroups(1);

      }

    });
  }



  GetProjectConfigurationDetails() {
    $('.spinner').show();
    let project_id = localStorage.getItem('project_id');
    if (project_id != null) {
      this.inpS.getRequest('GetProjectConfigurationListById/' + project_id + '').subscribe(ProjectConfigurationDetails => {
        $('.spinner').hide();
        this.resultData = [];
        let ary = {};

        if (ProjectConfigurationDetails != null) {
          ProjectConfigurationDetails.forEach((elem, index) => {

            if (!ary.hasOwnProperty(elem.type)) {
              ary[elem.type] = {};
            }

            if (!ary[elem.type].hasOwnProperty(elem.configuration)) {
              ary[elem.type][elem.configuration] = {}
              if (elem.type == "FTP") {

                ary[elem.type][elem.configuration]['hostAddress'] = elem.hostAddress;
                ary[elem.type][elem.configuration]['userName'] = elem.userName;
                ary[elem.type][elem.configuration]['password'] = elem.password;
                ary[elem.type][elem.configuration]['port'] = elem.port;
                ary[elem.type][elem.configuration]['path'] = elem.path;
                ary[elem.type][elem.configuration]['file'] = elem.file;
              }

              else if (elem.type == 'DB') {

                ary[elem.type][elem.configuration] = {};
                ary[elem.type][elem.configuration]['dataBase'] = elem.sqlType;
                ary[elem.type][elem.configuration]['dataBaseName'] = elem.databaseName;
                ary[elem.type][elem.configuration]['tableName'] = elem.tableName;
                ary[elem.type][elem.configuration]['hostAddress'] = elem.hostAddress;
                ary[elem.type][elem.configuration]['userName'] = elem.userName;
                ary[elem.type][elem.configuration]['password'] = elem.password;
                ary[elem.type][elem.configuration]['port'] = elem.port;
              }
              else if (elem.type == 'CLOUD') {
                ary[elem.type][elem.configuration] = {};
                ary[elem.type][elem.configuration]['cloudName'] = elem.cloudName;
                ary[elem.type][elem.configuration]['S3Url'] = elem.s3Url;
                ary[elem.type][elem.configuration]['accessKey'] = elem.accessKey;
                ary[elem.type][elem.configuration]['secretKey'] = elem.secretKey;
              }

            }
          });


          // let arr1 = [];
          // Object.keys(ary).map(function (key) {
          //   arr1.push({ [key]: ary[key] })
          // });

          this.resultData = ary;

        }
      });
    }
  }

  btnReset() {
    this.submitForm = false;
  }

  configClick(type, configuration) {

    this.confDatas = [];
    if (this.resultData != null && this.resultData[type] != null && this.resultData[type][configuration] != null) {
      this.isUpdateBtn = true;
    } else {
      this.isUpdateBtn = false;
    }

    if (type != null && configuration != null && this.resultData != null && this.resultData[type] != null && this.resultData[type][configuration] != null) {
      if (type == 'FTP') {
        this.ftpForm.controls['ftpHostAddress'].setValue(this.resultData[type][configuration]['hostAddress']);
        this.ftpForm.controls['ftpUserName'].setValue(this.resultData[type][configuration]['userName']);
        this.ftpForm.controls['ftpPassword'].setValue(this.resultData[type][configuration]['password']);
        this.ftpForm.controls['ftpPort'].setValue(this.resultData[type][configuration]['port']);
        this.ftpForm.controls['ftpPath'].setValue(this.resultData[type][configuration]['path']);
        this.ftpForm.controls['ftpFile'].setValue(this.resultData[type][configuration]['file']);
        this.ftpForm.controls['ftpActive'].setValue(configuration);
      } else if (type == 'DB') {
        this.dbForm.controls['dbHostAddress'].setValue(this.resultData[type][configuration]['hostAddress']);
        this.dbForm.controls['dbUserName'].setValue(this.resultData[type][configuration]['userName']);
        this.dbForm.controls['dbPassword'].setValue(this.resultData[type][configuration]['password']);
        this.dbForm.controls['dbPort'].setValue(this.resultData[type][configuration]['port']);
        this.dbForm.controls['dbDatabase'].setValue(this.resultData[type][configuration]['dataBaseName']);
        this.dbForm.controls['dbTableName'].setValue(this.resultData[type][configuration]['tableName']);
        this.dbForm.controls['dbActive'].setValue(configuration);
      } else if (type == 'CLOUD') {
        this.cloudForm.controls['cloudName'].setValue(this.resultData[type][configuration]['cloudName']);
        this.cloudForm.controls['s3Url'].setValue(this.resultData[type][configuration]['S3Url']);
        this.cloudForm.controls['accessKey'].setValue(this.resultData[type][configuration]['accessKey']);
        this.cloudForm.controls['secretKey'].setValue(this.resultData[type][configuration]['secretKey']);
        this.cloudForm.controls['cloudActive'].setValue(configuration);
      }
    } else {
      this.formReset(type, configuration);

    }
  }


  tabChange(type) {
    this.submitForm = false;
    this.configClick(type, 'input');
    if (this.saveRecords) {
      this.GetProjectConfigurationDetails();
      this.saveRecords = false;
    }
  }


  formReset(type, configuration) {
    if (type == 'FTP') {
      this.ftpForm.reset();
      this.ftpForm.controls['ftpActive'].setValue(configuration);
    } else if (type == 'DB') {
      this.dbForm.reset();
      this.dbForm.controls['dbActive'].setValue(configuration);
    } else if (type == 'CLOUD') {
      this.cloudForm.reset();
      this.cloudForm.controls['cloudActive'].setValue(configuration);
    } else if (type == 'API') {
      //this.apiForm.reset();
      //this.apiForm.controls['apiActive'].setValue(configuration);
    }
    this.btnReset();
  }




  // new function drag and drop

  test(event, item, data) {

    this.dragData = data;
    const a = this.stored.findIndex(x => x.id === item.id);
    if (a === -1) {
      item.selected = true;
      let idx = data.indexOf(item);
      item.selected = true;
      this.stored.push(item)
    } else {

    }

    this.stored.forEach((elem, i) => {
      console.log(elem);
      const uIx = data.findIndex(x => x.id === elem.id);
      data.splice(uIx, 1);

      if (this.stored.length === (i + 1)) {
        this.stored.forEach((ele, i) => {
          data.push(ele);
        });
      }
    });
    $('.selected').addClass('dragging');
    $('.selected').css({ 'opacity': 0.3, 'height': '0' });
    $('cdk-drag-placeholder').css({ 'opacity': 0 });

    this.count = this.stored.length;
    // if (this.ctrlPress !== false && this.stored.length > 0) {
    //   for (let itm of event.source.dropContainer.data) {
    //     if (itm.selected) {
    //       itm.isMultidragging = true;
    //     } else {
    //       itm.isMultidragging = false;
    //     }
    //   }
    // }

  }


  drop(event: CdkDragDrop<any[]>, dropdata) {
    console.log(event);
    $('.selected').removeClass('dragging');
    $('.selected').css({ 'opacity': 1, 'height': 'inherit' });
    $('cdk-drag-placeholder').css({ 'opacity': 1 });
    // if (this.stored.length === 1) {
    //   if (event.previousContainer === event.container) {
    //     moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    //   } else {
    //     transferArrayItem(event.previousContainer.data,
    //       event.container.data,
    //       event.previousIndex,
    //       event.currentIndex);
    //   }
    //   this.stored = [];
    //   event.container.data.forEach((d) => {
    //     d.isMultidragging = false;
    //     d.selected = false;
    //   });
    // } 
    if (this.stored.length === 1) {
      const previousIndex = event.previousContainer.data.findIndex(x => x.id === this.stored[0].id);
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, previousIndex, event.currentIndex);
      } else {
        transferArrayItem(event.previousContainer.data,
          event.container.data,
          previousIndex,
          event.currentIndex);
      }
      this.stored = [];
      event.container.data.forEach((d) => {
        d.isMultidragging = false;
        d.selected = false;
      });
    } else {
      if (event.previousContainer === event.container) {
        let b = event.currentIndex;
        let dropcloner = [...dropdata];
        console.log('currentIndex', b);
        this.stored.forEach((el, idx) => {
          const k = dropdata.findIndex(x => x.id === el.id);

          dropdata.splice(k, 1);
          if (this.stored.length === idx + 1) {
            this.stored.reverse().forEach(elm => {
              dropdata.splice(b, 0, elm);
            });
          }
        });
        this.stored = [];
        event.container.data.forEach((d) => {
          d.isMultidragging = false;
          d.selected = false;
        });


      } else {
        if (this.stored.length > 0) {
          event.previousContainer.data.slice(0).reverse().forEach((item, idx) => {
            if (item.selected) {
              event.previousContainer.data.splice(event.previousContainer.data.indexOf(item), 1);

              event.container.data.splice(event.currentIndex, 0, item)
              event.container.data.forEach((d) => {
                // d.isMultidragging=false;
                d.selected = false;
              })

            }
          });
          this.stored = [];

        } else {

          transferArrayItem(event.previousContainer.data,
            event.container.data,
            event.previousIndex,
            event.currentIndex);
          event.container.data.forEach(function (d) {
            d.isMultidragging = false;
            d.selected = false;
          });
          this.stored = [];

        }
      }

    }

    this.sortIndex = [];


  }
  findId(b, dropcloner, dropdata, n) {
    try {
      if (this.stored.includes(dropcloner[b - n])) {
        this.findId(b, dropcloner, dropdata, n + 1);
      } else {
        return dropcloner[b - n].id;
      }
    } catch (e) {
      return dropdata[0].id
    }


  }
  entered(e) {
    console.log(e);
    // moveItemInArray(e.container.data, e.previousIndex, e.currentIndex);
    moveItemInArray(e.container.data, e.item.data, e.container.data);
  }
  onKeyDown(e, item, data) {


    this.ctrlPress = e.ctrlKey;

    if (e.ctrlKey) {
      // item.index = idx;

      let idx = data.indexOf(item);
      this.sortIndex.push(idx);
      this.sortIndex.sort();
      console.log(this.sortIndex);
      this.stored = [];
      this.sortIndex.forEach(i => {

        data[i].selected = true;
        this.stored.push(data[i]);
      });
    }
  }



  // new end

  // tree fund start
  nodeSelect(e) {
    this.restructureGroups(0);
  }
  nodeUnselect(e) {
    this.restructureGroups(1);
  }
  setParentIdForchildren(source, id) {
    console.log(source);
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < source.children.length; i++) {
      const item = source.children[i];
      item.parentId = id;

      if (item.children.length) {
        this.setParentIdForchildren(item, id);
      }
    }
  }
  setParentId(source) {
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < source.length; i++) {
      let id = source[i].id;
      source[i].parentId = id;
      const item = source[i].children;
      if (item.length) {
        this.setParentIdchild(item, id);
      }
    }
  }
  setParentIdchild(source, id) {
    try {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < source.length; i++) {
        const item = source[i];
        item.parentId = id;
        if (item.children) {
          this.setParentIdchild(item.children, id);
        }
      }
    } catch (e) {
      console.log(e);
    }

  }
  redesignNodeSelector(source, ns) {
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < source.length; i++) {
      const item = source[i].children;
      this.redesignNodeSelLoop(item, ns);
    }
  }

  redesignNodeSelLoop(source, ns) {
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < source.length; i++) {
      ns.forEach(id => {
        if (source[i].id === id) {
          this.nodeSelected.push(source[i]);
        }
        if (source[i].children) {
          this.redesignNodeSelLoop(source[i].children, ns);
        }
      })
    }
  }
  onNodeDrop(e) {
    console.log(e);
    let d = e.dragNode;
    e.dragNode.parentId = e.dropNode.parentId;

    // this.nodeSelected = [];
    this.dragableGroups = [];
    let cloner = [...this.groups];
    this.setParentId(cloner);


    setTimeout(() => {
      this.groups = [];
      this.groups = cloner;
      // this.redesignNodeSelector(this.groups, nodeSelectCloner);

      this.restructureGroups(1);
    }, 100);



    // this.setParentIdForchildren(e.dragNode, e.dropNode.parentId);
    // setTimeout(() => {
    //   this.nodeSelected.forEach((elem, i) => {
    //     if (elem.id === d.id) {
    //       this.nodeSelected.splice(i, 1);
    //     }
    //   });

    //   this.restructureGroups(1);
    // });

    // const b = this.dragableGroups.findIndex(x => x.id === d.parentId);
    // if (b === -1) { } else {
    //   const ix = this.dragableGroups[b].child.findIndex(x => x.id === d.id);
    //   this.dragableGroups[b].child.splice(ix, 1);
    // }

  }

  restructureGroups(val) {
    if (val) {
      this.dragableGroups = [];
    }

    this.nodeSelected.forEach(elem => {

      console.log(elem);

      const fi = this.dragableGroups.findIndex(x => x.id === elem.parentId);
      if (fi === -1) {
        const k = this.groups.findIndex(x => x.id === elem.parentId);
        try {
          const obj = {
            id: elem.parentId,
            groupName: this.groups[k].label,
            dbValue: elem.dbValue,
            displayGroupOrder: elem.displayGroupOrder,
            searchKey: '',
            child: [{
              id: elem.id,
              parentId: elem.parentId,
              groupName: elem.label,
              parentSubGroupName: elem.parentSubGroupName,
              searchKey: '',
              dbValue: elem.dbValue,
              projectId: elem.projectId,
              moduleId: elem.moduleId,
              attributeList: elem.attributeList,
              relationId: elem.relationId,
              displayOrder: elem.displayOrder,
              levelId: elem.levelId,
            }],
            relationId: elem.relationId
          };

          this.dragableGroups.push(obj);
        } catch (e) {

        }

      } else {
        const obj = {
          id: elem.id,
          parentId: elem.parentId,
          groupName: elem.label,
          parentSubGroupName: elem.parentSubGroupName,
          searchKey: '',
          dbValue: elem.dbValue,
          projectId: elem.projectId,
          moduleId: elem.moduleId,
          attributeList: elem.attributeList,
          relationId: elem.relationId,
          displayOrder: elem.displayOrder,
          levelId: elem.levelId,
        };

        const y = this.dragableGroups[fi].child.findIndex(x => x.id === elem.id);
        if (y === -1) { this.dragableGroups[fi].child.push(obj); }
      }

    });
  }

  s3pathcheck(val) {
    let pathvalid = /(s3-|s3\.)?(.*)\.amazonaws\.com/.test(val);
    return pathvalid;
  }
  // tree fun end
  filepathCheck(val) {


    //  https: url check
    //  let pathvalid =  /^\\(\\[^\\"<>|]+){2,}$|^https?:\/\/[^/"<>|]+$/.test(val);

    //  strict with C: or D: regex also with file Extenstion
    // let pathvalid =/^(?:[\w]\:|\\)(\\[a-z_\-\s0-9\.]+)+\.(txt|gif|pdf|doc|docx|xls|xlsx)$/.test(val)



    // only with \data\mycomputer
    //  ^\\([A-z0-9-_+]+\\)*([A-z0-9]+)$

    // only with /data/mycomputer
    //  ^\/([A-z0-9-_+]+\/)*([A-z0-9]+)$

    //  strict with C: or D: regex
    // let pathvalid = /^([A-Za-z]:)(\\[A-Za-z_\-\s0-9\.]+)+$/.test(val);
    // console.log(pathvalid)
    // return pathvalid;

    // path stirng
    let pathvalid = /^([a-z]:|)\\(?:[^\\/:*?"<>|\r\n]+\\)*[^\\/:*?"<>|\r\n]*$/i.test(val)
    console.log(pathvalid)
    return pathvalid;
  }

  ftpSubmitForm(formData) {

    this.submitForm = true;
    this.checkFileName(formData.value.ftpFile);
    this.ValidatePassword(formData.value.ftpPassword);

    if (this.ftpForm.valid && this.PasswordErrorMessage == 'true' && this.checkFile === true && this.errorFile == 'true' && this.filepathCheck(formData.value.ftpPath)) {
      $('.spinner').show();
      let projectId = localStorage.getItem('project_id');
      var data = {
        ProjectId: Number(projectId),
        Configuration: formData.value.ftpActive,
        Type: 'FTP',
        HostAddress: formData.value.ftpHostAddress,
        UserName: formData.value.ftpUserName,
        Password: formData.value.ftpPassword,
        Port: formData.value.ftpPort,
        Path: formData.value.ftpPath,
        File: formData.value.ftpFile,
        DatabaseName: null,
        TableName: null,
        CloudName: null,
        S3Url: null,
        AccessKey: null,
        SecretKey: null,
        Active: true
      };

      if (data != null && projectId != null) {

        this.inpS.postRequest('InsertProjectConfigurationDetails', data).subscribe((response) => {
          if (response.status == 1) {
            this.successmessage("Project configuration " + (this.isUpdateBtn == false ? 'saved' : 'updated') + " successfully");
            this.isUpdateBtn = true;
            this.GetProjectConfigurationDetails();
            this.inpS.getAttribute().subscribe(resp => {
              this.attributeTypes = resp.data;
              this.GetProjectConfigurationList(this.attributeTypes);
            }, error => {
              $('.spinner').hide();
            });
            this.submitForm = false;
            this.saveRecords = true;
          }
          else {
            if (response.status == 2) {
              $('.spinner').hide();
              this.Errormessage(response.message);
            } else {
              $('.spinner').hide();
              this.Errormessage(response.message);
            }
          }
        }, err => {
          this.Errormessage(err.message);
          $('.spinner').hide();
        });
      } else {
        $('.spinner').hide();
        this.Errormessage('Please select one project for Project Definition');
      }
    }
  }


  dbSubmitForm(formData) {
    this.submitForm = true;
    this.ValidatePassword(formData.value.dbPassword);
    console.log(formData);
    console.log(this.PasswordErrorMessage);
    if (this.dbForm.valid && this.PasswordErrorMessage == 'true') {
      $('.spinner').show();

      let projectId = localStorage.getItem('project_id');
      var data = {
        ProjectId: Number(projectId),
        Configuration: formData.value.dbActive,
        Type: 'DB',
        HostAddress: formData.value.dbHostAddress,
        UserName: formData.value.dbUserName,
        Password: formData.value.dbPassword,
        Port: formData.value.dbPort,
        Path: null,
        File: null,
        DatabaseName: formData.value.dbDatabase,
        TableName: formData.value.dbTableName,
        CloudName: null,
        S3Url: null,
        AccessKey: null,
        SecretKey: null,
        Active: true
      };

      if (data != null && projectId != null) {
        this.inpS.postRequest('InsertProjectConfigurationDetails', data).subscribe((response) => {
          if (response.status == 1) {


            this.GetProjectConfigurationDetails();
            this.successmessage("Project configuration " + (this.isUpdateBtn == false ? 'saved' : 'updated') + " successfully");
            this.submitForm = false;
            this.saveRecords = true;
            this.isUpdateBtn = true;
          }
          else {
            $('.spinner').hide();
            if (response.status == 2) {
              this.Errormessage(response.message);
            } else {
              this.Errormessage(response.message);
            }
          }
        });
      } else {
        $('.spinner').hide();
        this.Errormessage('Invalid project id');
      }
    }
  }

  cloudSubmitForm(formData) {

    let accessKey = this.onlySpecialchars(formData.value.accessKey);
    let secretKey = this.onlySpecialchars(formData.value.secretKey);
    let accessKey1 = this.onlySpecialcharNumber(formData.value.accessKey);
    let secretKey1 = this.onlySpecialcharNumber(formData.value.secretKey);
    this.submitForm = true;

    if (this.cloudForm.valid && !accessKey && !secretKey && !accessKey1 && !secretKey1) {
      $('.spinner').show();
      let projectId = localStorage.getItem('project_id');
      var data = {
        ProjectId: Number(projectId),
        Configuration: formData.value.cloudActive,
        Type: 'CLOUD',
        HostAddress: null,
        UserName: null,
        Password: null,
        Port: null,
        Path: null,
        File: null,
        DatabaseName: null,
        TableName: null,
        CloudName: formData.value.cloudName,
        S3Url: formData.value.s3Url,
        AccessKey: formData.value.accessKey,
        SecretKey: formData.value.secretKey,
        Active: true
      };

      if (data != null && projectId != null) {
        this.inpS.postRequest('InsertProjectConfigurationDetails', data).subscribe((response) => {
          $('.spinner').hide()
          if (response.status == 1) {
            this.successmessage("Project configuration " + (this.isUpdateBtn == false ? 'saved' : 'updated') + " successfully");
            this.GetProjectConfigurationDetails();
            this.saveRecords = true;
            this.submitForm = false;
            this.isUpdateBtn = true;
          }
          else {
            $('.spinner').hide();
            if (response.status == 2) {
              this.Errormessage(response.message);
            } else {
              this.Errormessage(response.message);
            }
          }
        }, err => {
          $('.spinner').hide();
        });
      } else {
        this.Errormessage('Invalid project id');
      }
    }
  }



  apiSubmitForm(formData) {

    alert('working')
    // this.submitForm = true;
    // if (this.ftpForm.valid) {
    //   let projectId = localStorage.getItem('project_id');
    //   var data = {
    //     ProjectId: Number(projectId),
    //     Configuration: formData.value.apiActive,
    //     Type: 'FTP',
    //     HostAddress: formData.value.ftpHostAddress,
    //     UserName: formData.value.ftpUserName,
    //     Password: formData.value.ftpPassword,
    //     Port: formData.value.ftpPort,
    //     Path: formData.value.ftpPath,
    //     File: formData.value.ftpFile,
    //     DatabaseName: null,
    //     TableName: null,
    //     CloudName: null,
    //     S3Url: null,
    //     AccessKey: null,
    //     SecretKey: null,
    //     Active: true
    //   };

    //   if (data != null && projectId != null) {
    //     this.inpS.postRequest('InsertProjectConfigurationDetails', data).subscribe((response) => {
    //       if (response.status == 1) {
    //         this.successmessage("Project configuration saved successfully");
    //         this.submitForm = false;
    //       }
    //       else {
    //         if (response.status == 2) {
    //           this.Errormessage(response.message);
    //         } else {
    //           this.Errormessage(response.message);
    //         }
    //       }
    //     });
    //   } else {
    //     this.Errormessage('Invalid project id');
    //   }
    // }
  }



  ngOnInit() {
    this.user_id = parseInt(localStorage.getItem('user_id'));
    $('.spinner').show();
    this.selectedValue = true;
    //this.groupNamesuggestions();
    $('.modal').appendTo('#fullscreen');
    this.ftpForm = this.formBuilder.group({
      ftpHostAddress: ['', [Validators.required, Validators.pattern('((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))')]],
      ftpUserName: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]+$'), formValidators.nonNumeric, Validators.maxLength(50)]],
      ftpPassword: ["", [Validators.required, Validators.minLength(8), Validators.maxLength(15)]],
      ftpPort: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$')]],
      ftpPath: ['', [Validators.required]],
      ftpFile: ['', [Validators.required, Validators.pattern('^\.[^.]+\.(txt|gif|pdf|doc|docx|xls|xlsx)$'), Validators.maxLength(50), formValidators.nonNumeric]],
      ftpActive: ['0', Validators.required]
    });

    this.dbForm = this.formBuilder.group({
      dbHostAddress: ['', [Validators.required, Validators.pattern('((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))')]],
      dbPort: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$')]],
      dbDatabase: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]+$'), formValidators.nonNumeric, Validators.maxLength(50)]],
      dbUserName: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]+$'), formValidators.nonNumeric, Validators.maxLength(50)]],
      dbPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(15)]],
      dbTableName: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]+$'), formValidators.nonNumeric, Validators.maxLength(50)]],
      dbActive: ['0', Validators.required]
    });


    this.cloudForm = this.formBuilder.group({
      cloudName: ['', [Validators.required]],
      s3Url: ['', [Validators.required, Validators.pattern('^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$')]],
      accessKey: ['', [Validators.required, Validators.maxLength(50), formValidators.nonNumeric, Validators.pattern("[A-Za-z0-9_./*!-()&$@=;:+,?]*$")]],
      secretKey: ['', [Validators.required, Validators.maxLength(50), formValidators.nonNumeric, Validators.pattern('[A-Za-z0-9_./*!-()&$@=;:+,?]*$')]],
      cloudActive: ['0', Validators.required]
    });


    this.apiForm = this.formBuilder.group({
      restApiEndpoint: ['', [Validators.required]],
      httpRequest: ['', [Validators.required]],
      apiActive: ['0', Validators.required]
    });

    this.shrService.getSideBarDetail().subscribe(resp => {
      this.showSideBar = resp;

    });


    this.GetProjectConfigurationDetails();
    this.inpS.getAttribute().subscribe(resp => {
      this.attributeTypes = resp.data;
      this.GetProjectConfigurationList(this.attributeTypes);
      $('.spinner').hide();
    }, error => {
      $('.spinner').hide();
    });

    this.inpS.getgroups().subscribe(resp => {
      this.groups = [];
      $('.spinner').hide();
    }, error => {
      $('.spinner').hide();
    });

    // this.inpS.getDefaultAttribute().subscribe(resp => {
    //   this.defaultAttribute = resp.data;
    //   console.log(this.defaultAttribute); 
    // }, error => {

    // });
  }

  atleastonestring(val) {
    let regex = /^(?=.*[a-zA-Z]).+/
    return regex.test(val);
  }


  onlySpecialchars(value) {
    let regex = /^[^a-zA-Z0-9]+$/;
    if (regex.test(value)) {
      return true;
    } else {
      return false;
    }
  }

  onlySpecialcharNumber(value) {
    let regex = /(?=.*?[a-zA-Z])/;
    if (!regex.test(value)) {
      return true;
    } else {
      return false;
    }
  }


  public onlyAllowAlphanumeric(event: KeyboardEvent | any) {
    const pattern = /^[a-zA-Z0-9_-]*$/;
    console.log(!pattern.test(event.target.value));
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-zA-Z0-9_-]*$/, "");
    }

  }


  // groupNamesuggestions() {
  //   this.suggestionList = [
  //     { name: 'Afghanistan', code: 'AF' },
  //     { name: 'Åland Islands', code: 'AX' },
  //     { name: 'Albania', code: 'AL' },
  //     { name: 'Algeria', code: 'DZ' },
  //     { name: 'American Samoa', code: 'AS' },
  //     { name: 'AndorrA', code: 'AD' },
  //     { name: 'Belgium', code: 'BE' },
  //     { name: 'Belize', code: 'BZ' },
  //     { name: 'Benin', code: 'BJ' },
  //     { name: 'Bermuda', code: 'BM' },
  //     { name: 'Bhutan', code: 'BT' },
  //     { name: 'Bolivia', code: 'BO' },
  //     { name: 'Cyprus', code: 'CY' },
  //     { name: 'Czech Republic', code: 'CZ' },
  //     { name: 'Denmark', code: 'DK' },
  //     { name: 'Djibouti', code: 'DJ' },
  //     { name: 'Dominica', code: 'DM' },
  //     { name: 'Dominican Republic', code: 'DO' },
  //     { name: 'Ecuador', code: 'EC' },
  //     { name: 'Egypt', code: 'EG' },
  //     { name: 'Liechtenstein', code: 'LI' },
  //     { name: 'Lithuania', code: 'LT' },
  //     { name: 'Luxembourg', code: 'LU' },
  //     { name: 'Macao', code: 'MO' },
  //     { name: 'Macedonia, The Former Yugoslav Republic of', code: 'MK' },
  //     { name: 'Madagascar', code: 'MG' },
  //     { name: 'Malawi', code: 'MW' },
  //     { name: 'Paraguay', code: 'PY' },
  //     { name: 'Peru', code: 'PE' },
  //     { name: 'Philippines', code: 'PH' },
  //     { name: 'Pitcairn', code: 'PN' },
  //     { name: 'Poland', code: 'PL' },
  //     { name: 'Portugal', code: 'PT' },
  //     { name: 'Puerto Rico', code: 'PR' },
  //     { name: 'Qatar', code: 'QA' }
  //   ]
  // }



  isCheckNumber(value) {
    let pattern = /^[0-9]+$/;
    if ((value && !pattern.test(value))) {
      return false;
    } else {
      return true;
    }
  }


  nonNumericOnly(inputValue, attributeType): boolean {

    let value = inputValue.replace(/\s/g, '');
    if (attributeType == 'attribute') {
      if ((value && !value.match(/^[0-9]+$/))) {
        this.nonNumericAttr = 'true';
      } else {
        this.nonNumericAttr = "should not be numeric only";
      }
    } else if (attributeType == 'display') {
      if ((value && !value.match(/^[0-9]+$/))) {
        this.nonNumericDisplay = 'true';
      } else {
        this.nonNumericDisplay = "should not be numeric only";
      }
    } else if (attributeType == 'odpDisplay') {
      if ((value && !value.match(/^[0-9]+$/))) {
        this.nonNumericOdpDisplay = 'true';
      } else {
        this.nonNumericOdpDisplay = "should not be numeric only";
      }
    }
    return true;
  }


  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }



  ValidatePassword(password: string) {
    var input = password;
    if (input == null || input == '') {
      this.PasswordErrorMessage = "Password should not be empty";
    }
    var hasNumber = new RegExp("[0-9]+");
    var hasUpperChar = new RegExp("[A-Z]+");
    var hasMiniMaxChars = new RegExp("^.{8,15}$");
    var hasLowerChar = new RegExp("[a-z]+");
    //  var hasSymbols = new RegExp("[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
    var hasSymbols = new RegExp("[!@#$%^&*(),.?/:{}|<>]");

    if (!hasUpperChar.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one upper case letter.";
      return this.PasswordErrorMessage;
    }
    else if (!hasLowerChar.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one lower case letter.";
      return this.PasswordErrorMessage;
    }
    else if (!hasMiniMaxChars.test(input)) {
      this.PasswordErrorMessage = "Password should be min 8 to 15 characters.";
      return this.PasswordErrorMessage;
    }
    else if (!hasNumber.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one numeric value.";
      return this.PasswordErrorMessage;
    } else if (!hasSymbols.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one special case character.";
      return this.PasswordErrorMessage;
    }
    else {
      return this.PasswordErrorMessage = 'true';
    }
  }



  singleOrMultiple(val) {
    this.selectedValue = val;
    this.pleaseWait = false;
    this.fileName = 'Choose File';
    if (parseInt(val) === 2) {
      this.formValues.isBulkUpload = 2;
    } else if (parseInt(val) === 1) {
      this.formValues.isBulkUpload = 1;
    } else {
      this.formValues.isBulkUpload = 3;
    }
  }

  checkFileName(value) {
    this.errorFile = 'true';
    if (value.indexOf('.') !== -1) {
      var array = value.split(/\.(?=[^\.]+$)/);
      var extension = ['txt', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
      var n = extension.includes(array[1]);
      if (n) {
        let splitValue = value.replace(/\s/g, '').split('.');
        if ((splitValue.length > 0 && splitValue[0] && splitValue[0].match(/^[0-9]+$/))) {
          this.checkFile = false;
          return false;
        }else if(splitValue[0].match(/(?=.*?[a-zA-Z])/)){
          this.checkFile = false;
          return false;
        }
        this.checkFile = true;
      } else {
        this.errorFile = 'Invalid file extension';
      }
    } else {
      this.errorFile = 'Invalid file name'
    }
  }



  onItemSelect(val) {

    this.valuesInvalid.min = false;
    this.valuesInvalid.max = false;
    this.formValues.min = '';
    this.formValues.max = '';
    this.valuesInvalid.values = false;
    this.onItemSelectCount += 1
    if (this.isEditAttribute) {
      this.editFormValues.inputValues = '';
      if (this.onItemSelectCount > 1) {
        this.editFormValues.min = '';
        this.editFormValues.max = '';
      }
    } else {
      $('#singlebulkvalue').val("");
      $('#inputValues').val('');
    }

    this.fileName = 'Choose File';
    this.formValues.inputValues = '';
    this.inputFrom = val.attributeTypeName.toUpperCase();

    if (this.inputFrom === 'DATE RANGE') {
      let start;
      let end;
      this.cdateFormat = 'DD MMM, YYYY';
      console.log(this.editAttributeArray);
      if (this.isEditAttribute) {
        if (this.editAttributeArray[this.editformIndex].dateFrom && this.editAttributeArray[this.editformIndex].dateTo) {
          start = moment(new Date(this.editAttributeArray[this.editformIndex].dateFrom), this.editAttributeArray[this.editformIndex].dateFormat.format);
          end = moment(new Date(this.editAttributeArray[this.editformIndex].dateTo), this.editAttributeArray[this.editformIndex].dateFormat.format);
          this.cdateFormat = this.editAttributeArray[this.editformIndex].dateFormat.format ? this.editAttributeArray[this.editformIndex].dateFormat.format : this.cdateFormat;
        } else {
          start = moment().subtract(29, 'days');
          end = moment();
          this.cdateFormat = 'DD MMM, YYYY';
        }

      } else {
        start = moment().subtract(29, 'days');
        end = moment();
        this.cdateFormat = 'DD MMM, YYYY';
      }

      setTimeout(() => {
        var $picker = $('input[name="dateRange"]').daterangepicker({
          container: '#rangedate',
          startDate: start,
          endDate: end,
          opens: 'left',
          drops: 'auto',
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
        }, this.cb(start, end));

        this.cb(start, end);
        $('input[name="dateRange"]').on('apply.daterangepicker', (ev, picker) => {
          $('input[name="dateRange"]').val(picker.startDate.format(this.cdateFormat) + ' - ' + picker.endDate.format(this.cdateFormat));
          if (this.isEditAttribute) {
            this.editFormValues.dateFrom = picker.startDate.format(this.cdateFormat);
            this.editFormValues.dateTo = picker.endDate.format(this.cdateFormat);
          } else {
            this.formValues.dateFrom = picker.startDate.format(this.cdateFormat);
            this.formValues.dateTo = picker.endDate.format(this.cdateFormat);
          }
        });

        $('.drp-buttons .drp-selected').hide();

        $picker.on('show.daterangepicker', (ev, picker) => {
          if (picker.element.offset().top - $(window).scrollTop() + picker.container.outerHeight() + 60 > $(window).height()) {
            picker.drops = 'up';
          } else {
            picker.drops = 'down';
          }
          picker.move();
        });

      }, 100);

    }
  }


  cb(start, end) {
    $('input[name="dateRange"]').val(start.format(this.cdateFormat) + ' - ' + end.format(this.cdateFormat));
    this.formValues.dateFrom = start.format(this.cdateFormat);
    this.formValues.dateTo = end.format(this.cdateFormat);
    console.log(start.format(this.cdateFormat) + ' - ' + end.format(this.cdateFormat));
  }


  addAttributeList() {
    this.issubmitted = true;

    this.formValues.attributeName = this.formValues.attributeName.replace(/[&\/\\#,+()$~%.'":^!@=;*?<>{}]/g, '');
    let filteredElements = this.attrList[0].maingroup.filter(f => f.attributeName.toLowerCase() === this.formValues.attributeName.toLowerCase());
    if (filteredElements.length > 0) {
      this.Errormessage("Attribute already exists");
      return false;
    }


    let inputvalues = this.formValues.inputValues;
    if (inputvalues[0] != null && inputvalues[0] != '') {
      inputvalues = inputvalues.replace(/^[,\s]+|[,\s]+$/g, '');
      inputvalues = inputvalues.replace(/\s*,\s*/g, ',');
    }
    const attrTypeName = this.formValues.selAttrTypes.attributeTypeName.toUpperCase();
    if (attrTypeName === 'CHECKBOX' || attrTypeName === 'DROPDOWN' || attrTypeName === 'RADIO BUTTON') {
      if (inputvalues === '') {
        this.valuesInvalid.values = true;
        return;
      }

    } else {
      this.valuesInvalid.values = false;
    }
    if (attrTypeName === 'LENGTH' || attrTypeName === 'NUMERIC RANGE') {


      if (this.formValues.min === '' || this.formValues.min == null) {
        this.valuesInvalid.min = true;
        return false;
      } else { this.valuesInvalid.min = false; }

      if (this.formValues.max === '' || this.formValues.max == null) {
        this.valuesInvalid.max = true;
        return false;
      } else { this.valuesInvalid.max = false; }

      if (!this.isCheckNumber(this.formValues.min)) {
        this.Errormessage("Please enter valid min value");
        return false;
      }

      if (!this.isCheckNumber(this.formValues.max)) {
        this.Errormessage("Please enter valid max value");
        return false;
      }

      if (Number(this.formValues.min) >= Number(this.formValues.max)) {
        this.Errormessage("Please enter valid max value");
        return false;
      }
    }

    if (attrTypeName === 'DATE RANGE') {
      if (this.formValues.dateFormat === '') {
        this.valuesInvalid.dateFormat = true;
        console.log('return', true);
        return;
      } else { this.valuesInvalid.dateFormat = false; }
    }


    const obj = {
      id: Math.floor(Math.random() * 1000),
      attributeName: this.formValues.attributeName.trim().replace(/\s/g, ""),
      displayName: this.formValues.displayName.trim(),
      isSameAsDisplayName: this.formValues.isSameAsDisplayName,
      opdisplayName: this.formValues.opdisplayName,
      selAttrTypes: this.formValues.selAttrTypes,
      inputValues: inputvalues.split(','),
      min: this.formValues.min,
      max: this.formValues.max,
      dateFrom: this.formValues.dateFrom,
      dateTo: this.formValues.dateTo,
      dateFormat: this.formValues.dateFormat,
      selected: false,
      flag: 1,
      dbValue: false
    };


    this.attrList[0].maingroup.push(obj);

    this.formValues = {
      isEdit: false,
      attributeName: '',
      displayName: '',
      isSameAsDisplayName: true,
      opdisplayName: '',
      selAttrTypes: '',
      inputValues: '',
      min: '',
      max: '',
      dateFrom: '',
      dateTo: '',
      dateFormat: '',
      isBulkUpload: 1
    };
    this.isreload = true;
    this.inputFrom = '';
    this.fileName = 'Choose File';
    this.issubmitted = false;
  }

  sameAsInput(val) {
    if (this.isEditAttribute) {
      if (this.editFormValues.isSameAsDisplayName) {
        this.editFormValues.opdisplayName = val;
      }

    } else {
      if (this.formValues.isSameAsDisplayName) {
        this.formValues.opdisplayName = val;
      }

    }

  }
  checkBoxSameAsInput(val) {
    console.log(val);
    if (this.isEditAttribute) {
      if (this.editFormValues.isSameAsDisplayName) {
        this.editFormValues.isSameAsDisplayName = false;

      } else {
        this.editFormValues.opdisplayName = this.editFormValues.displayName;
        this.editFormValues.isSameAsDisplayName = true;
      }
    } else {
      if (this.formValues.isSameAsDisplayName) {
        this.formValues.isSameAsDisplayName = false;
      } else {
        this.formValues.isSameAsDisplayName = true;
        this.formValues.opdisplayName = this.formValues.displayName;
      }


    }

  }

  isFill(val, isvalid, data) {

    const attrTypeName = data.selAttrTypes.attributeTypeName.toUpperCase();

    if (attrTypeName === 'CHECKBOX' || attrTypeName === 'DROPDOWN' || attrTypeName === 'RADIO BUTTON') {
      let inputvalues = data.inputValues;
      inputvalues = inputvalues.replace(/^[,\s]+|[,\s]+$/g, '');
      inputvalues = inputvalues.replace(/\s*,\s*/g, ',');
      if (inputvalues === '' && inputvalues == null) {
        this.valuesInvalid.values = true;
      } else {
        this.valuesInvalid.values = false;
      }
    }

    if (attrTypeName === 'LENGTH' || attrTypeName === 'NUMERIC RANGE') {

      if (data.min === '' && data.min == null) {
        this.valuesInvalid.min = true;
        return;
      } else { this.valuesInvalid.min = false; }
      if (data.max === '' && data.max == null) {
        this.valuesInvalid.max = true;
        return;
      } else { this.valuesInvalid.max = false; }


    }

  }

  isFilldateFormat(val, isvalid) {

    if (val.format === '') {
      this.valuesInvalid.dateFormat = true;
    } else {
      this.valuesInvalid.dateFormat = false;
      console.log(false, isvalid);
    }
  }

  grpName(value) {
    let grpNameValue = value.replace(/\s/g, '');
    this.isCheckGrpName = false;
    if (grpNameValue == '') {
      this.isCheckGrpName = 'required';
      return false;
    }

    if ((grpNameValue && grpNameValue.match(/^[0-9]+$/))) {
      this.isCheckGrpName = 'numeric';
      return false;
    }


    if ((grpNameValue && !grpNameValue.match(/(?=.*?[a-zA-Z])/))) {
      this.isCheckGrpName = 'specialCharNumeric';
      return false;
    }
  }


  search(event) {
    console.log(event.query);
    let grpNameValue = event.query.replace(/\s/g, '');
    this.isCheckGrpName = false;
    if (grpNameValue == '') {
      this.isCheckGrpName = 'required';
      return false;
    }

    if ((grpNameValue && grpNameValue.match(/^[0-9]+$/))) {
      this.isCheckGrpName = 'numeric';
      return false;
    }

    if ((grpNameValue && !grpNameValue.match(/(?=.*?[a-zA-Z])/))) {
      this.isCheckGrpName = 'specialCharNumeric';
      return false;
    }


    let filtered: any[] = [];
    let query = event.query;
    for (let i = 0; i < this.suggestionList.length; i++) {
      let group = this.suggestionList[i];
      if (group.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(group.name);
      }
    }

    this.groupNameSuggestion = filtered;
  }

  subGrpName(value) {
    let subGrpNameValue = value.replace(/\s/g, '');
    this.isCheckSubGrpName = true;

    if (subGrpNameValue == '' && subGrpNameValue == null) {
      this.isCheckSubGrpName = 'required';
      return false;
    }

    if ((subGrpNameValue && subGrpNameValue.match(/^[0-9]+$/))) {
      this.isCheckSubGrpName = 'numeric';
      return false;
    }

    if ((subGrpNameValue && !subGrpNameValue.match(/(?=.*?[a-zA-Z])/))) {
      this.isCheckSubGrpName = 'specialCharNumeric';
      console.log(this.isCheckSubGrpName);
      return false;
    }

  }

  // drag and drop functions
  addSubGroup(name) {

    this.grpName(name);
    if (this.isCheckGrpName == 'numeric' || this.isCheckGrpName == 'specialCharNumeric') {
      return false;
    }

    if (name == '' || name == null) {
      this.isCheckGrpName = 'required';
      return false;
    }

    let findGroupName = this.nodeSelected.filter(f => f.label.trim().toLowerCase() === name.trim().toLowerCase());
    if (findGroupName.length > 0) {
      this.Errormessage('Group name already exists');
      return false;
    }

    const obj: any = {
      id: Math.floor(Math.random() * 1000),
      label: name,
      parentSubGroupName: '',
      searchKey: '',
      isSelected: true,
      expandedIcon: 'pi pi-folder-open',
      collapsedIcon: 'pi pi-folder',
      displayOrder: 1,
      levelId: 1,
      attributeList: [],
      children: []
    }
    obj.parentId = obj.id;
    obj.relationId = obj.id;
    obj.dbValue = false;

    const k = this.currentProjectGroups.findIndex(x => x.name.toLowerCase() === name.toLowerCase());
    if (k === -1 || k === undefined) {
      this.groups.push(obj);
      this.nodeSelected.push(obj);
      this.restructureGroups(0);
      this.grpieName = '';
      $('#attr').val('');
      this.currentProjectGroups.push({
        name: name,
        code: localStorage.getItem('project_id')
      })
    } else {
      this.Errormessage('Group Name already exists.');
    }


  }
  nodeisSelected(node) {
    if (node.isSelected) {
      node.isSelected = false;
      const ix = this.nodeSelected.findIndex(x => x.id === node.id);
      this.nodeSelected.splice(ix, 1);
    } else {
      node.isSelected = true;
      this.nodeSelected.push(node);
    }

    this.restructureGroups(1);
    console.log(this.nodeSelected);
  }

  addSubgroupModal(val) {

    this.subGrpName(val);

    if (this.isCheckSubGrpName == 'numeric' || this.onlySpecialchars(val) || this.isCheckSubGrpName == 'specialCharNumeric') {
      return false;
    }

    if (val.trim() == '' || val.trim() == null) {
      this.isCheckSubGrpName = 'required';
      return false;
    }

    if (this.isCheckSubGrpName != true) {
      return false;
    }

    let filteredSubGroupName = this.nodeSelected.filter(f => f.label.trim().toLowerCase() === val.trim().toLowerCase());
    if (filteredSubGroupName.length > 0) {
      this.Errormessage("Sub group already exists");
      return false;
    }

    const parentGroups = this.finder(this.groups, this.cmgroup.id);
    const obj: any = {
      id: Math.floor(Math.random() * 1000),
      label: val,
      parentSubGroupName: parentGroups.label,
      searchKey: '',
      relationId: this.cmgroup.id,
      isSelected: true,
      expandedIcon: 'pi pi-folder-open',
      collapsedIcon: 'pi pi-folder',
      displayOrder: parentGroups.children.length + 1,
      levelId: parentGroups.levelId + 1,
      attributeList: [],
      children: []
    };
    obj.parentId = this.cmgroup.parentId;
    obj.dbValue = false;
    const findobj = this.finder(this.groups, this.cmgroup.id);
    findobj.children.push(obj);
    this.nodeSelected.push(obj);
    this.restructureGroups(0);
    console.log(findobj);
    $('#addSubgroup').modal('hide');
  }


  childNameChange(source, id, val) {
    for (let i = 0; i < source.length; i++) {
      const b = source.findIndex(x => x.id === id);
      if (b === -1) {
        if (source.length === (i + 1)) {
          if (source.child.length) {
            this.childNameChange(source.child, id, val);
          }
        }
      } else {
        source[b].groupName = val;
      }
    }
  }

  childrenNameChange(source, id, val) {
    for (let i = 0; i < source.length; i++) {
      const b = source.findIndex(x => x.id === id);
      if (b === -1) {
        if (source.length === (i + 1)) {
          if (source.children.length) {
            this.childrenNameChange(source.child, id, val);
          }
        }
      } else {
        source[b].label = val;
      }
    }
  }

  editSubGroupName(val) {

    console.log(val);

    const pindex = this.dragableGroups.findIndex(x => x.id === this.cmgroup.parentId);
    const gPIdx = this.groups.findIndex(x => x.id === this.cmgroup.parentId);
    if (this.dragableGroups[pindex].dbValue == true) {
      let inputJson = [{
        AttributeId: Number(""),
        AttributeType: "",
        AttributeValue: [{
          value: "",
          min: Number(""),
          max: Number(""),
          format: "",
          range: "",
        }],
        DisplayName: "",
        OutputName: "",
        OutPutFlag: false,
        AttributeDisplayOrder: Number(""),
        GroupId: this.dragableGroups[pindex].id,
        DisplayGroupName: val,
        DisplayGroupOrder: this.dragableGroups[pindex].displayGroupOrder,
        UpdatedBy: Number(localStorage.getItem('user_id'))
      }];

      this.updateAttributeAPI(inputJson, 'group name');

    } else {

      if (this.cmgroup.parentId === this.cmgroup.id) {
        this.dragableGroups[pindex].groupName = val;
        const cIndex = this.dragableGroups[pindex].child.findIndex(x => x.id === this.cmgroup.id);
        this.dragableGroups[pindex].child[cIndex].groupName = val;

        this.groups[gPIdx].label = val;
      } else {
        this.childNameChange(this.dragableGroups[pindex].child, this.cmgroup.id, val);
        this.childrenNameChange(this.groups[gPIdx].children, this.cmgroup.id, val);
      }
      $('#addSubgroup').modal('hide');


    }
  }



  // recursive method to find nested loop
  finder(source, id) {
    for (let i = 0; i < source.length; i++) {
      const item = source[i];
      if (item.id === id) {
        return item;
      }
      // Item not returned yet. Search its children by recursive call.
      if (item.children) {
        const subresult = this.finder(item.children, id);

        // If the item was found in the subchildren, return it.
        if (subresult) {
          return subresult;
        }

      }
    }
    // Nothing found yet? return null.
    return null;
  }

  remover(source, id) {
    console.log(source);
    for (let i = 0; i < source.children.length; i++) {
      const item = source.children[i];
      console.log(item);
      if (item.id === id) {
        source.children.splice(i, 1);
      }

      if (item.children.length) {
        this.remover(item, id);
      }
    }

  }
  // recursive method to find nested loop

  openaddSgModal(sg, mg, identity, type) {

    console.log(sg);
    console.log(mg);
    console.log(identity);
    console.log(type);

    if (type == 'edit') {
      if (sg.parentSubGroupName != '' && sg.parentSubGroupName != null) {
        this.dynamicName = 'sub group';
      } else {
        this.dynamicName = 'group';
      }
    } else {
      this.dynamicName = 'sub group';
    }


    if (identity) {
      this.subGroupUpdate = true;
      $('#sgName').val(sg.groupName);
    } else {
      $('#sgName').val('');
      this.subGroupUpdate = false;
    }

    this.cmgroup = {
      id: sg.id,
      parentId: sg.parentId ? sg.parentId : sg.id
    };
  }


  reassignAttributeList(source) {
    for (let a = 0; a < source.length; a++) {
      for (let i = 0; i < source[a].attributeList.length; i++) {
        if (!source[a].attributeList[i].dbValue) {
          this.attrList[0].maingroup.push(source[a].attributeList[i]);
        }
      }
      if (source[a].children) {
        this.reassignAttributeList(source[a].children);
      }
    }

  }
  alertdeletegroup(sg, group, sgIx, pgIx) {
    this.cmgroup.sg = sg;
    this.cmgroup.group = group;
    let findData = this.finder(this.groups, sg.id);
    console.log(findData);
    this.cmgroup.sgIx = sgIx;
    this.cmgroup.pgIx = pgIx;
    this.cmgroup.groupName = sg.groupName;
  }

  GetDeleteApiData(source) {
    const apiData = {};
    for (let a = 0; a < source.length; a++) {
      if (source[a].dbValue) {
        if (!apiData.hasOwnProperty(source[a].id)) {
          apiData[source[a].id] = {};
          if (!apiData[source[a].id].hasOwnProperty('AttributeList')) {
            apiData[source[a].id]['AttributeList'] = [];
            apiData[source[a].id]['GroupId'] = Number(source[a].id);
            apiData[source[a].id]['ProjectId'] = Number(source[a].projectId);
            apiData[source[a].id]['ModuleId'] = Number(source[a].moduleId);
            apiData[source[a].id]['UpdatedBy'] = Number(this.user_id);
            source[a].attributeList.forEach((attrV, attrI) => {
              let attributeList = {
                AttributeId: Number(attrV.id),
                AttributeName: attrV.attributeName
              }
              apiData[source[a].id]['AttributeList'].push(attributeList);
            });
          }
        }
      }

      if (source[a].children) {
        this.GetDeleteApiData(source[a].children);
      }
    }
  }

  removalGroups(data) {
    if (data && data.children && data.children.length > 0) {
      for (let i = 0; i < data.children.length; i++) {
        this.removalgroups.push(data.children[i]);
        if (data && data.children && data.children[i].children && (data.children[i].children.length > 0)) {
          this.removalGroups(data.children[i].children);
        }
      }
    }
  }

  deleteGroup() {
    const sg = this.cmgroup.sg;
    const data = this.cmgroup.group;
    const i = this.cmgroup.sgIx;
    const dId = this.cmgroup.pgIx;

    let apiData = {};
    const clonerofgroups = Object.assign([], this.groups);
    let findObj = this.finder(clonerofgroups, sg.id);
    this.removalgroups = [];
    this.removalgroups.push(findObj)
    this.removalGroups(findObj);

    if (this.removalGroups.length > 0) {
      this.removalgroups.forEach((elem, index) => {
        if (elem.dbValue) {
          if (!apiData.hasOwnProperty(elem.groupid)) {
            apiData[elem.id] = {};
            if (!apiData[elem.id].hasOwnProperty('AttributeList')) {
              apiData[elem.id]['AttributeList'] = [];
              apiData[elem.id]['GroupId'] = Number(elem.id);
              apiData[elem.id]['ProjectId'] = Number(elem.projectId),
                apiData[elem.id]['ModuleId'] = Number(elem.moduleId),
                apiData[elem.id]['UpdatedBy'] = Number(this.user_id)
              elem.attributeList.forEach((attrV, attrI) => {
                let attributeList = {
                  AttributeId: Number(attrV.id),
                  AttributeName: attrV.attributeName
                }
                apiData[elem.id]['AttributeList'].push(attributeList);
              });
            }
          }
        }
      });


      if (apiData) {
        $('.spinner').show();
        this.inpS.postRequest('DeleteGroup', Object.values(apiData)).subscribe((response) => {
          if (response.status) {
            this.dragableGroups = [];
            this.groups = [];
            this.resultData = [];
            this.issubmitted = false;
            $('.spinner').hide();
            this.inpS.getAttribute().subscribe(resp => {
              this.attributeTypes = resp.data;
              this.GetProjectConfigurationList(this.attributeTypes);
              $('#deletegroup').modal('hide');
              this.successmessage("Project groups deleted successfully");
            }, error => {

            });
          }
          else {
            $('.spinner').hide();
            if (response.status == 2) {
              this.Errormessage(response.message);

            } else {
              this.Errormessage(response.message);
            }
          }
        });
      }
    }

    this.reassignAttributeList([findObj]);
    const rgI = this.groups.findIndex(x => x.id === sg.id);

    if (rgI === -1) {
      data.splice(i, 1);
      this.reassignAttributeList(findObj);
      const gId = this.groups.findIndex(x => x.id === sg.parentId);
      const g = this.groups[gId];
      this.remover(g, sg.id);
    } else {
      this.dragableGroups.splice(dId, 1);
      this.groups.splice(rgI, 1);
    }
    this.dragableGroups = [];
    let nodeSelectCloner = [];
    this.nodeSelected.forEach(elem => {
      nodeSelectCloner.push(elem.id);
    });


    setTimeout(() => {
      this.nodeSelected = [];
      this.redesignNodeSelLoop(this.groups, nodeSelectCloner);
      this.restructureGroups(1);
    }, 0);

    const k = this.currentProjectGroups.findIndex(x => x.name.toLowerCase() === sg.groupName.toLowerCase());
    if (k === -1) { } else {
      this.currentProjectGroups.splice(k, 1);
    }
    $('#deletegroup').modal('hide');

  }

  delAttribute() {

    if (this.delAttributeModal.dbValue) {
      $('.spinner').show();
      let delAttrJson = {
        AttributeId: this.delAttributeModal.attrId,
        ProjectId: this.delAttributeModal.projectId,
        ModuleId: this.delAttributeModal.moduleId,
        UpdatedBy: this.user_id
      };
      this.inpS.postRequest('DeleteMappedAttr', delAttrJson).subscribe((response) => {

        if (response.status) {
          this.delAttributeModal.data.splice(this.delAttributeModal.index, 1);
          $('#delAttribute').modal('hide');
          this.dragableGroups = [];
          this.groups = [];
          this.resultData = [];
          this.issubmitted = false;
          $('.spinner').hide();
          this.inpS.getAttribute().subscribe(resp => {
            this.attributeTypes = resp.data;
            this.GetProjectConfigurationList(this.attributeTypes);
            this.successmessage(response.message);
          }, error => {

          });

        }
        else {
          $('.spinner').hide();
          if (response.status == 2) {
            this.Errormessage(response.message);

          } else {
            this.Errormessage(response.message);
          }
        }
      });


    } else {
      this.delAttributeModal.data.splice(this.delAttributeModal.index, 1);
      $('#delAttribute').modal('hide');
      let filteredAttr = this.allDefaultAttr.filter(f => f.id == this.delAttributeModal.attrId);
      if (filteredAttr.length == 1) {
        this.defaultAttribute.push(filteredAttr[0]);
      }
    }

  }

  deleteAttributeModalOpen(data, index) {
    this.delAttributeModal.data = data;
    this.delAttributeModal.dbValue = data[index].dbValue;
    this.delAttributeModal.projectId = data[index].projectId;
    this.delAttributeModal.moduleId = data[index].moduleId;
    this.delAttributeModal.index = index;
    this.delAttributeModal.attrId = data[index].id;
    this.delAttributeModal.name = data[index].attributeName;
  }
  addOrRemoveFlag(atr, event) {
    if (atr.flag) {
      atr.flag = 0;
    } else {
      atr.flag = 1;
    }
    $('.dropdown-menu.dropdown-menu-right').removeClass('show');
    event.stopPropagation();
  }

  updateAttributeAPI(inputjson, uptName) {


    if (inputjson.length > 0 && inputjson != null) {
      $('.spinner').show();
      this.inpS.postRequest('UpdateProjectConfigurationAttr', inputjson).subscribe((response) => {
        $('.spinner').hide();
        if (response.status == 1) {
          this.isEditAttribute = false;
          $('#editAttr').modal('hide');
          $('#addSubgroup').modal('hide');
          this.successmessage("Project " + uptName + " updated successfully");
          this.dragableGroups = [];
          this.groups = [];
          this.resultData = [];
          this.issubmitted = false;
          this.inpS.getAttribute().subscribe(resp => {
            this.attributeTypes = resp.data;
            this.GetProjectConfigurationList(this.attributeTypes);
          }, error => {

          });
          $('.spinner').hide();

        }
        else {
          $('.spinner').hide();
          if (response.status == 2) {
            this.Errormessage(response.message);

          } else {
            this.Errormessage(response.message);
          }
        }
      });
    }



  }



  editAttribute(data, index, editdata, event) {

    console.log(data);
    console.log(editdata);
    this.editData = JSON.parse(JSON.stringify(data));;
    this.editformIndex = index;
    this.editAttributeArray = editdata;
    event.stopPropagation();
    this.isEditAttribute = true;
    this.onItemSelectCount = 0;
    this.onItemSelect(data.selAttrTypes);

    $('#editAttr').modal('show');
    this.editFormValues = data;
    this.fileName = 'Choose File';

  }

  updateAttribute() {
    this.issubmitted = true;
    let filteredElements = this.attrList[0].maingroup.filter(f => f.attributeName.toLowerCase() === this.editFormValues.attributeName.toLowerCase() && f.id != this.editFormValues.id);
    if (filteredElements.length > 0) {
      this.Errormessage("Attribute already exists");
      return false;
    }


    let inputvalues = this.editFormValues.inputValues;
    if (inputvalues != null && inputvalues != '') {
      inputvalues = inputvalues.replace(/^[,\s]+|[,\s]+$/g, '');
      inputvalues = inputvalues.replace(/\s*,\s*/g, ',');
    }
    const attrTypeName = this.editFormValues.selAttrTypes.attributeTypeName.toUpperCase();
    if (attrTypeName === 'CHECKBOX' || attrTypeName === 'DROPDOWN' || attrTypeName === 'RADIO BUTTON') {
      if (inputvalues === '') {
        this.valuesInvalid.values = true;
        return;
      }

    } else {
      this.valuesInvalid.values = false;
    }
    if (attrTypeName === 'LENGTH' || attrTypeName === 'NUMERIC RANGE') {

      if (this.editFormValues.min === '' || this.editFormValues.min == null) {
        this.valuesInvalid.min = true;
        return;
      } else { this.valuesInvalid.min = false; }
      if (this.editFormValues.max === '' || this.editFormValues.max == null) {
        this.valuesInvalid.max = true;
        return;
      } else { this.valuesInvalid.max = false; }

      if (!this.isCheckNumber(this.formValues.min)) {
        this.Errormessage("Please enter valid min value");
        return false;
      }

      if (!this.isCheckNumber(this.formValues.max)) {
        this.Errormessage("Please enter valid max value");
        return false;
      }

      if (Number(this.editFormValues.min) >= Number(this.editFormValues.max)) {
        this.Errormessage("Please enter valid maximum value");
        return false;
      }
    }
    if (attrTypeName === 'DATE RANGE') {
      if (this.editFormValues.dateFormat === '') {
        this.valuesInvalid.dateFormat = true;
        return;
      } else { this.valuesInvalid.dateFormat = false; }

    }


    this.editFormValues.itemName = this.editFormValues.displayName;
    this.inputFrom = '';
    this.editData = this.editFormValues;
    if (this.editFormValues.dbValue == true) {
      let inputJson = [{
        AttributeId: this.editData.id,
        AttributeType: this.editData.selAttrTypes.attributeTypeName,
        AttributeValue: [{
          value: this.editData.inputvalues,
          min: (this.editData.selAttrTypes.attributeTypeName.toUpperCase() == 'LENGTH' || this.editData.selAttrTypes.attributeTypeName.toUpperCase() == 'NUMERIC RANGE') ? Number(this.editData.min) : Number(""),
          max: (this.editData.selAttrTypes.attributeTypeName.toUpperCase() == 'LENGTH' || this.editData.selAttrTypes.attributeTypeName.toUpperCase() == 'NUMERIC RANGE') ? Number(this.editData.max) : Number(""),
          format: (this.editData.selAttrTypes.attributeTypeName.toUpperCase() == 'DATE RANGE') ? this.editData.dateFormat.format : "",
          range: (this.editData.selAttrTypes.attributeTypeName.toUpperCase() == 'DATE RANGE') ? this.editData.dateFrom + ' - ' + this.editData.dateTo : "",
        }],
        DisplayName: this.editData.displayName,
        OutputName: this.editData.opdisplayName,
        OutPutFlag: (this.editData.flag == 1) ? true : false,
        AttributeDisplayOrder: this.editData.attributedisplayorder,
        GroupId: this.editData.groupid,
        GroupOrder: this.editData.grouporder,
        DisplayGroupName: this.editData.displaygroupname,
        DisplayGroupOrder: this.editData.displaygrouporder,
        UpdatedBy: Number(localStorage.getItem('user_id'))
      }];

      this.updateAttributeAPI(inputJson, 'attributes');

    } else {
      this.isEditAttribute = false;
      $('#editAttr').modal('hide');
      this.issubmitted = false;
    }


  }

  removeGroups(maingroup, i) {
    maingroup.splice(i, 1);
  }
  // removeSG(si, mi) {
  //   // this.AttributeGroups[mi].subgroups.splice(si, 1);
  // }

  closeModal() {

    this.inputFrom = '';
    this.formValues = {
      attributeName: '',
      displayName: '',
      isSameAsDisplayName: true,
      opdisplayName: '',
      selAttrTypes: '',
      inputValues: '',
      min: '',
      max: '',
      dateFrom: '',
      dateTo: '',
      dateFormat: '',
      isBulkUpload: this.formValues.isBulkUpload
    };
    this.isCheckGrpName = false;
    this.isCheckSubGrpName = false;
    if (this.isEditAttribute) {
      this.editAttributeArray[this.editformIndex] = this.editData;
      this.isEditAttribute = false;
    }


  }
  // drag and drop

  filterSearch(fval, dragdata, cloner) {
    dragdata = this.dgdp.filterSearch(fval, cloner, ['itemName']);
    // this.DragableList = dragdata;
  }


  saveAttributeGroups() {


    let projectId = localStorage.getItem('project_id');
    if (projectId == null || projectId == '') {
      this.Errormessage("Please select one project for Project Definition");
      return false;
    }


    this.resultData = [];
    this.groups.forEach((elem, index) => {
      let groupsData = {};
      let groupOrder = index + 1;

      let inputJson = [];
      if (this.attrList[0].maingroup.length > 0) {
        this.attrList[0].maingroup.forEach((attrelem, attrindex) => {
          let re = /\'/gi;
          let attributeValue = this.getAttributeValue(attrelem, re);
          let attrObj = {
            projectid: Number(projectId),
            moduleid: 3,
            attributeType: attrelem.selAttrTypes['attributeTypeName'],
            attributeName: attrelem.attributeName,
            attributeValue: attributeValue,
            displayName: attrelem.displayName,
            outputName: attrelem.opdisplayName,
            outputFlag: (attrelem.flag == 1) ? true : false,
          }

          inputJson.push(attrObj);

        });

      } else {
        let attrObj = {
          projectid: Number(projectId),
          moduleid: 3,
          attributeType: "",
          attributeName: "",
          attributeValue: [{}],
          displayName: "",
          outputName: "",
          outputFlag: false,
        };

        inputJson.push(attrObj);
      }



      console.log(this.dragableGroups);
      console.log(this.groups);
      this.getAttrArray(elem.label, elem, groupsData, index, 'mainGrp', groupOrder, inputJson);
      if (this.dragableGroups[index].child.length > 1) {
        this.dragableGroups[index].child.forEach((subElem, subIndex) => {
          if (subIndex > 0) {
            groupsData = {};
            this.getAttrArray(elem.label, subElem, groupsData, subIndex, 'subGrp', groupOrder, inputJson);
          }
        });
      }
    });

    $('.spinner').show();

    this.inpS.postRequest('InsertProjectConfigurationInput', this.resultData).subscribe((response) => {

      if (response.status == 1) {
        this.attrList[0].maingroup = [];
        this.successmessage("Project attributes inserted successfully");
        this.dragableGroups = [];
        this.groups = [];
        this.resultData = [];
        this.issubmitted = false;
        $('.spinner').hide();
        this.inpS.getAttribute().subscribe(resp => {
          this.attributeTypes = resp.data;
          this.GetProjectConfigurationList(this.attributeTypes);
        }, error => {

        });

      }
      else {
        $('.spinner').hide();
        if (response.status == 2) {
          this.Errormessage(response.message);

        } else {
          this.Errormessage(response.message);
        }
      }
    });
  }



  getAttributeValue(attr, re) {
    let attributeValue = [{}];
    if (attr.selAttrTypes['attributeTypeName'] == 'Label') {
      attributeValue = [{ value: "" }]
    } else if (attr.selAttrTypes['attributeTypeName'] == 'Textbox') {
      attributeValue = [{ value: "" }]
    } else if (attr.selAttrTypes['attributeTypeName'] == 'TextArea') {
      attributeValue = [{ value: "" }]
    } else if (attr.selAttrTypes['attributeTypeName'] == 'checkbox') {
      attributeValue = [{ value: attr.inputValues.toString().replace(re, "") }]
    } else if (attr.selAttrTypes['attributeTypeName'] == 'Radio button') {
      attributeValue = [{ value: attr.inputValues.toString().replace(re, "") }]
    } else if (attr.selAttrTypes['attributeTypeName'] == 'Length') {
      attributeValue = [{ min: attr.min, max: attr.max }]
    } else if (attr.selAttrTypes['attributeTypeName'] == 'Date Range') {
      attributeValue = [{ format: attr.dateFormat['format'], range: attr.dateFrom + ' - ' + attr.dateTo }]
    } else if (attr.selAttrTypes['attributeTypeName'] == 'Numeric Range') {
      attributeValue = [{ min: attr.min, max: attr.max }]
    } else if (attr.selAttrTypes['attributeTypeName'] == 'Dropdown') {
      attributeValue = [{ value: attr.inputValues.toString().replace(re, "") }]
    } else {
      attributeValue = [{ value: "" }]
    }

    return attributeValue;
  }

  getAttrArray(groupName, elem, groupsData, index, grpType, groupOrder, inputJson) {


    if (elem.attributeList.length > 0) {
      elem.attributeList.forEach((attr, attrIndex) => {
        groupsData = {};

        groupsData['inputjson'] = inputJson;
        groupsData['groupname'] = groupName;
        if (grpType == 'subGrp') {
          groupsData['subgroupname'] = elem.groupName;
          groupsData['displaygrouporder'] = Number(elem.displayOrder);
          groupsData['attributedisplayorder'] = attrIndex + 1;
          groupsData['grouporder'] = Number(groupOrder);
          groupsData['displaygroupsubgroupname'] = elem.groupName;
        } else {
          groupsData['subgroupname'] = "";
          groupsData['attributedisplayorder'] = attrIndex + 1;
          groupsData['displaygrouporder'] = Number(elem.displayOrder);
          groupsData['grouporder'] = Number(groupOrder);
          groupsData['displaygroupsubgroupname'] = elem.label;
        }
        groupsData['parentsubgroupname'] = elem.parentSubGroupName;
        groupsData['attributename'] = attr.attributeName;
        let re = /\'/gi;
        let attributeValue = this.getAttributeValue(attr, re);
        groupsData['attributetype'] = attr.selAttrTypes['attributeTypeName'];
        groupsData['attributevalue'] = attributeValue;
        groupsData['displayname'] = attr.displayName;
        groupsData['outputname'] = attr.opdisplayName;
        groupsData['outputflag'] = (attr.flag == 1) ? true : false;
        groupsData['levelid'] = elem.levelId;


        this.resultData.push(groupsData);
      });
    } else {
      console.log(elem);

      groupsData['inputjson'] = inputJson;
      groupsData['groupname'] = groupName;

      if (grpType == 'subGrp') {
        groupsData['displaygroupsubgroupname'] = elem.groupName;
        groupsData['subgroupname'] = elem.groupName;
        groupsData['grouporder'] = Number(groupOrder)
        groupsData['displaygrouporder'] = Number(elem.displayOrder);
        groupsData['attributedisplayorder'] = Number("");

      } else {
        groupsData['displaygroupsubgroupname'] = elem.label;
        groupsData['subgroupname'] = "";
        groupsData['grouporder'] = Number(groupOrder);
        groupsData['displaygrouporder'] = Number(elem.displayOrder);
        groupsData['attributedisplayorder'] = Number("");
      }
      groupsData['parentsubgroupname'] = elem.parentSubGroupName;
      groupsData['attributename'] = "";
      groupsData['attributetype'] = "";
      groupsData['displayname'] = "";
      groupsData['outputname'] = "";
      groupsData['outputflag'] = false;
      groupsData['levelid'] = elem.levelId;
      this.resultData.push(groupsData);
    }
  }



  successmessage(message) {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: message });
  }

  Errormessage(errorsmessage) {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
  }


  defaultAttrAdd() {

    this.selDefAttribute.forEach((elem, i) => {
      const k = this.defaultAttribute.indexOf(elem);

      this.attrList[0].maingroup.push(elem);
      if (k === -1) {

      } else {
        this.defaultAttribute.splice(k, 1);
      }

      if (this.selDefAttribute.length === (i + 1)) {
        this.selDefAttribute = [];
      }

    });

  }


  isCheckDateFormat(dateFormat, dateType, fromToValue) {
    var formats = ['MM/DD/YYYY', 'DD/MM/YYYY', 'DD/MMM/YYYY'];
    if (dateType) {
      return formats.includes(dateFormat);
    } else {
      var d = moment(fromToValue, formats);
      return d.isValid();
    }

  }



  addBulkAttributeValues(data) {

    let inputString = '';
    let attrListAry = [];
    this.errorExclReports = [];
    this.errorExistsAttr = [];
    data.forEach((elem, i) => {


      this.uploadSingleAttributeValidate(elem, this.formValues.selAttrTypes.attributeTypeName);
      if (attrListAry.includes(elem['value'])) {
        this.errorExistsAttr.push(elem['value']);
      } else {
        attrListAry.push(elem['value']);
        inputString += (i === 0 ? '' : ', ') + elem.value;
      }

    });


    if (this.errorExistsAttr.length > 0) {
      this.Errormessage(this.errorExistsAttr.join(' ,') + ' already exists');
      $('#singlebulkvalue').val("");
      this.fileName = "Choose File";
      this.pleaseWait = false;
      this.formValues.inputValues = '';
      return false;
    } else {
      if (this.errorExclReports.length > 0) {
        this.Errormessage(this.errorExclReports.join("\r\n"));
        this.pleaseWait = false;
        this.fileName = "Choose File";
        $('#singlebulkvalue').val("");
        this.formValues.inputValues = '';
        return false;
      } else {
        this.formValues.inputValues = inputString;
        this.pleaseWait = false;
        $('bulkerr').hide();
      }
    }


  }


  commaSeprated(inputValues, type, rowNo) {

    let inputAry = inputValues.split(',');
    let inAry = [];

    inputAry.forEach(elem => {
      elem = elem.trim();
      if (elem == '' || elem == null) {
        this.errorExclReports.push('Row ' + rowNo + ' ' + type + ' -  values is empty');
      } else if (this.isCheckNumber(elem)) {
        this.errorExclReports.push('Row ' + rowNo + ' ' + type + ' -  values should not be numeric only')
      } else if (this.onlySpecialchars(elem)) {
        this.errorExclReports.push('Row ' + rowNo + ' ' + type + ' -  Special characters only not allowed in attribute name')
      } else if (this.onlySpecialcharNumber(elem)) {
        this.errorExclReports.push('Row ' + rowNo + ' ' + type + ' -  must contain atleast one alphabets')
      } else if (elem.length > 50) {
        this.errorExclReports.push('Row ' + rowNo + ' ' + type + ' -  must be lessthan 50 characters')
      }

      if (inAry.includes(elem.trim())) {
        this.errorExistsComma.push('Row ' + rowNo + '  ' + type + ' values - ' + elem + ' already exists');
      } else {
        inAry.push(elem.trim());
      }
    });
  }


  uploadBulkAttributeValidate(row) {

    if (row['TYPE'] == '' || row['TYPE'] == null) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Attribute type is empty');
    } else if (this.isCheckNumber(row['TYPE'])) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Attribute type should not be numeric only')
    } else if (this.onlySpecialchars(row['TYPE'])) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Special characters only not allowed in attribute type')
    } else if (row['TYPE'].length > 50) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Attribute type must be lessthan 50 characters')
    } else if (row['TYPE'].toUpperCase() == 'DROPDOWN') {
      if (row['COMMA SEPERATED'] == '' || row['COMMA SEPERATED'] == null) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Drop down value is empty')
      } else {
        this.commaSeprated(row['COMMA SEPERATED'], row['TYPE'], row['SI.NO']);
      }
    } else if (row['TYPE'].toUpperCase() == 'RADIO BUTTON') {
      if (row['COMMA SEPERATED'] == '' || row['COMMA SEPERATED'] == null) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Radio button value is empty')
      } else {
        this.commaSeprated(row['COMMA SEPERATED'], row['TYPE'], row['SI.NO']);
      }
    } else if (row['TYPE'].toUpperCase() == 'CHECKBOX') {
      if (row['COMMA SEPERATED'] == '' || row['COMMA SEPERATED'] == null) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Checkbox value is empty');
      } else {
        this.commaSeprated(row['COMMA SEPERATED'], row['TYPE'], row['SI.NO']);
      }
    } else if (row['TYPE'].toUpperCase() == 'NUMERIC RANGE' || row['TYPE'].toUpperCase() == 'LENGTH') {
      if (row['MAX'] == '' || row['MAX'] == null) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Max value is empty');
      } else if (!this.isCheckNumber(row['MAX'])) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Max value should be numeric only');
      }

      if (row['MIN'] == '' || row['MIN'] == null) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Min value is empty');
      } else if (!this.isCheckNumber(row['MIN'])) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Min value should be numeric only');
      }

      if (Number(row['MIN']) >= Number(row['MAX'])) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Please enter valid maximum value')
      }

    } else if (row['TYPE'].toUpperCase() == 'DATE RANGE') {
      if (row['DATE FORMAT'] == '' || row['DATE FORMAT'] == null) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Date range is empty');
      } else if (!this.isCheckDateFormat(row['DATE FORMAT'], 1, '')) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Invalid date format');
      }

      if (row['DATE FROM'] == '' || row['DATE FROM'] == null) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Date from is empty');
      } else if (!this.isCheckDateFormat(row['DATE FORMAT'], 0, row['DATE FROM'])) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Date from not mismatch');
      }

      if (row['DATE TO'] == '' || row['DATE TO'] == null) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Date to is empty');
      } else if (!this.isCheckDateFormat(row['DATE FORMAT'], 0, row['DATE TO'])) {
        this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Date to not mismatch');
      }

    }


    if (row['ATTRIBUTE NAME'] == '' || row['ATTRIBUTE NAME'] == null) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Attribute name is empty');
    } else if (this.isCheckNumber(row['ATTRIBUTE NAME'])) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Attribute name should not be numeric only')
    } else if (this.onlySpecialchars(row['ATTRIBUTE NAME'])) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Special characters only not allowed in attribute name')
    } else if (this.onlySpecialcharNumber(row['ATTRIBUTE NAME'])) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Attribute name must contain atleast one alphabets')
    } else if (row['ATTRIBUTE NAME'].length > 50) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Attribute name must be lessthan 50 characters')
    } else if (row['ATTRIBUTE NAME'].match(' ')) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Attribute name in between space not allowed')
    }

    if (row['DISPLAY NAME'] == '' || row['DISPLAY NAME'] == null) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Display name is empty');
    } else if (this.isCheckNumber(row['DISPLAY NAME'])) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Display name should not be numeric only')
    } else if (this.onlySpecialchars(row['DISPLAY NAME'])) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Special characters only not allowed in display name')
    } else if (this.onlySpecialcharNumber(row['DISPLAY NAME'])) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Display name must contain atleast one alphabets')
    } else if (row['DISPLAY NAME'].length > 50) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Display name must be lessthan 50 characters')
    }


    if (row['OUTPUT NAME'] == '' || row['OUTPUT NAME'] == null) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Output name is empty');
    } else if (this.isCheckNumber(row['OUTPUT NAME'])) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Output name should not be numeric only')
    } else if (this.onlySpecialchars(row['OUTPUT NAME'])) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Special characters only not allowed in output name')
    } else if (this.onlySpecialcharNumber(row['OUTPUT NAME'])) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Output name must contain atleast one alphabets')
    } else if (row['OUTPUT NAME'].length > 50) {
      this.errorExclReports.push('Row ' + row['SI.NO'] + ' -  Output name must be lessthan 50 characters')
    }
  }


  uploadSingleAttributeValidate(row, attrType) {

    if (row['value'] == '' || row['value'] == null) {
      this.errorExclReports.push('Row ' + row['id'] + ' -  ' + attrType + ' values is empty');
    } else if (this.isCheckNumber(row['value'])) {
      this.errorExclReports.push('Row ' + row['id'] + ' -  ' + attrType + ' values should not be numeric only')
    } else if (this.onlySpecialchars(row['value'])) {
      this.errorExclReports.push('Row ' + row['id'] + ' -  Special characters only not allowed in ' + attrType + '')
    } else if (this.onlySpecialcharNumber(row['value'])) {
      this.errorExclReports.push('Row ' + row['id'] + ' -  ' + attrType + ' must contain atleast one alphabets')
    } else if (row['value'].length > 50) {
      this.errorExclReports.push('Row ' + row['id'] + ' - ' + attrType + 'values must be lessthan 50 characters')
    }

  }

  addBulkAttributeList(data) {
    this.errorExclReports = [];
    this.errorExistsAttr = [];
    this.errorExistsComma = [];

    let attrListAry = [];
    data.forEach(elem => {

      this.uploadBulkAttributeValidate(elem);
      let inputvalues = elem['COMMA SEPERATED'] ? elem['COMMA SEPERATED'] : '';
      inputvalues = inputvalues.replace(/^[,\s]+|[,\s]+$/g, '');
      inputvalues = inputvalues.replace(/\s*,\s*/g, ',');
      let curAttributeType;

      const a = this.attributeTypes.findIndex(x => x.attributeTypeName.toUpperCase() === elem.TYPE);
      if (a === -1) { } else {
        curAttributeType = this.attributeTypes[a];
      }

      console.log(this.errorExclReports);
      console.log(this.errorExistsAttr);
      console.log(this.errorExistsComma);
      console.log(elem['ATTRIBUTE NAME']);

      //let filteredElements = this.attrList[0].maingroup.filter(f => f.attributeName.toString().toLowerCase().trim() == elem['ATTRIBUTE NAME'].toString().toLowerCase().trim());

      var attrListDatas = this.attrList[0].maingroup.filter(function (attrList) {
        return attrList.attributeName == elem['ATTRIBUTE NAME'];
      });

      if (attrListDatas.length > 0) {
        this.errorExistsAttr.push(elem['ATTRIBUTE NAME']);
      } else {
        const obj = {
          id: Math.floor(Math.random() * 1000),
          attributeName: elem['ATTRIBUTE NAME'],
          displayName: elem['DISPLAY NAME'],
          opdisplayName: elem['OUTPUT NAME'],
          isSameAsDisplayName: elem['DISPLAY NAME'],
          itemName: elem['DISPLAY NAME'],
          selAttrTypes: curAttributeType,
          inputValues: inputvalues ? inputvalues.split(',') : '',
          min: elem.MIN ? elem.MIN : '',
          max: elem.MAX ? elem.MAX : '',
          dateFrom: elem['DATE FROM'] ? elem['DATE FROM'] : '',
          dateTo: elem['DATE TO'] ? elem['DATE TO'] : '',
          dateFormat: elem['DATE FORMAT'] ? elem['DATE FORMAT'] : '',
          selected: false,
          flag: 1
        };

        attrListAry.push(obj);
      }
    });

    let existsArr = '';
    let errorComma = '';
    if (this.errorExistsAttr.length > 0 || this.errorExclReports.length > 0 || this.errorExistsComma.length > 0) {
      if (this.errorExistsAttr.length > 0) {
        let existsArr = this.errorExistsAttr.join(' ,') + ' already exists';
        this.errorExclReports.push(existsArr);
      }

      let errorComma = this.errorExistsComma.join('\r\n');
      this.errorExclReports.push(errorComma);
      this.Errormessage(this.errorExclReports);
      $('#customFile').val("");
      this.fileName = "Choose File";
    } else {
      this.attrList[0].maingroup = this.attrList[0].maingroup.concat(attrListAry);
    }

    this.pleaseWait = false;
  }


  uploadBulk(e, identity) {
    this.pleaseWait = true;
    try {
      this.fileName = e.target.files[0].name;
      let output;
      import('xlsx').then(xlsx => {
        let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    // const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = xlsx.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = xlsx.utils.sheet_to_json(sheet);
        return initial;
      }, {});

      console.log(jsonData, 'jsondata')

      let output = jsonData[Object.keys(jsonData)[0]];
      let inputString = '';
      output.forEach((elem, i) => {
        inputString += (i === 0 ? '' : ', ') + elem.value;
      });

      if (inputString === '') {
        this.Errormessage("File is Empty");
        // this.fileName = e.target.files[0].name;
        this.pleaseWait = false;
        return false;
      }
      if (identity) {
        this.addBulkAttributeList(output);
      } else {
        this.addBulkAttributeValues(output);
      }

    };

    reader.readAsBinaryString(e.target.files[0]);

  });

} catch (e) {
  this.pleaseWait = false;
  console.log('Try again. Something went wrong check the uploaded sheet.')
}

  }


//   uploadBulk(e, identity) {
//     this.pleaseWait = true;
//     try {

//       this.fileName = e.target.files[0].name;
//       let output;
//       import('xlsx').then(xlsx => {
//         let workBook = null;
//         let jsonData = null;
//       const reader = new FileReader();
//     // const file = ev.target.files[0];
//     reader.onload = (event) => {
//       const data = reader.result;
//       workBook = xlsx.read(data, { type: 'binary' });
//       jsonData = workBook.SheetNames.reduce((initial, name) => {



//         const sheet = workBook.Sheets[name];
//         initial[name] = xlsx.utils.sheet_to_json(sheet);
//         return initial;
//       }, {});
//       output = jsonData[Object.keys(jsonData)[0]];
//       if (identity) {
//         this.addBulkAttributeList(output);
//       } else {
//         this.addBulkAttributeValues(output);
//       }

//     };
//     reader.readAsBinaryString(e.target.files[0]);
//   });

// } catch (e) {
//   this.pleaseWait = false;
//   console.log('Try again. Something went wrong check the uploaded sheet.')
// }
//   }







fs() {
  const fullscreen: any = document.getElementById('fullscreen');
  if (this.isFullScreen) {

    this.isFullScreen = false;
    if (fullscreen.requestFullscreen) {
      fullscreen.requestFullscreen();
    } else if (fullscreen.mozRequestFullScreen) { /* Firefox */
      fullscreen.mozRequestFullScreen();
    } else if (fullscreen.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
      fullscreen.webkitRequestFullscreen();
    } else if (fullscreen.msRequestFullscreen) { /* IE/Edge */
      fullscreen.msRequestFullscreen();
    }
  } else {

    this.isFullScreen = true;

    if (this.document.exitFullscreen) {
      console.log("NoisFullscreen");
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      this.document.msExitFullscreen();
    } else {

    }
  }
}

}
