import { TestBed } from '@angular/core/testing';

import { MultiDragDropService } from './multi-drag-drop.service';

describe('MultiDragDropService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MultiDragDropService = TestBed.get(MultiDragDropService);
    expect(service).toBeTruthy();
  });
});
