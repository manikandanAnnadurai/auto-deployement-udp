import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectUserRoleMappingComponent } from './project-user-role-mapping.component';

describe('ProjectUserRoleMappingComponent', () => {
  let component: ProjectUserRoleMappingComponent;
  let fixture: ComponentFixture<ProjectUserRoleMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectUserRoleMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectUserRoleMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
