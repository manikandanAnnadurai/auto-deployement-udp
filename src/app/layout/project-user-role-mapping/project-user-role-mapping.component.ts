import { Component, OnInit } from '@angular/core';
import { isBuffer } from 'util';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { CommonService } from '../../services/common.service';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ProjectDefinitionService } from '../../services/project-definition.service';
import { RoleDefinitionService } from '../../services/role-definition.service';
import { UserManagementService } from '../../services/user-management.service';
import { ProjectUserRoleMappingService } from '../../services/project-user-role-mapping.service';
import { SharedvalueService } from 'src/app/services/sharedvalue.service';
declare var $: any;
@Component({
  selector: 'app-project-user-role-mapping',
  templateUrl: './project-user-role-mapping.component.html',
  styleUrls: ['./project-user-role-mapping.component.css'],
  providers: [CommonService, MessageService, ToastModule]
})
export class ProjectUserRoleMappingComponent implements OnInit {

  public projects: any;
  public projectList = [];
  public roleDefinitions: any;
  public roles = [];

  public selectedProject: number;
  public selectedModelProject: number;
  public modelProjectValidate: boolean
  public projectValidate: boolean;
  public projectId: number;
  public projectName: string;
  public mprojectId: number;
  public mprojectName: string;
  public selectedFrontTableUserWRole = [];
  public user_role_id: number;
  public disableUserList = [];
  public userManagementList = [];
  public projectUserRoleMapId: number;
  public deleteProjectUserName: string;
  public userId: number;
  public currentRoleUserList: number;




  public userRoles: any;
  public users: any;
  public pmList = [];
  public tlList = [];
  public qcList = [];
  public puList = [];
  public pmListLocal = [];
  public tlListLocal = [];
  public qcListLocal = [];
  public puListLocal = [];
  public activeTab = 'pm';
  public selectedTableUser = [];
  public savebuttonDisable = true;
  public cols: any;
  public exportPdfcolumn: any;
  public tabId: any;
  public mtabId: any;

  public cloneOfUsers = [];
  dummyRows: any;
  selectedUserwithRole = [];
  public previousTab: any;
  public tabIdRole: string;
  public mtabIdRole: string;
  public tableRoleUserList = [];
  public selectedTableUserWRole = [];
  showSideBar = true;
  modalOpen = false;
  constructor(
    private messageService: MessageService,
    private UserManagement: UserManagementService,
    private ProjectDefinition: ProjectDefinitionService,
    private RoleDefinition: RoleDefinitionService,
    private ProjectUserRoleMapping: ProjectUserRoleMappingService,
    public shrService: SharedvalueService
  ) {


  }

  ngOnInit() {
  $('.spinner').show();


    // test hroizontal tab

    // test horizontal tab
    $('.modal').appendTo('#fullscreen');
    this.shrService.getSideBarDetail().subscribe(resp => { this.showSideBar = resp });

    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'mailId', header: 'Email Id' },
      { field: 'createdBy', header: 'Created By' },
      { field: 'insertedDateTime', header: 'Created On' },
    ];
    this.user_role_id = parseInt(localStorage.getItem('user_role_id'));
    this.GetProjectList();
  }


  GetProjectList() {
    $('.spinner').show();
    this.ProjectDefinition.getRequest('GetProjectList').subscribe((ProjectDetails) => {
      this.projects = ProjectDetails;
      let projectListArray = {};

      if (this.projects.length > 0) {
        this.projects.forEach(resource => {
          this.projectList.push({ label: resource['projectName'], value: resource['projectId'] });
          projectListArray[resource['projectId']] = { label: resource['projectName'], value: resource['projectId'] };
        });

        this.projects['projectTypeList'] = projectListArray;
        this.projectId = this.projectList[0].value;
        this.mprojectId = parseInt(this.projectList[0].value);
        this.mprojectName = this.projectList[0].label;
        this.projectName = this.projectList[0].label;

        console.log(this.mprojectId);
        // this.selectedProject = this.projectList[0].value;
        // this.selectedModelProject = this.projectList[0].value;
        $('.spinner').show();
      }
      this.GetRoleDefinitionList(this.selectedProject);
    });


  }

  GetRoleDefinitionList(selectedProject) {
    $('.spinner').show();
    this.RoleDefinition.getRequest('GetRoleDefinitionList').subscribe((RoleDefinitionDetails) => {
      this.roleDefinitions = RoleDefinitionDetails;
      this.roleDefinitions.forEach(resource => {
        this.roles.push({ id: resource['roleId'], roleName: resource['role'] });
        this.tableRoleUserList[resource['roleId']] = { roleId: resource['roleId'], data: [] }
      });

      this.tabId = this.roles[0].id;
      this.tabIdRole = this.roles[0].roleName;
      this.mtabId = this.roles[0].id;
      this.mtabIdRole = this.roles[0].roleName;

      this.GetProjectUserRoleList(this.tabId, selectedProject);
      //this.GetModelProjectUserRoleList(this.mtabId, this.mprojectId);

    });
    this.GetUserManagementList();
  }


  GetUserManagementList() {
    this.UserManagement.getRequest('GetUserManagementList').subscribe((UserManagementDetails) => {
      console.log(UserManagementDetails);
      this.cloneOfUsers = [];
      UserManagementDetails.forEach(resource => {
        this.cloneOfUsers.push({
          name: resource.firstName + ' ' + resource.lastName,
          id: resource.userId,
          mailId: resource.emailId,
          disabled: false,
        });
      });

      this.userManagementList = this.cloneOfUsers;

    });


  }

  ChangeModelProject(projectId) {

    this.mprojectId = projectId;
    this.mprojectName = this.projects['projectTypeList'][projectId].label;
    let role_id = this.mtabId;
    this.selectedTableUser = [];
    this.selectedUserwithRole = [];
    this.modelProjectValidate = false;
    this.selectedModelProject = projectId;
    this.GetModelProjectUserRoleList(role_id, this.mprojectId);
  }




  ChangeProject(projectId) {

    this.projectId = projectId;
    this.projectName = this.projects['projectTypeList'][projectId].label;
    let role_id = this.tabId;
    this.projectValidate = false;
    this.GetProjectUserRoleList(role_id, this.projectId);
  }


  GetProjectUserRoleList(role_id: number, project_id: number) {
$('.spinner').show();
    var data = {
      'RoleId': role_id,
      'ProjectId': project_id
    };

    console.log(data);

    var keys = Object.keys(data);
    if (keys.length > 0) {
      this.ProjectUserRoleMapping.postRequest('GetProjectUserRoleMapList', data).subscribe((response) => {
        this.currentRoleUserList = response.length;
        let obj;
        if (response.length == 0) {
          for (var tableRoleKey in this.tableRoleUserList) {
            this.tableRoleUserList[tableRoleKey]['data'] = [];
          }
        } else {
          var data = [];
          for (var responseKey in response) {
            this.tableRoleUserList[response[responseKey].roleId].data = [];

            data.push({
              projectUserRoleMapId: response[responseKey].id,
              name: response[responseKey].userName,
              id: response[responseKey].userId,
              mailId: response[responseKey].emailId,
              createdBy: response[responseKey].createdBy,
              insertedDateTime: response[responseKey].createdOn
            });
            this.tableRoleUserList[response[responseKey].roleId].roleId = response[responseKey].roleId;
            this.tableRoleUserList[response[responseKey].roleId].data = data;
          }

        }
        $('.spinner').hide();
      },err =>{
        $('.spinner').hide();
      });
    } else {
      $('.spinner').hide();
      this.Errormessage('Invalid project id');
    }

  }




  GetModelProjectUserRoleList(role_id: number, project_id: number) {
$('.spinner').show();
    var data = {
      'RoleId': role_id,
      'ProjectId': project_id
    };
    console.log(data);

    var keys = Object.keys(data);
    if (keys.length > 0) {
      this.ProjectUserRoleMapping.postRequest('GetProjectUserRoleMapList', data).subscribe((response) => {
        this.disableUserList = [];
        let userIds = [];
        this.currentRoleUserList = response.length;

        if (response.length > 0 && response != null) {

          for (var resKey in response) {
            userIds.push(response[resKey]['userId']);
          }

          for (var userKey in this.cloneOfUsers) {
            if (userIds.includes(this.cloneOfUsers[userKey].id)) {
              this.disableUserList[userKey] = { name: this.cloneOfUsers[userKey].name, id: this.cloneOfUsers[userKey].id, mailId: this.cloneOfUsers[userKey].mailId, disabled: true };
            } else {
              this.disableUserList[userKey] = { name: this.cloneOfUsers[userKey].name, id: this.cloneOfUsers[userKey].id, mailId: this.cloneOfUsers[userKey].mailId, disabled: false };
            }
          }

          let cloneUserList = [...this.disableUserList];
          this.cloneOfUsers = [];
          this.cloneOfUsers = cloneUserList;

        } else {

          let cloneUserList = [...this.userManagementList];
          this.cloneOfUsers = [];
          this.cloneOfUsers = cloneUserList;
        }

        console.log(this.cloneOfUsers);

        this.cloneOfUsers.forEach((elem, i) => {
          if (elem.disabled) {
            this.selectedTableUser.push(elem);
          }
        })
        $('.spinner').hide();

      }, err =>{
        $('.spinner').hide();
      });
    } else {
      $('.spinner').hide();
      this.Errormessage('Invalid project id');
    }

  }



  successmessage(message) {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: message });
  }

  Errormessage(errorsmessage) {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
  }

  clear() {
    this.messageService.clear();
  }

  tabSelect(ev) {
    console.log(ev.index);
    console.log(this.roles[ev.index]);
    const role = this.roles[ev.index];
    if (this.selectedProject !== undefined && this.selectedProject !== null) {
      this.tabId = role.id;
      this.tabIdRole = role.roleName;
      this.selectedTableUserWRole = [];
      this.selectedFrontTableUserWRole = [];
      this.GetProjectUserRoleList(this.tabId, this.projectId);
    } else {
      this.projectValidate = true;
    }
    console.log(this.tabId);
    console.log(this.selectedTableUserWRole);
  }

  // tabSelect(role, index) {
  //   console.log(role, index);
  //   if (this.selectedProject != undefined && this.selectedProject != null) {
  //     this.tabId = role.id;
  //     this.tabIdRole = role.roleName;
  //     this.selectedTableUserWRole = [];
  //     this.selectedFrontTableUserWRole = [];
  //     this.GetProjectUserRoleList(this.tabId, this.projectId);
  //   } else {
  //     this.projectValidate = true;
  //   }
  //   console.log(this.tabId);
  //   console.log(this.selectedTableUserWRole);
  // }

  setactiveTabInModal(ev) {
    const role = this.roles[ev.index];
    if (this.selectedModelProject !== undefined && this.selectedModelProject !== null) {
      this.previousTab = this.mtabId;
      this.mtabId = role.id;
      this.mtabIdRole = role.roleName;
      this.selectedTableUser = [];
      const idx = this.selectedUserwithRole.findIndex(x => x.roleId === this.mtabId);
      if (idx === -1) {
        this.selectedTableUser = [];
        // this.selectedUserwithRole = [];
      } else {
        this.selectedUserwithRole[idx].userList.forEach((elem, i) => {
          this.selectedTableUser[i] = { name: elem.userName, id: elem.id, mailId: elem.mailId };
        });
      }

      this.GetModelProjectUserRoleList(this.mtabId, this.mprojectId);
    } else {
      this.modelProjectValidate = true;
    }
  }

  onRowSelect($event) {

    if (this.selectedModelProject != undefined && this.selectedModelProject != null) {
      let obj;
      let index;
      if (this.selectedUserwithRole.length) {
        index = this.selectedUserwithRole.findIndex(x => x.roleId === this.mtabId);
      } else {
        index = -1;
      }

      if (index === -1) {
        obj = {
          roleId: this.mtabId,
          roleName: this.mtabIdRole,
          projectId: this.mprojectId,
          projectName: this.mprojectName,
          userList: [{ userName: $event.data.name, id: $event.data.id, mailId: $event.data.mailId }]
        };
        this.selectedUserwithRole.push(obj);
      } else {
        obj = { userName: $event.data.name, id: $event.data.id, mailId: $event.data.mailId };
        const sno = this.selectedUserwithRole[index].userList.findIndex(x => x.id === $event.data.id);
        if (sno === -1) { this.selectedUserwithRole[index].userList.push(obj) }
      }
    } else {
      this.modelProjectValidate = true;
    }


  }


  onFrontRowSelect($event) {

    let obj;
    let index;
    console.log($event);
    if (this.selectedFrontTableUserWRole.length) {
      index = this.selectedFrontTableUserWRole.findIndex(x => x.roleId === this.tabId);
    } else {
      index = -1;
    }

    if (index === -1) {
      obj = {
        roleId: this.tabId,
        roleName: this.tabIdRole,
        projectId: this.projectId,
        projectName: this.projectName,
        userList: [{ projectUserRoleMapId: $event.data.projectUserRoleMapId, userName: $event.data.name, id: $event.data.id, mailId: $event.data.mailId }]
      };
      this.selectedFrontTableUserWRole.push(obj);
    } else {
      obj = { projectUserRoleMapId: $event.data.projectUserRoleMapId, userName: $event.data.name, id: $event.data.id, mailId: $event.data.mailId };
      const sno = this.selectedFrontTableUserWRole[index].userList.findIndex(x => x.id === $event.data.id);
      if (sno === -1) { this.selectedFrontTableUserWRole[index].userList.push(obj) }
    }

  }

  selectRow(checkValue) {
    console.log(checkValue);
  }

  onRowUnselect($event) {
    if (this.selectedModelProject != undefined && this.selectedModelProject != null) {
      const index = this.selectedUserwithRole.findIndex(x => x.roleId === this.mtabId);
      const i = this.selectedUserwithRole[index].userList.findIndex(x => x.id === $event.data.id);
      this.selectedUserwithRole[index].userList.splice(i, 1);
    } else {
      this.modelProjectValidate = true;
    }
  }

  onFrontRowUnselect($event) {
    const index = this.selectedFrontTableUserWRole.findIndex(x => x.roleId === this.tabId);
    const i = this.selectedFrontTableUserWRole[index].userList.findIndex(x => x.id === $event.data.id);
    this.selectedFrontTableUserWRole[index].userList.splice(i, 1);
  }

  headerCheckboxToggle($event) {

    if (this.selectedModelProject != undefined && this.selectedModelProject != null) {
      let obj;
      console.log($event);
      if ($event.checked) {
        console.log(this.cloneOfUsers);
        console.log(this.selectedUserwithRole);
        this.cloneOfUsers.forEach((elem, i) => {
          if (elem.disabled == false) {
            const k = this.selectedUserwithRole.findIndex(x => x.roleId === this.mtabId);
            obj = {
              roleId: this.mtabId,
              roleName: this.mtabIdRole,
              projectId: this.mprojectId,
              projectName: this.mprojectName,
              userList: [{ userName: elem.name, id: elem.id, mailId: elem.mailId }]
            };

            if (k === -1) {
              this.selectedUserwithRole.push(obj);
            } else {
              const a = this.selectedUserwithRole[k].userList.findIndex(x => x.id === elem.id);
              if (a === -1) { this.selectedUserwithRole[k].userList.push({ userName: elem.name, id: elem.id, mailId: elem.mailId }); }
            }
          }
        });

      } else {
        const k = this.selectedUserwithRole.findIndex(x => x.roleId === this.mtabId);
        if (k === -1) { } else {
          this.selectedUserwithRole.splice(k, 1);
        }
      }
    } else {
      this.modelProjectValidate = true;
    }

  }


  frontHeaderCheckboxToggle($event) {

    let obj;
    if ($event.checked) {
      var frontTableUser = [];
      this.tableRoleUserList.forEach((elem, i) => {
        if (elem.roleId == this.tabId) {
          elem.data.forEach((dataelem, dataKey) => {
            frontTableUser.push({ projectUserRoleMapId: dataelem.projectUserRoleMapId, name: dataelem.name, id: dataelem.id, mailId: dataelem.mailId });
          });
        }
      });

      frontTableUser.forEach((elem, i) => {
        const k = this.selectedFrontTableUserWRole.findIndex(x => x.roleId === this.tabId);
        obj = {
          roleId: this.tabId,
          roleName: this.tabIdRole,
          projectId: this.projectId,
          projectName: this.projectName,
          userList: [{ projectUserRoleMapId: elem.projectUserRoleMapId, userName: elem.name, id: elem.id, mailId: elem.mailId }]
        };

        if (k === -1) {
          this.selectedFrontTableUserWRole.push(obj);
        } else {
          const a = this.selectedFrontTableUserWRole[k].userList.findIndex(x => x.id === elem.id);
          if (a === -1) { this.selectedFrontTableUserWRole[k].userList.push({ projectUserRoleMapId: elem.projectUserRoleMapId, userName: elem.name, id: elem.id, mailId: elem.mailId }); }
        }

      });

    } else {
      const k = this.selectedFrontTableUserWRole.findIndex(x => x.roleId === this.tabId);
      if (k === -1) { } else {
        this.selectedFrontTableUserWRole.splice(k, 1);
      }
    }

    console.log(this.selectedFrontTableUserWRole);
  }

  delUserFromGroup(item: any, user) {

    const i = this.selectedUserwithRole.findIndex(x => x.roleId === item.roleId);
    if (this.selectedUserwithRole[i].userList === []) { } else {
      const j = this.selectedUserwithRole[i].userList.findIndex(x => x.id === user.id);
      this.selectedUserwithRole[i].userList.splice(j, 1);
    }
    if (this.mtabId === item.roleId) {
      const k = this.selectedTableUser.findIndex(x => x.id === user.id);
      this.selectedTableUser.splice(k, 1);

      const clone = [...this.selectedTableUser];
      this.selectedTableUser = [];
      this.selectedTableUser = clone;
    }

  }


  submitUser() {
  $('.spinner').show();
    var data = [];
    if (this.selectedUserwithRole.length > 0) {
      for (var mainKey in this.selectedUserwithRole) {
        for (var userListKey in this.selectedUserwithRole[mainKey]['userList']) {
          data.push({
            ProjectId: this.selectedUserwithRole[mainKey]['projectId'],
            UserId: this.selectedUserwithRole[mainKey]['userList'][userListKey]['id'],
            RoleId: this.selectedUserwithRole[mainKey]['roleId'],
            Active: true,
            InsertedBy: this.user_role_id,
            RecordStatus: 1,
          });
        }
      }

      this.ProjectUserRoleMapping.postRequest('InsertProjectRoleUserMapping', data).subscribe((response) => {
        if (response.status == 1) {

          this.successmessage("Project user role created successfully");
          this.GetProjectUserRoleList(this.mtabId, this.selectedModelProject);
          this.selectedProject = this.selectedModelProject;
          this.tabId = this.mtabId;
          $('.close').trigger('click');
          $('.spinner').hide();
          this.Close();
        }
        else {
          if (response.status == 2) {
            $('.spinner').hide();
            this.Errormessage(response.message);
          } else {
            $('.spinner').hide();
            this.Errormessage(response.message);
          }
        }
      },err =>{
        this.Errormessage(err.message+'. Try again later');
        $('.spinner').hide();
      });
    } else if (this.currentRoleUserList == this.cloneOfUsers.length) {
      $('.spinner').hide();
      this.Errormessage('Users already exists');
    } else {
      console.log(this.currentRoleUserList);
      console.log(this.cloneOfUsers.length);
      $('.spinner').hide();
      this.Errormessage('Please select a new user');
    }

  }

  openModal(){

    setTimeout(()=>{
      console.log('openmodal')
      this.modalOpen = true;
    },500);
  }
  Close() {
    this.modalOpen = false;
    this.selectedUserwithRole = [];
    this.selectedTableUser = [];
    this.selectedModelProject = null;
    this.mtabId = this.roles[0].id;
    this.mtabIdRole = this.roles[0].roleName;
    this.cloneOfUsers = this.userManagementList;
  }


  deleteProjectUserRole(projectUserRole) {
    console.log(projectUserRole)
    this.projectUserRoleMapId = projectUserRole.projectUserRoleMapId;
    this.userId = projectUserRole.id;
    this.deleteProjectUserName = projectUserRole.name;
  }

  delUserFromProject() {
   $('.spinner').show();
    var data = {
      'ProjectUserRoleMapId': this.projectUserRoleMapId,
      'RoleId': this.projectId,
      'ProjectId': this.selectedProject,
      'UserId': this.userId,
    };

    const isEmpty = !Object.values(data).some(x => (x !== null && x === undefined));

    if (isEmpty) {
      this.ProjectUserRoleMapping.postRequest('DeleteProjectUserRoleMap', data).subscribe((response) => {
        if (response.status == 1) {
          this.GetProjectUserRoleList(this.tabId, this.selectedProject);
          $('.modal-close').trigger('click');
          // $('.spinner').hide();
          this.successmessage("User deleted successfully");
        }
        else {
          if (response["status"] == 2) {
            $('.spinner').hide();
            this.Errormessage('Project user role already exists');
          }
          $('.spinner').show();
        }
      });
    }
  }

  delMultiUserFromProject() {
    console.log(this.selectedFrontTableUserWRole);

    var data = [];
    if (this.selectedFrontTableUserWRole.length > 0) {
      for (var mainKey in this.selectedFrontTableUserWRole) {
        for (var userListKey in this.selectedFrontTableUserWRole[mainKey]['userList']) {
          data.push({
            ProjectUserRoleMapId: this.selectedFrontTableUserWRole[mainKey]['userList'][userListKey]['projectUserRoleMapId'],
            ProjectId: this.selectedFrontTableUserWRole[mainKey]['projectId'],
            UserId: this.selectedFrontTableUserWRole[mainKey]['userList'][userListKey]['id'],
            RoleId: this.selectedFrontTableUserWRole[mainKey]['roleId'],
          });
        }
      }

      //debugger
      console.log(data);

      this.ProjectUserRoleMapping.postRequest('DeleteMultipleProjectUserRoleMap', data).subscribe((response) => {
        if (response.status == 1) {
          this.GetProjectUserRoleList(this.tabId, this.selectedProject);
          $('.modal-close').trigger('click');
          this.successmessage("User deleted successfully");
          this.selectedTableUserWRole = [];
        }
        else {
          if (response["status"] == 2) {
            this.Errormessage('Project user role already exists');
          }
        }
      });
    }
  }

  delUserFromSelectionModal(user) {
    this.dummyRows = [...this.selectedTableUser];
    this.dummyRows.forEach((elem, i) => {
      if (user.id === elem.id) {
        this.dummyRows.splice(i, 1);

      }
    });
    this.selectedTableUser = this.dummyRows;
  }

  exportPdf(tab) {
    let data;
    switch (tab) {
      case 'pm':
        data = this.pmList;
        break;
      case 'qc':
        data = this.qcList;
        break;
      case 'tl':
        data = this.tlList;
        break;
      case 'pu':
        data = this.puList;
        break;

    }
    this.cols = [
      { field: 'name', header: 'Group Name' },
      { field: 'mailId', header: 'No of Users' },
      { field: 'createdBy', header: 'Created by' },
      { field: 'insertedDateTime', header: 'Created on' },

    ];

    this.exportPdfcolumn = this.cols.map(col => ({ title: col.header, dataKey: col.field }));
    const doc = new jsPDF('l');
    doc.autoTable(this.exportPdfcolumn, data);
    doc.save('UDP_' + tab + '_' + new Date().getTime() + '.pdf');

  }


  exportExcel(tab) {
    let data;
    switch (tab) {
      case 'pm':
        data = this.pmList;
        break;
      case 'qc':
        data = this.qcList;
        break;
      case 'tl':
        data = this.tlList;
        break;
      case 'pu':
        data = this.puList;
        break;

    }
    import('xlsx').then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(data);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, 'UDP_' + tab);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXTENSION_TYPE = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_' + new Date().getTime() + EXTENSION_TYPE);
    });
  }


  exportCSV(tab) {
    let data;
    switch (tab) {
      case 'pm':
        data = this.pmList;
        break;
      case 'qc':
        data = this.qcList;
        break;
      case 'tl':
        data = this.tlList;
        break;
      case 'pu':
        data = this.puList;
        break;

    }
    import('xlsx').then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(data);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'csv', type: 'array' });

      this.saveAsCSVFile(excelBuffer, 'UDP_' + tab);
    });
  }

  saveAsCSVFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {


      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXTENSION_TYPE = '.csv';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_' + new Date().getTime() + EXTENSION_TYPE);
    });
  }




}
