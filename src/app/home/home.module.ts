import { NgModule } from '@angular/core';
import { homeRoutingModule } from './home-routing.module';
import { CommonModule } from '@angular/common';
import { homeComponent } from './home.component';
//import {TableModule} from 'primeng/table';
//import {ChartModule, TreeTableModule,  DropdownModule, MultiSelectModule, CarouselModule, TooltipModule} from 'primeng/primeng';
import {
    ReactiveFormsModule,
    FormsModule,
    FormGroup,
    FormControl,
    Validators,
    FormBuilder
  } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    homeRoutingModule, FormsModule, ReactiveFormsModule,
  ],
  declarations: [homeComponent]
})
export class homeModule { }
