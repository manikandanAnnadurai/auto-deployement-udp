import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class WorkflowService {

  constructor(private http: HttpClient) { }
  getBotList(route): any {
    return this.http.get<any>(route).pipe(map(res => res));
  }

  getJSON(route):any {
    return this.http.get<any>(route).pipe(map(res => res));
  }
}
