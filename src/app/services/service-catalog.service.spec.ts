import { TestBed } from '@angular/core/testing';

import { ServiceCatalogService } from './service-catalog.service';

describe('ServiceCatalogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceCatalogService = TestBed.get(ServiceCatalogService);
    expect(service).toBeTruthy();
  });
});
