import { TestBed } from '@angular/core/testing';

import { ClientAdministrationService } from './client-administration.service';

describe('ClientAdministrationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientAdministrationService = TestBed.get(ClientAdministrationService);
    expect(service).toBeTruthy();
  });
});
