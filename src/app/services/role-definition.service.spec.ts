import { TestBed } from '@angular/core/testing';

import { RoleDefinitionService } from './role-definition.service';

describe('RoleDefinitionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoleDefinitionService = TestBed.get(RoleDefinitionService);
    expect(service).toBeTruthy();
  });
});
