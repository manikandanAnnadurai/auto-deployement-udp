import { TestBed } from '@angular/core/testing';

import { ProjectTypeRegulationService } from './project-type-regulation.service';

describe('ProjectTypeRegulationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjectTypeRegulationService = TestBed.get(ProjectTypeRegulationService);
    expect(service).toBeTruthy();
  });
});
