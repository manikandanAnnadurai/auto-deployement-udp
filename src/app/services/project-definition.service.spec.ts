import { TestBed } from '@angular/core/testing';

import { ProjectDefinitionService } from './project-definition.service';

describe('ProjectDefinitionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjectDefinitionService = TestBed.get(ProjectDefinitionService);
    expect(service).toBeTruthy();
  });
});
